package behaviour.templateMethod.fileProcessor;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

import behaviour.templateMethod.fileprocessor.FileProcessor;

public class CopyProcessor extends FileProcessor{

	
	StringBuffer buffer;
	@Override
	protected void initialize() {
		buffer = new StringBuffer();
	}
	
	@Override
	protected void process(char ch) {
		buffer.append(ch);
		
	}
	
	@Override
	protected void terminate() {
		Writer writer = null;
		try {
			writer = new FileWriter("out.out");
			writer.write(buffer.toString());
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally{
			if(writer != null)
				try {
					writer.close();
				} catch (IOException e) {
					throw new IllegalStateException(e);
				}
		}
	}

}
