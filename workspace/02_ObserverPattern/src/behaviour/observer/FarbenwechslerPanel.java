package behaviour.observer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

public class FarbenwechslerPanel extends JPanel {

	private ArrayList<FarbigerKreis> kreise = new ArrayList<FarbigerKreis>();

	public FarbenwechslerPanel() {
		initializeKreise();
	}

	private void initializeKreise() {
		// TODO Kreise erzeugen
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Color oldColor = g.getColor();
		for (FarbigerKreis kreis : kreise) {
			g.setColor(kreis.getFarbe());
			g.fillOval(kreis.getX(), kreis.getY(), kreis.getRadius(),
					kreis.getRadius());
		}
		g.setColor(oldColor);
	}
	
	public void zeichneDasPanelNeu(){
		this.repaint();
	}
}
