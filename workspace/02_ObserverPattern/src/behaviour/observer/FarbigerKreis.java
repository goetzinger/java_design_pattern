package behaviour.observer;

import java.awt.Color;

public class FarbigerKreis {

	private Color farbe;
	private int x;
	private int y;
	private int radius;

	public FarbigerKreis(Color farbe, int x, int y, int radius) {
		super();
		this.farbe = farbe;
		this.x = x;
		this.y = y;
		this.radius = radius;
	}

	public Color getFarbe() {
		return farbe;
	}

	public void setFarbe(Color farbe) {
		this.farbe = farbe;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getRadius() {
		return radius;
	}

	public void setRadius(int radius) {
		this.radius = radius;
	}

}