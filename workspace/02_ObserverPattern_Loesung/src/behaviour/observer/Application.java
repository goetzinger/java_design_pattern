package behaviour.observer;

import javax.swing.JFrame;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Application().startup();

	}

	private void startup() {
		JFrame appFrame = new JFrame("Farbenwechsler");
		appFrame.setSize(500,500);
		appFrame.add(new FarbenwechslerPanel());
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.setVisible(true);
		
	}

}
