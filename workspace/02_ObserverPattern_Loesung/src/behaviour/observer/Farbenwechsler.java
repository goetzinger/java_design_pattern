package behaviour.observer;

import java.awt.Color;
import java.util.Observable;

public class Farbenwechsler extends Observable implements Runnable {

	private FarbigerKreis kreis;

	public Farbenwechsler(FarbigerKreis kreis) {
		this.kreis = kreis;
	}

	@Override
	public void run() {
		while (true) {
			for (int i = 0; i < 255; i++) {
				for (int j = 0; j < 255; j++)
					for (int k = 0; k < 255; k++) {
						kreis.setFarbe(new Color(i, j, k));
						super.setChanged();
						super.notifyObservers();
						super.clearChanged();
						Thread.yield();
					}
			}

		}

	}

}
