package behaviour.observer;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.JPanel;

public class FarbenwechslerPanel extends JPanel implements Observer {

	private ArrayList<FarbigerKreis> kreise = new ArrayList<FarbigerKreis>();
	private ExecutorService service = Executors.newFixedThreadPool(10);

	public FarbenwechslerPanel() {
		initializeKreise();
	}

	private void initializeKreise() {
		for (int i = 0; i < 10; i++) {
			FarbigerKreis kreis = new FarbigerKreis(Color.black, i *20, i*20, 15);
			kreise.add(kreis);
			Farbenwechsler wechsler = new Farbenwechsler(kreis);
			wechsler.addObserver(this);
			service.submit(wechsler);
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		Color oldColor = g.getColor();
		for (FarbigerKreis kreis : kreise) {
			g.setColor(kreis.getFarbe());
			g.fillOval(kreis.getX(), kreis.getY(), kreis.getRadius(),
					kreis.getRadius());
		}
		g.setColor(oldColor);
	}
	
	public void zeichneDasPanelNeu(){
		this.repaint();
	}

	@Override
	public void update(Observable o, Object arg) {
		this.zeichneDasPanelNeu();
		
	}
}
