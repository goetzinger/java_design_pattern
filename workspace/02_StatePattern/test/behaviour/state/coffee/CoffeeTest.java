package behaviour.state.coffee;

import org.junit.Before;
import org.junit.Test;

public class CoffeeTest {

	private CoffeeMachine testCandidate;

	@Before
	public void schalteKaffeeMaschineEin() {
		this.testCandidate = new CoffeeMachine();
	}

	@Test
	public void sollteAnSein() {
	}

	@Test
	public void öffneKlappe() {
	}

	@Test(expected = IllegalStateException.class)
	public void öffneKlappe2Mal() {
	}
}
