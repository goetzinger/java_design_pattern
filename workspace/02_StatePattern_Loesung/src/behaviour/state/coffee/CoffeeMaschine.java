package behaviour.state.coffee;

public class CoffeeMaschine {
	private OffState offstate = new OffState(this);
	
	
	private State currentState =offstate;


	public Object getState() {
		return currentState;
	}

	public void setState(State newState) {
		this.currentState = newState;
	}

	public void onOff() {
		currentState.onOff();
	}

	public void deckelTasteBetaetigt() {
		currentState.deckelTasteBetaetigt();
	}

	public void padEinlegen() {
		currentState.padEinlegen();
	}

	public void padEntnehmen() {
		currentState.padEntnehmen();
	}

	public void kaffeeZubereiten() {
		currentState.kaffeeZubereiten();
	}

	

}
