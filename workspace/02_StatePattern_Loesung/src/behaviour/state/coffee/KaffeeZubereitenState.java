package behaviour.state.coffee;

public class KaffeeZubereitenState extends State implements Runnable {

	public KaffeeZubereitenState(CoffeeMaschine coffeeMaschine) {
		super(coffeeMaschine);
		Thread thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run() {
		warte20SekundenBisKaffeeDurchgelaufen();
		setzeZustandAufDeckelOffen();
	}

	private void warte20SekundenBisKaffeeDurchgelaufen() {
		try {
			Thread.sleep(20000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void setzeZustandAufDeckelOffen() {
		coffeeMaschine.setState(new PadEingelegtState(coffeeMaschine));
	}

}
