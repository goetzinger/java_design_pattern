package behaviour.state.coffee;

public class MaschineVorbereitetState extends State {

	public MaschineVorbereitetState(CoffeeMaschine coffeeMaschine) {
		super(coffeeMaschine);
	}
	
	@Override
	public void kaffeeZubereiten() {
		coffeeMaschine.setState(new KaffeeZubereitenState(coffeeMaschine));
	}
	
	@Override
	public void deckelTasteBetaetigt() {
		coffeeMaschine.setState(new PadEingelegtState(coffeeMaschine));
	}

}
