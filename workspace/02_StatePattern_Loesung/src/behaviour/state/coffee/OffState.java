package behaviour.state.coffee;

public class OffState extends State {

	public OffState(CoffeeMaschine coffeeMaschine) {
		super(coffeeMaschine);
	}
	
	@Override
	public void onOff() {
		super.coffeeMaschine.setState(new OnState(super.coffeeMaschine));
	}

}
