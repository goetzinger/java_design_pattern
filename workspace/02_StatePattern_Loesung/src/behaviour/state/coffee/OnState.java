package behaviour.state.coffee;

public class OnState extends State {

	public OnState(CoffeeMaschine coffeeMaschine) {
		super(coffeeMaschine);
	}

	@Override
	public void onOff() {
		coffeeMaschine.setState(new OffState(coffeeMaschine));
	}

	@Override
	public void deckelTasteBetaetigt() {
		coffeeMaschine.setState(new OpenState(coffeeMaschine));
	}

}
