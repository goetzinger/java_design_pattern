package behaviour.state.coffee;

public class OpenState extends State {

	public OpenState(CoffeeMaschine coffeeMaschine) {
		super(coffeeMaschine);
	}
	
	@Override
	public void padEinlegen() {
		coffeeMaschine.setState(new PadEingelegtState(coffeeMaschine));
	}
	@Override
	public void deckelTasteBetaetigt() {
		coffeeMaschine.setState(new OnState(coffeeMaschine));
	}
}
