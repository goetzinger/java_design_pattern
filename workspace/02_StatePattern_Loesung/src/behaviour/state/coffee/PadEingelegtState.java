package behaviour.state.coffee;

public class PadEingelegtState extends State {

	public PadEingelegtState(CoffeeMaschine coffeeMaschine) {
		super(coffeeMaschine);
	}
	
	@Override
	public void deckelTasteBetaetigt() {
		coffeeMaschine.setState(new MaschineVorbereitetState(coffeeMaschine));
	}

}
