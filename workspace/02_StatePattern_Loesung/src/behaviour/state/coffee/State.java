package behaviour.state.coffee;

public class State {

	protected CoffeeMaschine coffeeMaschine;

	public State(CoffeeMaschine coffeeMaschine) {
		this.coffeeMaschine = coffeeMaschine;
	}

	public void onOff() {
		throw new IllegalStateException("ON OFF not allowed");		
	}

	public void deckelTasteBetaetigt() {
		throw new IllegalStateException("�ffnen not allowed");	
	}
	
	public void padEinlegen() {
		throw new IllegalStateException("Einlegen not allowed");
	}
	
	public void padEntnehmen() {
		throw new IllegalStateException("Entnehmen not allowed");
	}
	
	public void kaffeeZubereiten(){
		throw new IllegalStateException("Zubereiten not allowed");
	}
	

}
