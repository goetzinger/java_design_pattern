package behaviour.state.coffee;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class CoffeeTest {

	private CoffeeMaschine testCandidate;

	@Before
	public void schalteKaffeeMaschineEin() {
		this.testCandidate = new CoffeeMaschine();
		this.testCandidate.onOff();
	}

	@Test
	public void sollteAnSein() {
		Assert.assertEquals(OnState.class, testCandidate.getState().getClass());
	}
	
	@Test
	public void öffneKlappe(){
		testCandidate.deckelTasteBetaetigt();
		Assert.assertEquals(OpenState.class, testCandidate.getState().getClass());
	}
	
	@Test(expected=IllegalStateException.class)
	public void lege2malPadein(){
		testCandidate.deckelTasteBetaetigt();
		testCandidate.padEinlegen();
		testCandidate.padEinlegen();
	}
}
