package structure.adapter;

public interface IEC_60906_1_Stecker {
	
	public void stromFliesst(int milliampere, int volt);

}
