package structure.adapter;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Application().betreteHotelzimmer();
	}

	private void betreteHotelzimmer() {

		IEC_60906_1_Steckdose steckdose = new IEC_60906_1_Steckdose();

		startSteckdose(steckdose);

		SchukoKabel shukoKabel = new SchukoKabel();

		// TODO stecke Kabel in Steckdose
		steckeKabelInSteckdoese(shukoKabel,steckdose);
	}

	private void steckeKabelInSteckdoese(SchukoKabel shukoKabel,
			IEC_60906_1_Steckdose steckdose) {
		SchukoToIECAdapter schukoToIECAdapter = new SchukoToIECAdapter(shukoKabel);
		steckdose.steckeStecker(schukoToIECAdapter);
		
		
	}

	private void startSteckdose(IEC_60906_1_Steckdose steckdose) {
		new Thread(new SteckdosenRunnable(steckdose)).start();

	}

}
