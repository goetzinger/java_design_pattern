package structure.adapter;

public class IEC_60906_1_Steckdose {
	
	private IEC_60906_1_Stecker eingesteckter;
	private int milliampere = 2000;
	private int volt = 220;
	
	void sendeStrom(){
		if(eingesteckter != null)
			eingesteckter.stromFliesst(milliampere, volt);
	}

	public void steckeStecker(IEC_60906_1_Stecker eingesteckter) {
		if(eingesteckter != null)
			this.eingesteckter = eingesteckter;
	}

	

}
