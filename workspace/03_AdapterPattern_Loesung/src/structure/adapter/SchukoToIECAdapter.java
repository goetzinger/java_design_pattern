package structure.adapter;

public class SchukoToIECAdapter implements IEC_60906_1_Stecker {

	private SchukoKabel shukoKabel;

	public SchukoToIECAdapter(SchukoKabel shukoKabel) {
		this.shukoKabel = shukoKabel;
	}

	@Override
	public void stromFliesst(int milliampere, int volt) {
		shukoKabel.stromKommt(volt, milliampere / 1000);
		
	}

}
