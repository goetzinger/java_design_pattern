package structure.adapter;

public class SteckdosenRunnable implements Runnable {

	private IEC_60906_1_Steckdose steckdose;

	public SteckdosenRunnable(IEC_60906_1_Steckdose steckdose2) {
		this.steckdose = steckdose2;
	}

	@Override
	public void run() {
		while (!Thread.currentThread().isInterrupted()) {
			steckdose.sendeStrom();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}

	}
}
