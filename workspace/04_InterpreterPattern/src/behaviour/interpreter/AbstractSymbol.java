package behaviour.interpreter;

public abstract class AbstractSymbol implements Symbol {
	public boolean isNumber() {
		return false;
	}

	public boolean isIdentifier() {
		return false;
	}

	public boolean isSpecial() {
		return false;
	}

	public double getNumber() {
		throw new RuntimeException();
	}

	public String getIdentifier() {
		throw new RuntimeException();
	}

	public char getSpecial() {
		throw new RuntimeException();
	}
}