package behaviour.interpreter;

public class AddExpression extends Expression {

	private Expression whatToAdd;

	public AddExpression(Expression toAddExpression) {
		this.whatToAdd = toAddExpression;
	}

	@Override
	public Object evaluate() {
		return whatToAdd.evaluate();

	}

}
