package behaviour.interpreter;
public class Constant extends Expression {

  public final String value;

  public Constant (String value) {
    this.value = value;
  }

  public Object evaluate () {
    return this.value;
  }
}
