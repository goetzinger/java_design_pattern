package behaviour.interpreter;

import java.util.ArrayList;
import java.util.List;

public class ExpressionList extends Expression {

	List<Expression> expressions = new ArrayList<Expression>();

	public void add(Expression expression) {
		this.expressions.add(expression);
	}

	@Override
	public Object evaluate() {
		Object[] toReturn = new Object[expressions.size()];
		expressions.toArray(toReturn);
		return toReturn;
	}

}
