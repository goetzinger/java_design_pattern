package behaviour.interpreter;

public class ExpressionParser {

	private Scanner scanner;
	private Symbol symbol;

	public ExpressionParser(Scanner scanner) {
		this.scanner = scanner;
	}

	private boolean isOperator(char op) {
		return this.symbol != null && this.symbol.isSpecial()
				&& this.symbol.getSpecial() == op;
	}

	public Expression parseExpression() {
		this.symbol = scanner.read();
		if (isOperator('+')) {
			this.symbol = scanner.read();
			Expression toAddExpression = this.parseTerm();
			return new AddExpression(toAddExpression);
		} else
			throw new IllegalStateException();
	}

	private Expression parseTerm() {
		if (this.isOperator('(')) {

			ExpressionList list = new ExpressionList();
			this.symbol = scanner.read();
			while (!this.isOperator(')')) {
				
				list.add(this.parseTerm());
				this.symbol = scanner.read();
			}

			return list;
		}
		return this.parseFactor();

	}

	private Expression parseFactor() {
		if (this.symbol.isIdentifier())
			return new Constant(symbol.getIdentifier());
		else
			throw new IllegalStateException();
	}

}
