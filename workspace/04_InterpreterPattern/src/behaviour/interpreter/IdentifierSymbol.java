package behaviour.interpreter;


public class IdentifierSymbol extends AbstractSymbol {
	private final String value;

	public IdentifierSymbol(String v) {
		this.value = v;
	}

	public boolean isIdentifier() {
		return true;
	}

	public String getIdentifier() {
		return this.value;
	}

	public Object getValue() {
		return this.value;
	}
}