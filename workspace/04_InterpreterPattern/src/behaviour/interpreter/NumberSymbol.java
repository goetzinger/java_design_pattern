package behaviour.interpreter;


public class NumberSymbol extends AbstractSymbol {
	private final double value;

	public NumberSymbol(double v) {
		this.value = v;
	}

	public boolean isNumber() {
		return true;
	}

	public double getNumber() {
		return this.value;
	}

	public Object getValue() {
		return this.value;
	}
}