package behaviour.interpreter;

import java.io.StringReader;

public class Scanner {

	private StringReader reader;
	private String specialChars;
	private int currentChar;
	private StringBuffer buf = new StringBuffer();

	public Scanner(String specialChars, StringReader stringReader) {
		this.specialChars = specialChars;
		this.reader = stringReader;
	}

	public Symbol read() {
		this.readChar();
		while (Character.isWhitespace(this.currentChar))
			this.readChar();
		if (this.currentChar == -1)
			return null;
		if (Character.isDigit(this.currentChar))
			return this.readNumber();
		if (Character.isLetter(this.currentChar))
			return this.readIdentifier();
		if (this.specialChars.indexOf(this.currentChar) >= 0) {
			Symbol symbol = new SpecialSymbol((char) this.currentChar);
			this.readChar();
			return symbol;
		}
		throw new RuntimeException("bad char: '" + (char) this.currentChar
				+ "'");
	}

	private Symbol readNumber() {
		this.buf.setLength(0);
		
		while (Character.isDigit(this.currentChar)) {
			this.buf.append((char) this.currentChar);
			this.readChar();
		}
		if (this.currentChar == '.') {
			this.buf.append((char) this.currentChar);
			this.readChar();
			while (Character.isDigit(this.currentChar)) {
				this.buf.append((char) this.currentChar);
				this.readChar();
			}
		}
		if (Character.isLetter(this.currentChar))
			throw new RuntimeException("bad number: " + this.buf
					+ (char) this.currentChar);
		return new NumberSymbol(Double.parseDouble(this.buf.toString()));
	}

	private Symbol readIdentifier() {
		this.buf.setLength(0);
		this.buf.append((char) this.currentChar);
		this.readChar();
		while (Character.isLetterOrDigit(this.currentChar)) {
			this.buf.append((char) this.currentChar);
			this.readChar();
		}
		return new IdentifierSymbol(this.buf.toString());
	}

	private void readChar() {
		try {
			this.currentChar = this.reader.read();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

}
