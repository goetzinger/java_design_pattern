package behaviour.interpreter;


public class SpecialSymbol extends AbstractSymbol {
	private final char value;

	public SpecialSymbol(char v) {
		this.value = v;
	}

	public boolean isSpecial() {
		return true;
	}

	public char getSpecial() {
		return this.value;
	}

	public Object getValue() {
		return this.value;
	}
}