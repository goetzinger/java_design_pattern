package behaviour.interpreter;
public interface Symbol { 

  public abstract boolean isIdentifier ();
  public abstract boolean isSpecial ();

  public abstract String getIdentifier ();
  public abstract char getSpecial ();

  public abstract Object getValue ();
}
