package structure.proxy;

public class Client {
	
	private IService service;
	
	public void doSomething(){
		System.out.println(service.doHelloWorld());
	}
	
	public void setService(IService service) {
		this.service = service;
	}

}
