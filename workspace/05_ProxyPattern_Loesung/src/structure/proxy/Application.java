package structure.proxy;

import java.lang.reflect.Proxy;

public class Application {

	public static void main(String[] args) {
		Client client = new Client();
		ServiceReal service = new ServiceReal();
		client.setService(service);
		client.doSomething();

		Client clientMitStellvertreter = new Client();
		clientMitStellvertreter.setService(new ServiceProxy(service));
		clientMitStellvertreter.doSomething();
		clientMitStellvertreter.doSomething();

		Client clientDerEinenDynamischenProxyBekommt = new Client();
		IService serviceProxy = (IService) Proxy.newProxyInstance(
				Application.class.getClassLoader(),
				new Class[] { IService.class }, new HelloWorldServiceCache("hier sollte der realService sein"));
		clientDerEinenDynamischenProxyBekommt.setService(serviceProxy);
		clientDerEinenDynamischenProxyBekommt.doSomething();
	}
}
