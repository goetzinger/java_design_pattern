package structure.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class HelloWorldServiceCache implements InvocationHandler {
	
	String cachedHelloWorld;
	private Object realTarget;
	
	public HelloWorldServiceCache(Object realTarget) {
		super();
		this.realTarget = realTarget;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		if(method.getName().equals("doSomething"))
		{
			if(cachedHelloWorld != null)
				return cachedHelloWorld;
			else
				return cachedHelloWorld = (String) method.invoke(realTarget, args); 
		}
		return method.invoke(realTarget, args);
	}

}
