package structure.proxy;

public class ServiceProxy implements IService {

	private ServiceReal realTarget;
	private String cachedResult;

	public ServiceProxy(ServiceReal service) {
		this.realTarget = service;
	}

	@Override
	public String doHelloWorld() {
		if(this.cachedResult == null)
			cachedResult = realTarget.doHelloWorld();
		System.out.println("Proxy aufgerufen");
		return cachedResult;
	}

}
