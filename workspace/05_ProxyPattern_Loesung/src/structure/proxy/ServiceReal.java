package structure.proxy;

public class ServiceReal implements IService {

	@Override
	public String doHelloWorld() {
		return "hello World";
	}

}
