package net.javaseminar;

import javax.swing.JFrame;

public class Application {

	private JFrame appFrame;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Application().startup();

	}

	private void startup() {
		createFrame();
		initializeFrame();
		showFrame();
	}

	private JFrame createFrame() {
		return (this.appFrame = new JFrame("Strategy Pattern"));
	}

	private void initializeFrame() {
		appFrame.setSize(300,300);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		appFrame.add(new ValidationPanel());
	}

	private void showFrame() {
		appFrame.setVisible(true);
	}

}
