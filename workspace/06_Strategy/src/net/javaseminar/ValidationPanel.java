package net.javaseminar;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ValidationPanel extends JPanel {

	private JTextField someInteger;
	private JTextField integerBetween1000And1999;
	private JTextField doubleBetween1And10;
	private JButton saveButton;
	private JTextField exceptionField;

	public ValidationPanel() {
		initializeContent();
		initializeActionListener();
	}

	private void initializeContent() {
		this.setLayout(new BorderLayout());
		JPanel center = new JPanel();
		center.setLayout(new GridLayout(5,1));
		this.add(center);
		center.add(someInteger = new JTextField(50));
		center.add(integerBetween1000And1999 = new JTextField(50));
		center.add(doubleBetween1And10 = new JTextField(50));
		center.add(saveButton = new JButton("save"));
		center.add(exceptionField = new JTextField(200));
	}

	private void initializeActionListener() {
	}

}
