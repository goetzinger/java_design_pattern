package net.javaseminar;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;

public class Application {

	private JFrame appFrame;
	private ValidationPanel mainPanel;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Application().startup();

	}

	private void startup() {
		createFrame();
		initializeFrame();
		initializeValidator();
		showFrame();
	}

	private JFrame createFrame() {
		return (this.appFrame = new JFrame("Strategy Pattern"));
	}

	private void initializeFrame() {
		appFrame.setSize(600,300);
		appFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		mainPanel = new ValidationPanel();
		appFrame.add(mainPanel);
	}
	
	
	private void initializeValidator(){
		List<ValidatorStrategy> validator = new ArrayList<ValidatorStrategy>();
		validator.add(new IntValidator());
		validator.add(new Int1000To2000Validator());
		validator.add(new Double1To10Validator());
		mainPanel.setValidator(validator);
	}

	private void showFrame() {
		appFrame.setVisible(true);
	}

}
