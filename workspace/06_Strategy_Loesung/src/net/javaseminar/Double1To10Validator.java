package net.javaseminar;

public class Double1To10Validator implements ValidatorStrategy {

	@Override
	public void validate(String toValidate) {
		double number = Double.parseDouble(toValidate);
		if(number < 1.0 || number > 10.0)
			throw new IllegalArgumentException(toValidate +" not between 1.0 and 10.0");

	}

}
