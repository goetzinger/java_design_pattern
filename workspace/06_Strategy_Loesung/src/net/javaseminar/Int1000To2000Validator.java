package net.javaseminar;

public class Int1000To2000Validator implements ValidatorStrategy {

	@Override
	public void validate(String toValidate) {
		int number = Integer.parseInt(toValidate);
		if(number < 1000 || number >= 2000)
			throw new IllegalArgumentException(toValidate + " Number not between 1000 and 1999");
	}

}
