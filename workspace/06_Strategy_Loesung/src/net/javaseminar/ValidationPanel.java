package net.javaseminar;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ValidationPanel extends JPanel {

	private JTextField someInteger;
	private JTextField integerBetween1000And1999;
	private JTextField doubleBetween1And10;
	private JButton saveButton;
	private JTextField exceptionField;
	private List<? extends ValidatorStrategy> validator;

	public ValidationPanel() {
		initializeContent();
		initializeActionListener();
	}

	private void initializeContent() {
		this.setLayout(new BorderLayout());
		JPanel center = new JPanel();
		center.setLayout(new GridLayout(5,1));
		this.add(center);
		center.add(someInteger = new JTextField(50));
		center.add(integerBetween1000And1999 = new JTextField(50));
		center.add(doubleBetween1And10 = new JTextField(50));
		center.add(saveButton = new JButton("save"));
		center.add(exceptionField = new JTextField(200));
		exceptionField.setEditable(false);
	}

	private void initializeActionListener() {
		this.saveButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				validateAllFields();
			}
		});
	}

	private void validateAllFields() {
		this.exceptionField.setText("");
		try {
			this.validator.get(0).validate(someInteger.getText());
			this.validator.get(1).validate(integerBetween1000And1999.getText());
			this.validator.get(2).validate(doubleBetween1And10.getText());
		} catch (Exception e) {
			this.exceptionField.setText(e.getClass().getSimpleName() + " : " +e.getMessage());
		}

	}

	public void setValidator(List<? extends ValidatorStrategy> valdiator) {
		this.validator = valdiator;
	}
}
