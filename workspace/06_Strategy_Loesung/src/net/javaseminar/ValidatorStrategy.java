package net.javaseminar;

public interface ValidatorStrategy {
	
	public void validate(String toValidate);

}
