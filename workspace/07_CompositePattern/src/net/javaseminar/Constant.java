package net.javaseminar;

public class Constant extends Call {

	double value;

	public Constant(double value) {
		super();
		this.value = value;
	}

	@Override
	public double evaluate() {
		return value;
	}

}
