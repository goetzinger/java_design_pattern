package net.javaseminar;

public class Mal extends Call {

	private Call rechterSummand;
	private Call linkerSummand;

	public Mal(Call linkerSummand, Call rechterSummand) {
		this.linkerSummand = linkerSummand;
		this.rechterSummand = rechterSummand;
	}

	@Override
	public double evaluate() {
		return rechterSummand.evaluate() * linkerSummand.evaluate();
	}
}
