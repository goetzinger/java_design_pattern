package net.javaseminar;

public class Plus extends Call{

	private Call multiplikator1;
	private Call multiplikator2;

	public Plus(Call multiplikator1,Call multiplikator2) {
		this.multiplikator1 = multiplikator1;
		this.multiplikator2 = multiplikator2;
	}
	
	@Override
	public double evaluate(){
		return multiplikator1.evaluate() + multiplikator2.evaluate();
	}

	}
