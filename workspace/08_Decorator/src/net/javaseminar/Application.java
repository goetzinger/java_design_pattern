package net.javaseminar;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		starteGruenenClient();
		starteRoteClient();
		startBlauenClient();
	}

	private static void startBlauenClient() {
		Fahrzeug f = new PKW();
		Schaltungsdecorator schaltungsdecorator = new Schaltungsdecorator(f);
		Bremsendecorator decoratorMitBremseUndSchaltung = new Bremsendecorator(schaltungsdecorator);
		decoratorMitBremseUndSchaltung.fahren();
	}

	private static void starteGruenenClient() {
		Fahrzeug f = new PKW();
		f.fahren();
	}

	private static void starteRoteClient() {
		Fahrzeug f = new PKW();
		Fahrzeug decorator = new Schaltungsdecorator(f);
		decorator.fahren();
	}

}
