package net.javaseminar;

public class Bremsendecorator implements Fahrzeug {

	private Fahrzeug fahrzeug;

	public Bremsendecorator(Fahrzeug fahrzeug) {
		this.fahrzeug = fahrzeug;
	}

	@Override
	public void fahren() {
		System.out.println("Bremse l�sen");
		this.fahrzeug.fahren();
	}

}
