package net.javaseminar;

import java.lang.reflect.Proxy;

public class Application {
	
	public static void main(String[] args) {
		Fahrer client = new Fahrer();
		client.setFahrzeug(new PKW());
		client.fahrLos();
		
		Fahrer fahrerMitFahrzeugElektronischeHilfe = new Fahrer();
		Fahrzeug fahrzeugMitProxy = new Schaltungsproxy(new PKW());
		fahrerMitFahrzeugElektronischeHilfe.setFahrzeug(fahrzeugMitProxy);
		fahrerMitFahrzeugElektronischeHilfe.fahrLos();
		
		Fahrer fahrerMitDynamischemProxy = new Fahrer();
		Fahrzeug proxy = (Fahrzeug) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(),
				new Class[]{Fahrzeug.class}, new SchaltungInvocationHandler(new PKW()));
		fahrerMitDynamischemProxy.setFahrzeug(proxy);
		fahrerMitDynamischemProxy.fahrLos();
	}

}
