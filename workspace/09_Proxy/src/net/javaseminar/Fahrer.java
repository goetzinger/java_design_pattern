package net.javaseminar;

public class Fahrer {
	
	private Fahrzeug fahrzeug;

	public void fahrLos(){
		this.fahrzeug.fahren();
		System.out.println("Fahrer ruft toString auf!");
		this.fahrzeug.toString();
	}
	
	public void setFahrzeug(Fahrzeug fahrzeug) {
		this.fahrzeug = fahrzeug;
		
	}

}
