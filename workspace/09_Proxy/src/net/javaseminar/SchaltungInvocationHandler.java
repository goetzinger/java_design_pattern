package net.javaseminar;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class SchaltungInvocationHandler implements InvocationHandler {

	private Object pkw;

	public SchaltungInvocationHandler(Object  pkw) {
		this.pkw = pkw;
	}

	@Override
	public Object invoke(Object proxy, Method method, Object[] args)
			throws Throwable {
		System.out.println("Methode " + method.getName() + " wurde aufgerufen");
		if (method.getName().equals("fahren")) {
			System.out.println("Schalten");
		}
		if (pkw.getClass().getMethod(method.getName(),
				method.getParameterTypes()) != null)
			return method.invoke(pkw, args);
		throw new IllegalArgumentException(pkw.getClass().getSimpleName()
				+ " hat keine Methode " + method);
	}

}
