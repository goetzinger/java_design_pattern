package net.javaseminar;

public class Schaltungsproxy implements Fahrzeug {

	private PKW pkw;

	public Schaltungsproxy(PKW pkw) {
		this.pkw = pkw;
	}

	@Override
	public void fahren() {
		System.out.println("Gang einlegen");
		pkw.fahren();

	}

}
