package net.javaseminar;

public class Paket<T> {

	private T inhalt;

	public Paket(T inhalt) {
		this.inhalt = inhalt;
	}

	
	public T getInhalt() {
		return inhalt;
	}
}
