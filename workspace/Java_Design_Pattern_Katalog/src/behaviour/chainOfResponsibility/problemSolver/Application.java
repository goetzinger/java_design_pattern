package behaviour.chainOfResponsibility.problemSolver;
public class Application {

  public static void main (String [] args) {
    
    ProblemSolverChain chain = new ProblemSolverChain ();

    chain.append (new PlusSolver ());
    chain.append (new SqrtSolver ());
    chain.append (new ConcatSolver ());

    String problem = Console.readLine ("a problem (or exit) ");
    while (! "exit".equals (problem)) {
      try {
        chain.solve (problem);
      }
      catch (Exception e) {
        System.out.println (e);
      }
      problem = Console.readLine ("a problem (or exit) ");
    }
  }
}