package behaviour.chainOfResponsibility.problemSolver;
public class ConcatSolver extends ProblemSolver {
  public void solve (String problem) throws Exception {
    String [] tokens = problem.split (" ");
    if (! (tokens.length == 3 && tokens [0].equals ("concat"))) {
      this.sendToNext (problem);
      return;
    }
    String result = tokens [1] + tokens [2];
    System.out.println (result);
  }
}

