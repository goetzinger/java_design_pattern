package behaviour.chainOfResponsibility.problemSolver;
import java.io.*;

public class Console {
	
	public static String readLine () {
		return Console.readLine (null);
	}

	public static String readLine (String prompt) {
		if (prompt != null)
			System.out.print (prompt);
		System.out.print ("> ");
		BufferedReader rdr = new BufferedReader (new InputStreamReader (System.in));
		try {
			return rdr.readLine ();
		}
		catch (Exception e) {
			return null;
		}
	}
}
