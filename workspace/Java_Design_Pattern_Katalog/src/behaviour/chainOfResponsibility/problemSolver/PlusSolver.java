package behaviour.chainOfResponsibility.problemSolver;
public class PlusSolver extends ProblemSolver {
  public void solve (String problem) throws Exception {
    String [] tokens = problem.split (" ");
    if (! (tokens.length == 3 && tokens [1].equals ("+"))) {
      this.sendToNext (problem);
      return;
    }
    double result = Double.parseDouble (tokens [0])
                  + Double.parseDouble (tokens [2]);
    System.out.println (result);
  }
}
