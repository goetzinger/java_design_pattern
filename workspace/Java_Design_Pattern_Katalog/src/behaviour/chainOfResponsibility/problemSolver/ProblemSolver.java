package behaviour.chainOfResponsibility.problemSolver;
public abstract class ProblemSolver {
  private ProblemSolver next;
  public abstract void solve (String problem) throws Exception;
  public void setNext (ProblemSolver next) {
    this.next = next;
  }
  protected void sendToNext (String problem) throws Exception {
    if (this.next != null)
      this.next.solve (problem);
  }
}
