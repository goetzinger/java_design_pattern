package behaviour.chainOfResponsibility.problemSolver;
class ProblemSolverChain {
  private ProblemSolver first;
  private ProblemSolver last;
  public void append (ProblemSolver problemSolver) {
    if (this.first == null)
      this.first = problemSolver;
    else
      this.last.setNext (problemSolver);
    this.last = problemSolver;
  }
  public void solve (String problem) throws Exception {
    if (this.first != null)
      this.first.solve (problem);
  }
}

