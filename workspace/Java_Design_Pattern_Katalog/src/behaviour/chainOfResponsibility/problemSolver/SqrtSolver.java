package behaviour.chainOfResponsibility.problemSolver;
public class SqrtSolver extends ProblemSolver {
  public void solve (String problem) throws Exception {
    if (! (problem.startsWith ("sqrt"))) {
      this.sendToNext (problem);
      return;
    }
    String [] tokens = problem.split (" ");
    if (tokens.length != 2)
      throw new Exception ();
    double result = Math.sqrt (Double.parseDouble (tokens [1]));
    System.out.println (result);
  }
}

