package behaviour.command.calc;
public class AddCommand implements Command {
  private final Calculator calculator;
  private final int value;
  public AddCommand (Calculator calculator, String [] tokens) throws Exception {
    this.calculator = calculator;
    if (tokens.length != 2)
      throw new Exception ("add <number>");
    try {
      this.value = Integer.parseInt (tokens [1]);
    }
    catch (NumberFormatException e) {
      throw new Exception ("add <number>");
    }
  }
  public void execute () {
    this.calculator.add (this.value);
  }
  public void undo () {
    this.calculator.subtract (this.value);
  }
  public boolean isQuery () {
    return false;
  }
}