package behaviour.command.calc;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Application {

  public static void main (String [] args) {
    System.out.println ("usage: add <number> | subtract <number> | get | exit");
        
    Calculator calculator = new Calculator ();
    CommandHistory history = new CommandHistory ();
    
    try {
      BufferedReader reader = new BufferedReader (new InputStreamReader (System.in));
      String line;
      while (! (line = reader.readLine ()).equals ("exit")) {
        String [] tokens = line.split (" ");
        if (tokens.length == 0)
          continue;
        String commandWord = tokens [0];
        if ("undo".equals (commandWord) || "redo".equals (commandWord)) {
          if ("undo".equals (commandWord)) {
            if (history.canUndo ())
              history.undo ();
            else
              System.out.println ("cannot undo");
          }
          else {
            if (history.canRedo ())
              history.redo ();
            else
              System.out.println ("cannot redo");
          }
        }
        else {
          Command command = null;
          try {
            if ("add".equals (commandWord)) 
              command = new AddCommand (calculator, tokens);
            else if ("subtract".equals (commandWord)) 
              command = new SubtractCommand (calculator, tokens);
            else if ("get".equals (commandWord)) 
              command = new QueryCommand (calculator, tokens);
            else
              System.out.println ("unknown command");
          }
          catch (Exception e) {
            System.out.println ("usage: " + e.getMessage ());
          }
          if (command != null) {
            command.execute ();
            if (! command.isQuery ())
              history.add (command);
          }
        }
      }
    }
    catch (Exception e) {
      System.out.println (e);
    }
  }
}
