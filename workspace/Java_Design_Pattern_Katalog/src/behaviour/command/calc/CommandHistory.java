package behaviour.command.calc;
import java.util.ArrayList;

public class CommandHistory {
  
  private ArrayList<Command> commands = new ArrayList<Command> ();
  private int undoIndex = -1;
  
  public void add (Command cmd) {
    for (int i = this.size () - 1; i > this.undoIndex; i--)
      this.commands.remove (i);
    this.commands.add (cmd);
    this.undoIndex++;
  }

  public int size () {
    return this.commands.size ();
  }
  
  public Command get (int index) {
    return (Command) this.commands.get (index);
  }

  public void undo () {
    if (! this.canUndo ())
      throw new IllegalStateException ("cannot undo");
    this.get (this.undoIndex).undo ();
    this.undoIndex--;
  } 

  public void redo () {
    if (! this.canRedo ())
      throw new IllegalStateException ("cannot redo");
    this.undoIndex++;
    this.get (this.undoIndex).execute ();
  } 

  public boolean canUndo () {
    return this.undoIndex >= 0;
  }

  public boolean canRedo () {
    return this.undoIndex < this.size () - 1;
  }
}
