package behaviour.command.calc;
public class QueryCommand implements Command {
  private final Calculator calculator;
  public QueryCommand (Calculator calculator, String [] tokens) throws Exception {
    this.calculator = calculator;
    if (tokens.length != 1)
      throw new Exception ("get");
  }
  public void execute () {
    System.out.println (this.calculator.getValue ());
  }
  public void undo () {
    throw new RuntimeException ();
  }
  public boolean isQuery () {
    return true;
  }
}