package behaviour.command.commandFactory;
public class AddCommand implements Command {
  
  public static class Factory implements CommandFactory {
    public String getName () {
      return "add";
    }
    public Command createCommand (
          Calculator calculator, String [] tokens) throws Exception {
      return new AddCommand (calculator, tokens);
    }
  }

  private final Calculator calculator;
  private final int value;
  private AddCommand (Calculator calculator, String [] tokens) throws Exception {
    this.calculator = calculator;
    if (tokens.length != 2)
      throw new Exception ("add <number>");
    try {
      this.value = Integer.parseInt (tokens [1]);
    }
    catch (NumberFormatException e) {
      throw new Exception ("add <number>");
    }
  }
  public void execute () {
    this.calculator.add (this.value);
  }
  public void undo () {
    this.calculator.subtract (this.value);
  }
  public boolean isQuery () {
    return false;
  }
}