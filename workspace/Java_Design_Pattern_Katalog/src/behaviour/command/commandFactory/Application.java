package behaviour.command.commandFactory;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Application {

  public static void main (String [] args) {
    System.out.println ("usage: add <number> | subtract <number> | get | exit");
        
    Calculator calculator = new Calculator ();
    CommandHistory history = new CommandHistory ();
    CommandFactoryTable factoryTable = new CommandFactoryTable ();
    
    try {
      BufferedReader reader = new BufferedReader (new InputStreamReader (System.in));
      String line;
      while (! (line = reader.readLine ()).equals ("exit")) {
        String [] tokens = line.split (" ");
        if (tokens.length == 0)
          continue;
        String commandWord = tokens [0];
        if ("undo".equals (commandWord) || "redo".equals (commandWord)) {
          if ("undo".equals (commandWord)) {
            if (history.canUndo ())
              history.undo ();
            else
              System.out.println ("cannot undo");
          }
          else {
            if (history.canRedo ())
              history.redo ();
            else
              System.out.println ("cannot redo");
          }
        }
        else {
          CommandFactory factory = factoryTable.getCommandFactory (commandWord);
          if (factory == null)  {
            System.out.println ("command '" + commandWord + "' not found");
          }
          else {
            try {
              Command command = factory.createCommand (calculator, tokens);
              command.execute ();
              if (! command.isQuery ())
                history.add (command);
            }
            catch (Exception e) {
              System.out.println (e);
            }
          }
        }
      }
    }
    catch (Exception e) {
      System.out.println (e);
    }
  }
}
