package behaviour.command.commandFactory;
public interface Command {
  public abstract void execute ();
  public abstract void undo ();
  public abstract boolean isQuery ();
}
