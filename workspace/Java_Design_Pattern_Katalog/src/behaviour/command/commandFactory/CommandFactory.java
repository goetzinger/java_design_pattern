package behaviour.command.commandFactory;
public interface CommandFactory {
  public abstract String getName ();
  public abstract Command createCommand (
                  Calculator calculator, String [] tokens) throws Exception;
}