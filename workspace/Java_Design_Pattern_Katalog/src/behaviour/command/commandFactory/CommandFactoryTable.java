package behaviour.command.commandFactory;
import java.util.ArrayList;
import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Modifier;

public class CommandFactoryTable {
  private ArrayList<CommandFactory> factories = new ArrayList<CommandFactory> ();

  public CommandFactoryTable () {
    try {
      File dir = new File (".");      
      String [] fileNames = dir.list (new FilenameFilter () {
        public boolean accept (File dir, String name) {
          return name.endsWith (".class");
        } 
      });
      for (int i = 0; i < fileNames.length; i++) {
        String filename = fileNames [i];
        int pos = filename.lastIndexOf ('.');
        String className = filename.substring (0, pos);
        Class cls = Class.forName (className);
        if (CommandFactory.class.isAssignableFrom (cls) 
                && ! Modifier.isAbstract (cls.getModifiers ())) {
          CommandFactory command = (CommandFactory) cls.newInstance ();
          this.factories.add (command);
        }
      }
    }
    catch (Exception e) {
      System.err.println (e);
    }
  }

  public CommandFactory getCommandFactory (String name) {
    for (CommandFactory factory : this.factories) {
      if (factory.getName ().equals (name))
        return factory;
    }
    return null;
  }
}
