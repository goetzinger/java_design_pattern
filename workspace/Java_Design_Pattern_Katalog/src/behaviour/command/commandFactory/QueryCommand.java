package behaviour.command.commandFactory;
public class QueryCommand implements Command {

	public static class Factory implements CommandFactory {
	  public String getName () {
	  	return "get";
	  }
  	public Command createCommand (
  				Calculator calculator, String [] tokens) throws Exception {
  		return new QueryCommand (calculator, tokens);
  	}
	}

  private final Calculator calculator;
  private QueryCommand (Calculator calculator, String [] tokens) throws Exception {
  	this.calculator = calculator;
  	if (tokens.length != 1)
  		throw new Exception ("get");
  }
  public void execute () {
  	System.out.println (this.calculator.getValue ());
  }
  public void undo () {
  	throw new RuntimeException ();
  }
  public boolean isQuery () {
  	return true;
  }
}