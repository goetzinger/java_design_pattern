package behaviour.command.commandFactory;
public class SubtractCommand implements Command {

	public static class Factory implements CommandFactory {
	  public String getName () {
	  	return "subtract";
	  }
  	public Command createCommand (
  				Calculator calculator, String [] tokens) throws Exception {
  		return new SubtractCommand (calculator, tokens);
  	}
	}

  private final Calculator calculator;
  private final int value;
  private SubtractCommand (Calculator calculator, String [] tokens) throws Exception {
  	this.calculator = calculator;
  	if (tokens.length != 2)
  		throw new Exception ("subtract <number>");
  	try {
			this.value = Integer.parseInt (tokens [1]);
		}
		catch (NumberFormatException e) {
			throw new Exception ("subtract <number>");
		}
  }
  public void execute () {
  	this.calculator.subtract (this.value);
  }
  public void undo () {
  	this.calculator.add (this.value);
  }
  public boolean isQuery () {
  	return false;
  }
}