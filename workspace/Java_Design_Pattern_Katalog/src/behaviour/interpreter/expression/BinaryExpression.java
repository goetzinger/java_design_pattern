package behaviour.interpreter.expression;
public abstract class BinaryExpression extends Expression {

  public final Expression left;
  public final Expression right;

  public BinaryExpression (Expression left, Expression right) {
    this.left = left;
    this.right = right;
  }
}