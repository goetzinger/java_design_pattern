package behaviour.interpreter.expression;
public class Constant extends Expression {

  public final double value;

  public Constant (double value) {
    this.value = value;
  }

  public double evaluate () {
    return this.value;
  }
}
