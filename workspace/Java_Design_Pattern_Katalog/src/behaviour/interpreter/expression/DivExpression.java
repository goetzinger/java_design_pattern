package behaviour.interpreter.expression;
public class DivExpression extends BinaryExpression {
  public DivExpression (Expression left, Expression right) {
    super (left, right);
  }
  public double evaluate () {
    return this.left.evaluate () / this.right.evaluate ();
  }
}