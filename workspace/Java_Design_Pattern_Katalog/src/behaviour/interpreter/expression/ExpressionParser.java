package behaviour.interpreter.expression;
public class ExpressionParser {

  private final Scanner scanner;
  private Symbol symbol;

  public ExpressionParser (Scanner scanner) {
    this.scanner = scanner;
    this.read ();
  }
  
  private void read () {
    this.symbol = scanner.read ();
  }
  
  private boolean isOperator (char op) {
    return this.symbol != null 
            &&this.symbol.isSpecial () && this.symbol.getSpecial () == op;
  }

  public Expression parseExpression () {
    Expression left = this.parseTerm ();
    while (this.isOperator ('+') || this.isOperator ('-')) {
      boolean plus = this.isOperator ('+');
      this.read ();
      Expression right = this.parseTerm ();
      if (plus) 
        left = new PlusExpression (left, right);
      else
        left = new MinusExpression (left, right);
    }
    return left;
  }

  public Expression parseTerm () {
    Expression left = this.parseFactor ();
    while (this.isOperator ('*') || this.isOperator ('/')) {
      boolean times = this.isOperator ('*');
      this.read ();
      Expression right = this.parseFactor ();
      if (times) 
        left = new TimesExpression (left, right);
      else
        left = new DivExpression (left, right);
    }
    return left;
  }

  public Expression parseFactor () {
    boolean minus = false;
    if (this.isOperator ('-')) { // unary minus 
      minus = true;
      this.read ();
    }
    Expression expr;
    if (this.symbol.isNumber ()) {
      expr = new Constant (this.symbol.getNumber ());
      this.read ();
     }
    else if (this.isOperator ('(')) {
      this.read ();
      expr = this.parseExpression ();
      if (! this.isOperator (')'))
        throw new RuntimeException ("')' expected");
      this.read ();
    }
    else
      throw new RuntimeException ("number or '(' expected");
    if (minus) 
      expr = new NegativeExpression (expr);
    return expr;
  }
}
