package behaviour.interpreter.expression;
public class MinusExpression extends BinaryExpression {
  public MinusExpression (Expression left, Expression right) {
    super (left, right);
  }
  public double evaluate () {
    return this.left.evaluate () - this.right.evaluate ();
  }
}