package behaviour.interpreter.expression;
public class NegativeExpression extends UnaryExpression {
  public NegativeExpression (Expression expr) {
    super (expr);
  }
  public double evaluate () {
    return - this.expr.evaluate ();
  }
}