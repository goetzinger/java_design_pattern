package behaviour.interpreter.expression;
public class PlusExpression extends BinaryExpression {
  public PlusExpression (Expression left, Expression right) {
    super (left, right);
  }
  public double evaluate () {
    return this.left.evaluate () + this.right.evaluate ();
  }
}