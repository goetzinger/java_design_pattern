package behaviour.interpreter.expression;
public class TimesExpression extends BinaryExpression {
  public TimesExpression (Expression left, Expression right) {
    super (left, right);
  }
  public double evaluate () {
    return this.left.evaluate () * this.right.evaluate ();
  }
}