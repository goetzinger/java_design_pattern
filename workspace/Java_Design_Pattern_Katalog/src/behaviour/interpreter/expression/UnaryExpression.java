package behaviour.interpreter.expression;
public abstract class UnaryExpression extends Expression {
  public final Expression expr;
  public UnaryExpression (Expression expr) {
    this.expr = expr;
  }
}
