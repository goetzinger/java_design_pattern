package behaviour.interpreter.parsing;
public class ExpressionParser {

  private final Scanner scanner;
  private Symbol symbol;

  public ExpressionParser (Scanner scanner) {
    this.scanner = scanner;
    this.read ();
  }
  
  private void read () {
    this.symbol = scanner.read ();
  }
  
  private boolean isOperator (char op) {
    return this.symbol != null 
            &&this.symbol.isSpecial () && this.symbol.getSpecial () == op;
  }

  public double parseExpression () {
    double value = this.parseTerm ();
    while (this.isOperator ('+') || this.isOperator ('-')) {
      boolean plus = this.isOperator ('+');
      this.read ();
      double v = this.parseTerm ();
      if (plus) value += v; else value -= v;
    }
    return value;
  }

  public double parseTerm () {
    double value = this.parseFactor ();
    while (this.isOperator ('*') || this.isOperator ('/')) {
      boolean times = this.isOperator ('*');
      this.read ();
      double v = this.parseFactor ();
      if (times) value *= v; else value /= v;
    }
    return value;
  }

  public double parseFactor () {
    boolean minus = false;
    if (this.isOperator ('-')) { // unary minus 
      minus = true;
      this.read ();
    }
    double value;
    if (this.symbol.isNumber ()) {
      value = this.symbol.getNumber ();
      this.read ();
    }
    else if (this.isOperator ('(')) {
      this.read ();
      value = this.parseExpression ();
      if (! this.isOperator (')'))
        throw new RuntimeException ("')' expected");
      this.read ();
    }
    else
      throw new RuntimeException ("number or '(' expected");
    if (minus) value = -value;
    return value;
  }
}
