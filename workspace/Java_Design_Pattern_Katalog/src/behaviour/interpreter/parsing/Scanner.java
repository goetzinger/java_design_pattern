package behaviour.interpreter.parsing;
import java.io.Reader;

class Scanner {

	public static abstract class AbstractSymbol implements Symbol { 
	  public boolean isNumber ()					{ return false; }
	  public boolean isIdentifier ()			{ return false; }
	  public boolean isSpecial ()					{ return false; }
	  public double getNumber ()					{ throw new RuntimeException (); }
	  public String getIdentifier ()			{ throw new RuntimeException (); }
	  public char getSpecial ()						{ throw new RuntimeException (); }
	}

	public static class NumberSymbol extends AbstractSymbol { 
		private final double value;
		public NumberSymbol (double v)			{ this.value = v; }
	  public boolean isNumber ()					{ return true; }
	  public double getNumber ()					{ return this.value; }
	  public Object getValue ()						{ return this.value; }
	}
	public static class IdentifierSymbol extends AbstractSymbol { 
		private final String value;
		public IdentifierSymbol (String v)	{ this.value = v; }
	  public boolean isIdentifier ()			{ return true; }
	  public String getIdentifier ()			{ return this.value; }
	  public Object getValue ()						{ return this.value; }
	}
	public static class SpecialSymbol extends AbstractSymbol { 
		private final char value;
		public SpecialSymbol (char v)				{ this.value = v; }
	  public boolean isSpecial ()					{ return true; }
	  public char getSpecial ()						{ return this.value; }
	  public Object getValue ()						{ return this.value; }
	}

  private final Reader reader;
  private final String specialCharacters;
  private int currentChar;
  private Symbol currentSymbol;
  private final StringBuffer buf = new StringBuffer ();

  public Scanner (String specialCharacters, Reader reader) {
    this.specialCharacters = specialCharacters;
    this.reader = reader;
    this.readChar ();
  }

  public Symbol read () {
    while (Character.isWhitespace(this.currentChar))
      this.readChar ();
    if (this.currentChar == -1) 
      return null;
    if (Character.isDigit (this.currentChar)) 
      return this.readNumber ();
    if (Character.isLetter(this.currentChar)) 
      return this.readIdentifier ();
    if (this.specialCharacters.indexOf (this.currentChar) >= 0) {
      Symbol symbol = new SpecialSymbol ((char) this.currentChar);
      this.readChar ();
      return symbol;
    }
    throw new RuntimeException ("bad char: '" + (char) this.currentChar + "'");
  }

  private Symbol readNumber () {
    this.buf.setLength (0);
    while (Character.isDigit(this.currentChar)) {
      this.buf.append ((char)this.currentChar);
      this.readChar ();
    }
    if (this.currentChar == '.') {
      this.buf.append ((char)this.currentChar);
      this.readChar ();
      while (Character.isDigit(this.currentChar)) {
        this.buf.append ((char)this.currentChar);
        this.readChar ();
      }
    }
    if (Character.isLetter(this.currentChar))
      throw new RuntimeException ("bad number: " + this.buf + (char) this.currentChar);
    return new NumberSymbol (Double.parseDouble (this.buf.toString()));
  }

  private Symbol readIdentifier () {
    this.buf.setLength (0);
    this.buf.append ((char) this.currentChar);
    this.readChar ();
    while (Character.isLetterOrDigit(this.currentChar)) {
      this.buf.append ((char) this.currentChar);
      this.readChar ();
    }
    return new IdentifierSymbol (this.buf.toString ());
  }

  private void readChar () {
    try {
      this.currentChar = this.reader.read ();
    }
    catch (Exception e) {
      throw new RuntimeException (e);
    }
  }
}
