package behaviour.interpreter.parsing;
public interface Symbol { 

  public abstract boolean isNumber ();
  public abstract boolean isIdentifier ();
  public abstract boolean isSpecial ();

  public abstract double getNumber ();
  public abstract String getIdentifier ();
  public abstract char getSpecial ();

  public abstract Object getValue ();
}
