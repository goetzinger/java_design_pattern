package behaviour.iterator.array;
import java.util.Iterator;

public class Application {
  public static void main (String [] args) {
    
    SimpleArrayList<String> names = new SimpleArrayList<String> ();

    names.add ("schall");
    names.add ("rauch");
    names.add ("hinz");
    names.add ("kunz");
    
    for (String name : names)
      System.out.println (name);
  } 
}
