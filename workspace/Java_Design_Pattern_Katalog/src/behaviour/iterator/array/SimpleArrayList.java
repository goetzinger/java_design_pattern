package behaviour.iterator.array;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleArrayList<T> implements Iterable<T> {

  private T [] elements = (T []) new Object [2];
  private int count;
  
  public void add (T element) {
    if (this.count == elements.length) {
      T [] newElements = (T []) new Object [this.elements.length * 2];
      System.arraycopy (this.elements, 0, newElements, 0, this.elements.length);
      this.elements = newElements;
    }
    this.elements [this.count] = element;
    this.count++;
  }
  
  public int size () {
    return this.count;
  }
  public T get (int index) {
    if (index < 0 || index >= this.count)
      throw new IndexOutOfBoundsException ();
    return this.elements [index];
  }
  public Iterator<T> iterator () {
    return new Iterator<T> () {
      private int current = -1;
      public boolean hasNext () {
        return this.current < SimpleArrayList.this.count - 1;
      } 
      public T next () {
        if (! this.hasNext ())
          throw new NoSuchElementException ();
        this.current++;
        return SimpleArrayList.this.elements [this.current];
      }
      public void remove () {
        throw new UnsupportedOperationException ();
      }
    };
  }
}
