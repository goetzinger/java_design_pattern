package behaviour.iterator.intervall;
import java.util.Iterator;

public class Application {
  public static void main (String [] args) {
    
    Interval interval = new Interval (5, 10);
    
    Iterator<Integer> iterator = interval.iterator ();
    while (iterator.hasNext ()) {
      int value = iterator.next ();
      System.out.println (value);
    }
    
    for (int value : interval)
      System.out.println (value);
  } 
}
