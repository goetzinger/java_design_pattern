package behaviour.iterator.intervall;
import java.util.Iterator;
import java.util.NoSuchElementException;

class IntervalIterator implements Iterator<Integer> {
  private final Interval interval;
  private int current;
  IntervalIterator (Interval interval) {
    this.interval = interval;
    this.current = this.interval.lowerBound - 1;
  }
  public boolean hasNext () {
    return this.current < this.interval.upperBound;
  } 
  public Integer next () {
    if (! this.hasNext ())
      throw new NoSuchElementException ();
    this.current++;
    return this.current;
  }
  public void remove () {
    throw new UnsupportedOperationException ();
  }
}


public class Interval implements Iterable<Integer> {

  public final int lowerBound;
  public final int upperBound;
  
  public Interval (int lowerBound, int upperBound) {
    this.lowerBound = lowerBound;
    this.upperBound = upperBound;
  }
  
  public Iterator<Integer> iterator () {
    return new IntervalIterator (this);
  }
}