package behaviour.iterator.list;
import java.util.Iterator;

public class Application {
  public static void main (String [] args) {
    
    SimpleLinkedList<String> names = new SimpleLinkedList<String> ();

    names.add ("schall");
    names.add ("rauch");
    names.add ("hinz");
    names.add ("kunz");
    
    for (String name : names)
      System.out.println (name);
  } 
}
