package behaviour.iterator.list;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class SimpleLinkedList<T> implements Iterable<T> {

  private static class Node<T> {
    T element;
    Node<T> next;
    Node (T element) {
      this.element = element;
    }
  }

  private Node<T> first;
  private Node<T> last;
  
  public void add (T element) {
    Node<T> node = new Node<T> (element);
    if (first == null) 
      first = node;
    else
      last.next = node;
    last = node;
  }
  
  public Iterator<T> iterator () {
    return new Iterator<T> () {
      private Node<T> currentNode;
      public boolean hasNext () {
        return this.currentNode != SimpleLinkedList.this.last;
      } 
      public T next () {
        if (! this.hasNext ())
          throw new NoSuchElementException ();
        if (this.currentNode == null)
          this.currentNode = SimpleLinkedList.this.first;
        else
          this.currentNode = this.currentNode.next;
        return this.currentNode.element;
      }
      public void remove () {
        throw new UnsupportedOperationException ();
      }
    };
  }
}
