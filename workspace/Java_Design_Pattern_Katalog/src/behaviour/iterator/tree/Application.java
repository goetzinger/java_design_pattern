package behaviour.iterator.tree;
import java.util.Iterator;

public class Application {
  public static void main (String [] args) {
    
    Node<String> node1 = new Node<String> ("1");
    Node<String> node11 = new Node<String> ("11");    
    Node<String> node12 = new Node<String> ("12");    
    Node<String> node111 = new Node<String> ("111");    
    Node<String> node112 = new Node<String> ("112");    
    Node<String> node121 = new Node<String> ("121");    
    Node<String> node122 = new Node<String> ("122");    

    node1.add (node11);
    node1.add (node12);
    node11.add (node111);
    node11.add (node112);
    node12.add (node121);
    node12.add (node122);

    node1.traverse (new Node.Handler<String> () {
      public void handle (Node<String> node) {
        int depth = node.getDepth ();
        while (depth --> 0)
          System.out.print ('\t');
        System.out.println (node);
      }
    });
    
    for (Node<String> node : node1) {
      int depth = node.getDepth ();
      while (depth --> 0)
        System.out.print ('\t');
      System.out.println (node);
    }
  } 
}
