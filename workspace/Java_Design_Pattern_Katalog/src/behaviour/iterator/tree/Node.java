package behaviour.iterator.tree;
import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Node<T> implements Iterable<Node<T>> {

  public static interface Handler<E> {
    public void handle (Node<E> node);
  }

  private T element;
  private Node<T> parent;
  private ArrayList<Node<T>> children;
  
  public Node (T element) {
    if (element == null)
      throw new NullPointerException ();
    this.element = element;
  }
  
  public void add (Node<T> node) {
    if (this.children == null)
      this.children = new ArrayList<Node<T>> ();
    this.children.add (node);
    node.parent = this;
  }

  public int getChildCount () {
    return this.children == null ? 0 : this.children.size ();
  }
  
  public Node<T> getChild (int index) {
    return this.children.get (index);
  }

  public Node<T> getParent () {
    return this.parent;
  }
  
  public T getElement () {
    return this.element;
  } 
  
  public int getDepth () {
    int depth = 0;
    Node<T> node = this;
    while (node.parent != null) {
      node = node.parent;
      depth++;
    }
    return depth; 
  }

  public int getIndexOfChild (Node<T> child) {
    for (int i = 0; i < this.getChildCount (); i++)
      if (this.getChild (i) == child)
        return i;
    return -1;
  }

  public boolean hasChildren () {
    return this.getChildCount () > 0;
  }

  public Node<T> getFirstChild () {
    if (! this.hasChildren ())
      return null;
    return this.getChild (0);
  }
  
  public Node<T> getLastChild () {
    if (! this.hasChildren ())
      return null;
    return this.getChild (this.getChildCount () - 1);
  }

  @Override public String toString () {
    return this.element.toString ();
  }
  
  public void traverse (Handler<T> handler) {
    handler.handle (this);
    for (int i = 0; i < this.getChildCount (); i++) 
      this.getChild (i).traverse (handler);
  }
  
  public Iterator<Node<T>> iterator () {
    return new Iterator<Node<T>> () {
      private Node<T> currentNode;
      private Node<T> lastNode;
      public boolean hasNext () {
        if (this.lastNode == null) {
          this.lastNode = Node.this;
          while (this.lastNode.hasChildren ()) 
            this.lastNode = this.lastNode.getLastChild ();
        }
        return this.currentNode != this.lastNode;
      } 
      public Node<T> next () {
        if (! this.hasNext ())
          throw new NoSuchElementException ();
        if (this.currentNode == null) 
          return this.currentNode = Node.this;
        if (this.currentNode.hasChildren ()) 
          return this.currentNode = this.currentNode.getFirstChild ();
        while (true) {
          Node<T> parent = this.currentNode.getParent ();
          int index = parent.getIndexOfChild (this.currentNode);
          if (index < parent.getChildCount () - 1) 
            return this.currentNode = parent.getChild (index + 1);
          this.currentNode = parent;
        }
      }
      public void remove () {
        throw new UnsupportedOperationException ();
      }
    };
  }
}
