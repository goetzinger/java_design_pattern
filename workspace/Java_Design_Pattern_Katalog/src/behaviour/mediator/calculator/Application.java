package behaviour.mediator.calculator;
import javax.swing.*;

public class Application {
	public static void main (String [] args) throws Exception {
   	UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
		MainFrame frame = new MainFrame ();
		frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
		frame.setVisible (true);
	}
}
