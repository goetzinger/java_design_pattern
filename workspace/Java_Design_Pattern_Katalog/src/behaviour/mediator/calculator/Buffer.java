package behaviour.mediator.calculator;
import java.math.BigDecimal;

public class Buffer {

  private StringBuffer buf0 = new StringBuffer ();
  private StringBuffer buf1 = new StringBuffer ();
  private boolean comma;
  
  public boolean reset () {
    this.buf0.setLength (0);
    this.buf1.setLength (0);
    this.comma = false;
    return true;
  }
  
  public boolean add (char digit) {
    if (this.comma) 
      buf1.append (digit);
    else if (digit > '0' || buf0.length () > 0) 
      buf0.append (digit);
    return true;
  }

  public boolean comma () {
    if (this.comma)
      return false;
    this.comma = true;
    return true;
  }

  public boolean plusMinus () {
    if (this.buf0.length () == 0)
      return true;
    if (this.buf0.charAt (0) == '-') 
      this.buf0.deleteCharAt (0);
    else
      this.buf0.insert (0, '-');
    return true;
  }

  public boolean remove () {
    if (this.comma) {
      if (this.buf1.length () > 0) {
        this.buf1.setLength (this.buf1.length () - 1);
        return true;
      }
      this.comma = false;
    }
    if (this.buf0.length () > 0) {
      this.buf0.setLength (this.buf0.length () - 1);
      return true;
    }
    return false;
  }

  public BigDecimal getValue () {
    return new BigDecimal (this.getString ().replace (',', '.'));
  }
  
  public void setValue (BigDecimal value) {
    this.reset ();
    String s = String.valueOf (value);
    int index = s.indexOf ('.');
    if (index < 0) 
      buf0.append (s);
    else {
      buf0.append (s.substring (0, index));
      s = s.substring (index + 1);
      if (s.length () > 1 || s.charAt (0) != '0')
        buf1.append (s);
    }
  }

  public String getString () {
    String s = buf0.toString ();
    if (s.length () == 0)
      s = "0";
    s += ",";
    s += buf1.toString ();
    return s;
  }
}