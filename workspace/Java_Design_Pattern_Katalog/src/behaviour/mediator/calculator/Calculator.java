package behaviour.mediator.calculator;
import java.math.BigDecimal;

public class Calculator {
  
  private static final BigDecimal zero = new BigDecimal (0);
  private BigDecimal value = zero;

  public void reset () {
    this.value = zero;
  }
  public BigDecimal getValue () {
    return this.value;
  } 
  public void add (BigDecimal value) {
    this.value = this.value.add (value);
  }
  public void subtract (BigDecimal value) {
    this.value = this.value.subtract (value);
  }
  public void multiply (BigDecimal value) {
    this.value = this.value.multiply (value);
  }
  public void divide (BigDecimal value) {
    if (! value.equals (zero))
      this.value = this.value.divide (value);
  }
}