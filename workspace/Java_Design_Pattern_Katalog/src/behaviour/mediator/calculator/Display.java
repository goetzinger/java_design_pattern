package behaviour.mediator.calculator;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.border.*;
import javax.swing.event.*;

public class Display extends JPanel {
  
  private final JLabel label = new JLabel ();
  
  public Display (Mediator mediator) {
    mediator.setDisplay (this);
    this.setLayout (new BorderLayout ());
    this.label.setFont (new Font ("", Font.PLAIN, 18));
    this.setBackground (Color.white);
    this.label.setBorder (BorderFactory.createBevelBorder (BevelBorder.LOWERED));
    this.add (this.label, BorderLayout.CENTER);
    this.setPreferredSize (new Dimension (100, 25));
    this.label.setHorizontalAlignment (SwingConstants.RIGHT);
  }
  
  public void setString (String str) {
    this.label.setText (str);
  }
}


