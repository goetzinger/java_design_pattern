package behaviour.mediator.calculator;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class Keyboard extends JPanel {

  private final JButton buttonBack = new JButton ("B");
  private final JButton buttonClear = new JButton ("C");
  private final JButton buttonComma = new JButton (",");
  private final JButton buttonPlusMinus = new JButton ("+/-");
  private final JButton digitButtons [] = new JButton [10];
  private final JButton operationButtons [] = new JButton [Operation.values ().length];

  private final Mediator mediator;
  
  public Keyboard (Mediator mediator) {
    this.mediator = mediator;
    this.addComponents ();
    this.registerListeners ();
    int n = Operation.values ().length;
  }
  
  private void addComponents () {
    for (int i = 0; i < this.digitButtons.length; i++) {
      this.digitButtons [i] = new JButton (String.valueOf (i));
    }
    for (int i = 0; i < this.operationButtons.length; i++) {
      this.operationButtons [i] = new JButton (Operation.values () [i].toString ());
      this.operationButtons [i].setForeground (Color.red);
    }
    this.setPreferredSize (new Dimension (250, 200));
    this.setLayout (new GridLayout (5, 4));
    this.add (new JLabel ());
    this.add (this.buttonBack); this.buttonBack.setForeground (Color.blue);
    this.add (this.buttonClear); this.buttonClear.setForeground (Color.blue);
    this.add (new JLabel ());
    for (int i = 7; i <= 9; i++) this.add (this.digitButtons [i]);
    this.add (this.operationButtons [3]);
    for (int i = 4; i <= 6; i++) this.add (this.digitButtons [i]);
    this.add (this.operationButtons [2]);
    for (int i = 1; i <= 3; i++) this.add (this.digitButtons [i]);
    this.add (this.operationButtons [1]);
    this.add (this.digitButtons [0]);
    this.add (this.buttonPlusMinus);
    this.add (this.buttonComma);    
    this.add (this.operationButtons [0]);
    Font f = new Font ("", Font.PLAIN, 18);
    for (int i = 0; i < this.getComponentCount (); i++)
      this.getComponent (i).setFont (f);
  }
  
  private void registerListeners () {
    for (int i = 0; i < this.digitButtons.length; i++) {
      final int index = i;
      this.digitButtons [i].addActionListener (new ActionListener () {
        public void actionPerformed (ActionEvent e) {
          Keyboard.this.mediator.onDigit ((char) (index + '0'));
          Keyboard.this.requestFocus ();
        }
      });
    }
    for (int i = 0; i < this.operationButtons.length; i++) {
      final int index = i;
      this.operationButtons [i].addActionListener (new ActionListener () {
        public void actionPerformed (ActionEvent e) {
          Keyboard.this.mediator.onOperation (Operation.values () [index]);
          Keyboard.this.requestFocus ();
        }
      });
    }
    this.buttonComma.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        Keyboard.this.mediator.onComma ();
        Keyboard.this.requestFocus ();
      }
    });
    this.buttonPlusMinus.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        Keyboard.this.mediator.onPlusMinus ();
        Keyboard.this.requestFocus ();
      }
    });
    this.buttonBack.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        Keyboard.this.mediator.onBack ();
        Keyboard.this.requestFocus ();
      }
    });
    this.buttonClear.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        Keyboard.this.mediator.onClear ();
        Keyboard.this.requestFocus ();
      }
    });
    this.addKeyListener (new KeyAdapter () {
      @Override public void keyTyped (KeyEvent e) {
        char ch = e.getKeyChar ();
        if (Character.isDigit (ch))
          Keyboard.this.mediator.onDigit (ch);
        else if (ch == ',')
          Keyboard.this.mediator.onComma ();
        else if (ch == 'C' || ch == 'c')        
          Keyboard.this.mediator.onClear ();
        else if (ch == 'B' || ch == 'b' || ch == KeyEvent.VK_BACK_SPACE)        
          Keyboard.this.mediator.onBack ();
        else {
          for (Operation op : Operation.values ()) {
            if (op.toString ().equals (String.valueOf (ch))) {
              Keyboard.this.mediator.onOperation (op);
              break;
            }
          }
        }
      }
    });
    this.addComponentListener (new ComponentAdapter () {
      @Override public void componentResized (ComponentEvent e) {
        Keyboard.this.requestFocus ();
      }
    });
  }
}
