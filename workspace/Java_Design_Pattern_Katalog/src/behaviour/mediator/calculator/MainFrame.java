package behaviour.mediator.calculator;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class MainFrame extends JFrame {

	private Mediator mediator = new Mediator ();
	private Keyboard tastatur = new Keyboard (this.mediator);
	private Display display = new Display (this.mediator);
	
	public MainFrame () {
		super ("Rechner");
		this.setLayout (new BorderLayout ());
		this.add (this.display, BorderLayout.NORTH);
		this.add (this.tastatur, BorderLayout.CENTER);
		this.pack ();
	}
}