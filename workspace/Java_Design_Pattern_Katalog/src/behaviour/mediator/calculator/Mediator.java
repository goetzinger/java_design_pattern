package behaviour.mediator.calculator;
import java.awt.Toolkit;
import java.math.BigDecimal;

public class Mediator {
  
  private Display display;
  private final Calculator calculator = new Calculator ();
  private final Buffer buffer = new Buffer ();
  private Operation lastOperation = Operation.ADD;
  private boolean wasOperation = false;
  
  public void setDisplay (Display display) {
    this.display = display ;
    this.showResult (true);
  }
  
  public void onDigit (char digit) {
    if (this.wasOperation) 
      this.showResult (this.buffer.reset ());
    this.wasOperation = false;
    this.showResult (this.buffer.add (digit));
  }

  public void onOperation (Operation operation) {
    if (this.wasOperation) {
      this.lastOperation = operation;
      return;   
    }
    this.wasOperation = true;
    BigDecimal value = this.buffer.getValue ();
    switch (this.lastOperation) {
      case ADD:
        this.calculator.add (value);
        break;
      case SUBTRACT:
        this.calculator.subtract (value);
        break;
      case MULTIPLY:
        this.calculator.multiply (value);
        break;
      case DIVIDE:
        this.calculator.divide (value);
        break;
    }
    this.buffer.setValue (this.calculator.getValue ());
    this.lastOperation = operation;
    this.showResult (true);
  }
  public void onComma () {
    if (this.wasOperation) 
      this.showResult (this.buffer.reset ());
    this.wasOperation = false;
    this.showResult (this.buffer.comma ());
  }
  public void onPlusMinus () {
    if (this.wasOperation) 
      return;
    this.showResult (this.buffer.plusMinus ());
  }
  public void onBack () {
    if (this.wasOperation) 
      return;
    this.showResult (this.buffer.remove ());
  }
  public void onClear () {
    this.wasOperation = false;
    this.calculator.reset ();
    this.showResult (this.buffer.reset ());
    this.lastOperation = Operation.ADD;
  }
  private void showResult (boolean done) {
    if (done) 
      this.display.setString (this.buffer.getString ());
    else
      Toolkit.getDefaultToolkit ().beep ();
  }
}