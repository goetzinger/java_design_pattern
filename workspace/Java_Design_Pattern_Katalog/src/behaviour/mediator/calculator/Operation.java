package behaviour.mediator.calculator;
public enum Operation {
  ADD ("+"),
  SUBTRACT ("-"),
  MULTIPLY ("*"),
  DIVIDE ("/");
  private final String label;
  Operation (String label) {
    this.label = label;
  }
  @Override public String toString () {
    return this.label;
  }
}
