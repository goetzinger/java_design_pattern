package behaviour.memento.pointeditor;
import java.awt.Color;

public class Element {
  public final int x;
  public final int y;
  public final int radius;
  public final Color color;
  public Element (int x, int y, int radius, Color color) {
    this.x = x;
    this.y = y;
    this.radius = radius;
    this.color = color;
  }
}