package behaviour.memento.pointeditor;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

public class MainFrame extends JFrame {

  private class ControlPanel extends JPanel {
    
    private JButton buttonSave = new JButton ("save");
    private JButton buttonRestore = new JButton ("restore");
    private JRadioButton radioRed = new JRadioButton ("red");
    private JRadioButton radioGreen = new JRadioButton ("green");
    private JRadioButton radioBlue = new JRadioButton ("blue");
    private ButtonGroup groupColor = new ButtonGroup ();
    private JRadioButton radioBig = new JRadioButton ("big");
    private JRadioButton radioSmall = new JRadioButton ("small");
    private ButtonGroup groupSize = new ButtonGroup ();
        
    public ControlPanel () {
      this.setLayout (new BoxLayout (this, BoxLayout.X_AXIS));
      this.add (this.buttonSave);
      this.add (this.buttonRestore);
      this.add (this.radioRed);
      this.add (this.radioGreen);
      this.add (this.radioBlue);
      this.add (this.radioBig);
      this.add (this.radioSmall);
      this.groupColor.add (this.radioRed);
      this.groupColor.add (this.radioGreen);
      this.groupColor.add (this.radioBlue);
      this.groupSize.add (this.radioBig);
      this.groupSize.add (this.radioSmall);
      this.radioBig.setSelected (true);
      this.radioRed.setSelected (true);
      this.buttonSave.addActionListener (new ActionListener () {
        public void actionPerformed (ActionEvent e) {
          MainFrame.this.picturePanel.onSave ();
        }
      });
      this.buttonRestore.addActionListener (new ActionListener () {
        public void actionPerformed (ActionEvent e) {
          MainFrame.this.picturePanel.onRestore ();
        }
      });
    }
    public Color getColor () {
      if (this.radioRed.isSelected ()) return Color.red;
      if (this.radioGreen.isSelected ()) return Color.green;
      if (this.radioBlue.isSelected ()) return Color.blue;
      return Color.black;
    }
    public int getRadius () {
      if (this.radioBig.isSelected ()) return 12;
      if (this.radioSmall.isSelected ()) return 8;
      return 1;
    }     
  }

  private class PicturePanel extends JPanel {
    private Picture picture = new Picture ();
    private PictureMementoStore mementoStore = new PictureMementoStore (this.picture);
    private final int radius = 8;
    public PicturePanel () {
      this.setBackground (Color.white);
      this.addMouseListener (new MouseAdapter () {
        @Override public void mouseReleased (MouseEvent e) {
          PicturePanel.this.onMouseReleased (e);
        }
      });
    }
    @Override public void paint (Graphics g) {
      super.paint (g);
      for (Element e : this.picture) {
        g.setColor (e.color);
        g.fillOval (e.x - e.radius, e.y - e.radius, 
                    2 * e.radius, 2 * e.radius);
      }
    }
    private void onMouseReleased (MouseEvent event) {
      int x = event.getX ();
      int y = event.getY ();
      Element e = this.picture.get (x, y);
      if (e != null) 
        this.picture.remove (e);
      else {
        Color color = MainFrame.this.controlPanel.getColor ();
        int radius = MainFrame.this.controlPanel.getRadius ();
        this.picture.add (new Element (x, y, radius, color));
      }
      this.repaint ();
    }
    private void onSave () {
      this.mementoStore.save ();
      this.repaint ();
    }
  
    private void onRestore () {
      this.mementoStore.restore ();
      this.repaint ();
    }
  }

  private ControlPanel controlPanel = new ControlPanel ();
  private PicturePanel picturePanel = new PicturePanel ();
  
  public MainFrame () {
    this.setLayout (new BorderLayout ());
    this.picturePanel.setPreferredSize (new Dimension (500, 250));
    this.add (this.controlPanel, BorderLayout.NORTH);
    this.add (this.picturePanel, BorderLayout.CENTER);
    this.pack ();
  }
}