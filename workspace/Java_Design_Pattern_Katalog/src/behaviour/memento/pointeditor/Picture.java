package behaviour.memento.pointeditor;
import java.util.ArrayList;
import java.util.Iterator;
import java.awt.Color;

public class Picture implements Iterable<Element> {
  
  private static class PictureMementoImpl implements PictureMemento {
    private final int [] array;
    private PictureMementoImpl (ArrayList<Element> elements) {
      this.array = new int [elements.size () * 4];
      int index = 0;
      for (Element e : elements) {
        array [index++] = e.x;
        array [index++] = e.y;
        array [index++] = e.radius;
        array [index++] = e.color.getRGB ();
      }
    }
    private ArrayList<Element> getState () {
      int n = this.array.length / 4;
      ArrayList<Element> elements = new ArrayList<Element> (n);
      int index = 0;
      for (int i = 0; i < n; i++) {
        int x = this.array [index++];
        int y = this.array [index++];
        int radius = this.array [index++];
        int rgb = this.array [index++];
        elements.add (new Element (x, y, radius, new Color (rgb)));
      }
      return elements;
    }
  }
  
  private ArrayList<Element> elements = new ArrayList<Element> ();
  public void add (Element e) {
    this.elements.add (e);
  }
  public void remove (Element e) {
    this.elements.remove (e);
  }
  public Element get (int x, int y) {
    for (Element e : this.elements) {
      if (x >= e.x - e.radius && x <= e.x + e.radius &&
          y >= e.y - e.radius && y <= e.y + e.radius)
        return e;
    }
    return null;
  }
  public Iterator<Element> iterator () {
    return this.elements.iterator ();
  }   
  public PictureMemento createMemento () {
    return new PictureMementoImpl (this.elements);
  }
  public void setMemento (PictureMemento memento) {
    this.elements = ((PictureMementoImpl) memento).getState ();
  }
}
