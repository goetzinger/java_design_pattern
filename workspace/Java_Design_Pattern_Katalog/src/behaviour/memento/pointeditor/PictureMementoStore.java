package behaviour.memento.pointeditor;
import java.util.Stack;

public class PictureMementoStore {

  private final Picture picture;
  private final Stack<PictureMemento> mementos = new Stack<PictureMemento> ();

  public PictureMementoStore (Picture picture) {
    this.picture = picture;
  }

  public void save () {
    this.mementos.push (this.picture.createMemento ());
  }
  
  public void restore () {
    if (! this.mementos.isEmpty ())
      this.picture.setMemento (this.mementos.pop ());
  }
}

