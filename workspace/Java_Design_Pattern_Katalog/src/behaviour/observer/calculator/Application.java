package behaviour.observer.calculator;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Application {
  public static void main (String [] args) throws Exception {

   	UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
    
    final Calculator calculator = new Calculator ();
    
    final JFrame frame = new JFrame ("Calculator");
    final JTextField textFieldInput = new JTextField (5);
    final JButton buttonAdd = new JButton ("Add");
    final JButton buttonSubtract = new JButton ("Subtract");
    final JTextField textFieldOutput = new JTextField (10);
    
    textFieldOutput.setEditable (false);
    frame.setLayout (new FlowLayout ());
    frame.add (textFieldInput);   
    frame.add (buttonAdd);    
    frame.add (buttonSubtract);   
    frame.add (textFieldOutput);    
    
    buttonAdd.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        calculator.add (Integer.parseInt (textFieldInput.getText ()));
      }
    });
    buttonSubtract.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        calculator.subtract (Integer.parseInt (textFieldInput.getText ()));
      }
    });
    frame.addWindowListener (new WindowAdapter () {
      @Override public void windowClosing (WindowEvent e) {
        System.exit (0);
      }
    });
    calculator.addCalculatorListener (new CalculatorListener () {
      public void stateChanged (CalculatorEvent e) {
        textFieldOutput.setText (String.valueOf (e.getValue ()));
      }
    });
    frame.pack ();
    frame.setVisible (true);
  }
}
