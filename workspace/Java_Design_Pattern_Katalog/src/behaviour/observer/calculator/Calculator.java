package behaviour.observer.calculator;
import java.util.ArrayList;

public class Calculator {
  private ArrayList<CalculatorListener> listeners = 
                    new ArrayList<CalculatorListener> ();
  private int value;
  public void add (int v) {
    this.value += v;
    this.fireStateChanged ();
  }
  public void subtract (int v) {
    this.value -= v;
    this.fireStateChanged ();
  }
  public int getValue () {
    return this.value;
  }
  private void fireStateChanged () {
    CalculatorEvent event = new CalculatorEvent (this, this.value);
    for (CalculatorListener listener : this.listeners) 
      listener.stateChanged (event);
  }
  public void addCalculatorListener (CalculatorListener listener) {
    this.listeners.add (listener);
  }
  public void removeCalculatorListener (CalculatorListener listener) {
    this.listeners.remove (listener);
  }
}