package behaviour.observer.calculator;
import java.util.EventObject;

public class CalculatorEvent extends EventObject {
  final private int value;
  public CalculatorEvent (Calculator source, int value) {
    super (source);
    this.value = value;
  }
  public int getValue () {
    return this.value;
  }
}
