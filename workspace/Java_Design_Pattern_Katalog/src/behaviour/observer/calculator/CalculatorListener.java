package behaviour.observer.calculator;
import java.util.EventListener;

public interface CalculatorListener extends EventListener {
  public abstract void stateChanged (CalculatorEvent e);
}
