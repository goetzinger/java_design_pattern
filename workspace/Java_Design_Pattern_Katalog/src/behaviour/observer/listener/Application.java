package behaviour.observer.listener;
public class Application {

  public static void main (String [] args) {
    Subject subject = new Subject ();
    subject.addSubjectListener (new ObserverA ());
    subject.addSubjectListener (new ObserverB ());
    subject.setState (333);
    subject.setState (4711);
  }
} 
