package behaviour.observer.listener;
public interface Observer {
  public abstract void update (Subject subject);
}
