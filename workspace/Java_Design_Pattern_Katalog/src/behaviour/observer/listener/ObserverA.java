package behaviour.observer.listener;
public class ObserverA implements SubjectListener {
  public void stateChanged (SubjectEvent e) {
    System.out.println ("OberserA: " + e.getSource () + " = " + e.getNewState ());
  }
}
