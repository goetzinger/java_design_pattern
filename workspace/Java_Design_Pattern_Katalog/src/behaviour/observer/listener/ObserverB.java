package behaviour.observer.listener;
public class ObserverB implements SubjectListener {
  public void stateChanged (SubjectEvent e) {
    System.out.println ("OberserB: " + e.getSource () + " = " + e.getNewState ());
  }
}
