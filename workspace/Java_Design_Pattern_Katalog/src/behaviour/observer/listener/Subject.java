package behaviour.observer.listener;
import java.util.ArrayList;

public class Subject {

  private int state;
  private ArrayList<SubjectListener> listeners 
                        = new ArrayList<SubjectListener> ();

  public void setState (int state) {
    this.state = state;
    this.notifyObservers ();
  }
  public int getState () {
    return this.state;
  }

  private void notifyObservers () {
    SubjectEvent e = new SubjectEvent (this, this.state);
    for (SubjectListener listener : this.listeners) 
      listener.stateChanged (e);
  }

  public void addSubjectListener (SubjectListener listener) {
    this.listeners.add (listener);
  }
  public void removeSubjectListener (SubjectListener listener) {
    this.listeners.remove (listener);
  }
}
