package behaviour.observer.listener;
public class SubjectEvent extends java.util.EventObject {
  private final int newState;
  public SubjectEvent (Subject source, int newState) {
    super (source);
    this.newState = newState;
  }
  public int getNewState () {
    return this.newState;
  }
}