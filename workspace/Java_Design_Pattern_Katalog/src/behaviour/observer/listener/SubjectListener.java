package behaviour.observer.listener;
public interface SubjectListener extends java.util.EventListener {
  public abstract void stateChanged (SubjectEvent e);
}