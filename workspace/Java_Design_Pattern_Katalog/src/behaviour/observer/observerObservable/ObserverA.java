package behaviour.observer.observerObservable;
import java.util.Observer;
import java.util.Observable;

public class ObserverA implements Observer {
  public void update (Observable o, Object arg) {
    Subject subject = (Subject) o;
    System.out.println ("ObserverA: new State = " + subject.getState ());
  }
}
