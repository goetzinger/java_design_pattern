package behaviour.observer.observerObservable;
import java.util.Observer;
import java.util.Observable;

public class ObserverB implements Observer {
  public void update (Observable o, Object arg) {
  	Subject subject = (Subject) o;
    System.out.println ("ObserverB: new State = " + subject.getState ());
  }
}