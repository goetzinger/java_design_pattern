package behaviour.state.door;
public class Application {
  public static void main (String [] args) {
    Door door = new Door ();
    try {
      door.open ();
      door.close ();
      door.open ();
      door.open ();
    }
    catch (IllegalStateException e) {
      System.out.println (e.getMessage ());
    }
  }
}
