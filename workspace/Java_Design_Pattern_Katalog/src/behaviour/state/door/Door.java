package behaviour.state.door;
public class Door {

  public final OpenedState openedState = new OpenedState (this);
  public final ClosedState closedState = new ClosedState (this);

  private State currentState = this.closedState;

  public State getState () {
    return this.currentState;
  }

  public void setState (State state) {
    System.out.println (this.currentState + " --> " + state);
    this.currentState = state;
  }

  public void open () {
    this.currentState.open ();
  }
  public void close () {
    currentState.close ();
  }
}
