package behaviour.state.door;
public class OpenedState extends State {

  public OpenedState (Door door) {
    super (door);
  }

  @Override public void close () {
    this.door.setState (this.door.closedState);
  }
}
