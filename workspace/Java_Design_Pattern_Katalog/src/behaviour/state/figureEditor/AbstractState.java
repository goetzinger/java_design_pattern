package behaviour.state.figureEditor;
import java.awt.event.MouseEvent;
import java.awt.Graphics2D;

public abstract class AbstractState implements State {
	public void enter () {}
	public void exit () {}
	public void mousePressed (MouseEvent e) { }
	public void mouseReleased (MouseEvent e) { }
	public void mouseDragged (MouseEvent e) { }
	public void onRemove () { }
	public void paint (Graphics2D g) { }
}
