package behaviour.state.figureEditor;
import javax.swing.*;

public class Application {
  public static void main (String [] args) throws Exception {
   	UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
    new FiguresFrame ();
  }
}
