package behaviour.state.figureEditor;
import java.awt.event.MouseEvent;
import java.awt.Point;
import java.awt.Graphics2D;
import java.awt.Color;

public class CreationState extends FiguresPaneState {

	private Figure createdFigure;
	private Class figureType;
	private SelectionMarker marker;

	public CreationState (FiguresPane figuresPane, Class figureType) {
		super (figuresPane);
		this.figureType = figureType;
	}

	@Override public void enter () {
		this.createdFigure = null;
	}
	
	@Override public void mousePressed (MouseEvent e) { 
		try {
			this.createdFigure = (Figure) this.figureType.newInstance ();
			this.marker = new SelectionMarker (this.createdFigure.getRect ());
			this.createdFigure.setLocation (e.getX (), e.getY ());
		}
		catch (Exception ex) {
			throw new RuntimeException (ex);
		}
	}

	@Override public void mouseDragged (MouseEvent e) { 
		Figure f = this.createdFigure;
		f.setSize (e.getX () - f.getX (), e.getY () - f.getY ());
		this.figuresPane.repaint ();
	}

	@Override public void mouseReleased (MouseEvent e) { 
		if (this.createdFigure.getWidth () > 0 && this.createdFigure.getHeight () > 0)
			this.figuresPane.add (this.createdFigure);
		this.createdFigure = null;
		this.figuresPane.repaint ();
	}
	
	@Override public void paint (Graphics2D g) {
		for (Figure f : this.figuresPane.getFigures ()) 
			f.draw (g);
		if (this.createdFigure != null) {
			this.createdFigure.draw (g);
			this.marker.draw (g);
		}
	}

	@Override public String toString () {
		return this.figureType.getName () + " Creation";
	}
}