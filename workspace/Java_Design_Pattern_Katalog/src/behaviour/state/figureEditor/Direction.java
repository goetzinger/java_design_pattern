package behaviour.state.figureEditor;
import java.awt.Rectangle;
import java.awt.Point;

public enum Direction {

	NORTHWEST (-1, -1),
	WEST 			(-1,  0),
	SOUTHWEST (-1, +1),

	NORTH     ( 0, -1),
	SOUTH 		( 0, +1),

	NORTHEAST (+1, -1),
	EAST 			(+1,  0),
	SOUTHEAST (+1, +1);
	
	public final int x;
	public final int y;
	
	private Direction (int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public static Direction get (int x, int y) {
		for (Direction d : values ())
			if (d.x == x && d.y == y)
				return d;
		return null;
	}

	public void resize (Rectangle r, Point p) {	
		switch (this) {
			case WEST: 			west  (r, p);								break;
			case EAST: 			east  (r, p); 							break;
			case NORTH: 		north (r, p); 							break;
			case SOUTH: 		south (r, p); 							break;
			case NORTHWEST: north (r, p); west (r, p); 	break;
			case NORTHEAST: north (r, p); east (r, p); 	break;
			case SOUTHWEST: south (r, p); west (r, p); 	break;
			case SOUTHEAST: south (r, p); east (r, p); 	break;
		}
	}	

	private static void west (Rectangle r, Point p) {
		int dx = r.x - p.x;	r.x = p.x; r.width += dx;
	}
	private static void east (Rectangle r, Point p) {
		r.width = p.x - r.x;
	}
	private static void north (Rectangle r, Point p) {
		int dy = r.y - p.y;	r.y = p.y; r.height += dy;
	}
	private static void south (Rectangle r, Point p) {
		r.height = p.y - r.y;
	}
}