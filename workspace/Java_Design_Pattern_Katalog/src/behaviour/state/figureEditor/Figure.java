package behaviour.state.figureEditor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Graphics2D;

public abstract class Figure implements Cloneable {
	
	protected Rectangle rect = new Rectangle ();

	public Figure clone () {
		try {
			Figure f = (Figure) super.clone ();
			f.rect = (Rectangle) this.rect.clone ();
			return f;
		}
		catch (Exception e) {
			return null;
		}	
	}
	public int getX () {
		return this.rect.x;
	}
	
	public int getY () {
		return this.rect.y;
	}

	public int getWidth () {
		return this.rect.width;
	}
	
	public int getHeight () {
		return this.rect.height;
	}

	public Rectangle getRect () {
		return this.rect;
	}

	public void setLocation (int x, int y) {
		this.rect.setLocation (x, y);
	}
	
	public void setSize (int w, int h) {
		this.rect.setSize (w, h);
	}
	
	public boolean contains (Point p) {
		return this.rect.contains (p);
	}
	
	public boolean isContainedIn (Rectangle r) {
		return r.contains (this.rect);
	}
	
	public boolean contains (Figure f) {
		return this.rect.contains (f.rect);
	}

	public Point translatePoint (Point p) {
		return new Point (p.x - this.rect.x, p.y - this.rect.y);
	}

	public abstract void draw (Graphics2D g);
}
