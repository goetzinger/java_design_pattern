package behaviour.state.figureEditor;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FiguresFrame extends JFrame {

  public FiguresFrame () {
    super ("Figures");
  	FiguresPane figuresPane = new FiguresPane ();
    this.setLayout (new BorderLayout ());
    ToggleBar toggleBar = new ToggleBar ();
    for (Action action : figuresPane.getToggleActions ()) 
    	toggleBar.add (action);
    this.add (toggleBar, BorderLayout.NORTH);
    this.add (figuresPane, BorderLayout.CENTER);
    this.addWindowListener (new WindowAdapter () {
      public void windowClosing (WindowEvent e) {
        System.exit (0);
      }
    });
    this.pack ();
    this.setVisible (true);
  }
}