package behaviour.state.figureEditor;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.ArrayList;

public class FiguresPane extends JPanel {

	private final State [] states = new State [] {
		new SingleSelectionState (this),
		new MultiSelectionState (this),
		new CreationState (this, Oval.class),
		new CreationState (this, Rect.class),
		new CreationState (this, Triangle.class),
	};

	private final ArrayList<Action> toggleActions = new ArrayList<Action> (this.states.length);

  public final Action removeAction = new AbstractAction ("remove") {
  	public void actionPerformed (ActionEvent e) {
			FiguresPane.this.currentState.onRemove ();
  	}
  };

	private final ArrayList<Figure> figures = new ArrayList<Figure> ();

	private State currentState;

	public final BasicStroke stroke = new BasicStroke (2);

  public FiguresPane () {
  	this.createToggleActions ();
  	this.setPreferredSize (new Dimension (600, 400));
  	this.setBackground (Color.white);
  	this.setCurrentState (this.states [0]);
		this.registerListeners ();  	
		this.registerKeys ();
  }

	private void createToggleActions () {
  	for (int i = 0; i < states.length; i++) {
  		final int index = i;
  		this.toggleActions.add (new AbstractAction (this.states [i].toString ()) {
  			public void actionPerformed (ActionEvent e) {
  				FiguresPane.this.setCurrentState (FiguresPane.this.states [index]);
  			} 
  		});
  	}
	}

	private void registerListeners () {
  	this.addMouseListener (new MouseAdapter () {
  		@Override public void mousePressed (MouseEvent e) {
  			FiguresPane.this.currentState.mousePressed (e);
  		}
  		@Override public void mouseReleased (MouseEvent e) {
  			FiguresPane.this.currentState.mouseReleased (e);
  		}
  	});
  	this.addMouseMotionListener (new MouseMotionAdapter () {
  		@Override public void mouseDragged (MouseEvent e) {
  			FiguresPane.this.currentState.mouseDragged (e);
  		}
  	});
	}
	
	private void registerKeys () {
		this.getInputMap (WHEN_IN_FOCUSED_WINDOW).put (
				KeyStroke.getKeyStroke (KeyEvent.VK_DELETE, 0), 
				this.removeAction.getValue (Action.NAME));		
    this.getActionMap ().put (
    		this.removeAction.getValue (Action.NAME), 
    		this.removeAction);
	}
	
	public Iterable<Action> getToggleActions () {
		return this.toggleActions;
	}

	public Iterable<Figure> getFigures () {
		return this.figures;
	}

	public void add (Figure f) {
		this.figures.add (f);
	}
	
	public void remove (Figure f) {
		this.figures.remove (f);
	}

	@Override public void paint (Graphics g) {
		super.paint (g);
		Graphics2D g2d = (Graphics2D) g;
		g2d.setColor (Color.black);
		g2d.setStroke (this.stroke);
		this.currentState.paint (g2d);
	}

	public void setCurrentState (State state) {
		if (this.currentState != null)
			this.currentState.exit ();	
		this.currentState = state;
		this.currentState.enter ();
		this.repaint ();
	}

	public State getCurrentState () {
		return this.currentState;
	}
}