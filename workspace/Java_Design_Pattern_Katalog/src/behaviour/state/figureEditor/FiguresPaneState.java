package behaviour.state.figureEditor;
import java.awt.event.MouseEvent;
import java.awt.Graphics2D;

public abstract class FiguresPaneState extends AbstractState {
	protected final FiguresPane figuresPane;
	public FiguresPaneState (FiguresPane figuresPane) {
		this.figuresPane = figuresPane;
	}
}