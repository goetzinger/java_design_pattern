package behaviour.state.figureEditor;
import java.util.ArrayList;
import java.awt.event.MouseEvent;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.Color;

public class MultiSelectionState extends FiguresPaneState {

	private final Rectangle rectangle = new Rectangle ();	
	private final SelectionMarker marker = new SelectionMarker (this.rectangle);
	private ArrayList<Figure> selectedFigures;

	private class SelectingState extends AbstractState {
		@Override public void mousePressed (MouseEvent e) { 
			Point p = e.getPoint ();
			rectangle.setBounds (p.x, p.y, 0, 0);
			selectedFigures = null;
			figuresPane.repaint ();
		}
		@Override public void mouseDragged (MouseEvent e) {
			Point p = e.getPoint ();
			rectangle.setSize (p.x - rectangle.x, p.y - rectangle.y);
			figuresPane.repaint ();			
		}
		@Override public void mouseReleased (MouseEvent e) {
			int x1 = Integer.MAX_VALUE, x2 = 0;
			int y1 = Integer.MAX_VALUE, y2 = 0;
			ArrayList<Figure> figures = new ArrayList<Figure> ();
			for (Figure f : figuresPane.getFigures ()) {
				Rectangle r = f.getRect ();
				if (rectangle.contains (r)) {
					if (r.x < x1) x1 = r.x;
					if (r.x + r.width > x2) x2 = r.x + r.width;
					if (r.y < y1) y1 = r.y;
					if (r.y + r.height > y2) y2 = r.y + r.height;
					figures.add (f);
				}
			}
			if (figures.size () == 0) 
				rectangle.setBounds (0, 0, 0, 0);
			else {
				rectangle.setBounds (x1, y1, x2 - x1, y2 - y1);
				selectedFigures = figures;
			}
			figuresPane.repaint ();
		}
	}

	private class MovingState extends AbstractState {
		Point lastPoint;
		@Override public void mousePressed (MouseEvent e) { 
			this.lastPoint = e.getPoint ();
		}
		@Override public void mouseDragged (MouseEvent e) {
			Point p = e.getPoint ();
			int dx = p.x - this.lastPoint.x;
			int dy = p.y - this.lastPoint.y;
			for (Figure f : selectedFigures) 
				f.setLocation (f.getX () + dx, f.getY () + dy);
			rectangle.x += dx;
			rectangle.y += dy;
			this.lastPoint = p;
			figuresPane.repaint ();
		}
	}

	private class ResizingState extends AbstractState {
		Direction direction;
		Resizer resizer;
		@Override public void mousePressed (MouseEvent e) { 
			this.direction = marker.getDirection (e.getPoint ());
			this.resizer = new Resizer (rectangle, selectedFigures);
		}
		@Override public void mouseDragged (MouseEvent e) {
			this.direction.resize (rectangle, e.getPoint ());
			this.resizer.resize (rectangle);
			figuresPane.repaint ();
		}
	}

	private SelectingState selectingState = new SelectingState ();
	private MovingState movingState = new MovingState ();
	private ResizingState resizingState = new ResizingState ();

	private State currentState;

	public MultiSelectionState (FiguresPane figuresPane) {
		super (figuresPane);
	}

	@Override public void enter () { 
		this.selectedFigures = null;
		this.rectangle.setBounds (0, 0, 0, 0);
		this.currentState = this.selectingState;
	}

	@Override public void mousePressed (MouseEvent e) { 
		Point p = e.getPoint ();
		if (this.currentState == this.selectingState) {
			if (this.selectedFigures != null) {
				if (this.marker.getDirection (p) != null) 
					this.currentState = this.resizingState;
				else if (this.rectangle.contains (p)) 
					this.currentState = this.movingState;
			}
		}
		else if (this.currentState == this.movingState) {
			if (this.marker.getDirection (p) != null) 
				this.currentState = this.resizingState;
			else if (! this.rectangle.contains (p)) 
				this.currentState = this.selectingState;
		}
		else if (this.currentState == this.resizingState) {
			if (this.marker.getDirection (p) == null) {
				if (this.rectangle.contains (p)) 
					this.currentState = this.movingState;
				else
					this.currentState = this.selectingState;
			}
		}
		this.currentState.mousePressed (e);
	}

	@Override public void mouseDragged (MouseEvent e) { 
		this.currentState.mouseDragged (e);
	}

	@Override public void mouseReleased (MouseEvent e) { 
		this.currentState.mouseReleased (e);
	}

	@Override public void onRemove () {
		if (this.selectedFigures != null) {
			for (Figure f : this.selectedFigures)
				this.figuresPane.remove (f);
			this.selectedFigures = null;
			this.rectangle.setBounds (0, 0, 0, 0);
			this.figuresPane.repaint ();
		}
	}

	@Override public void paint (Graphics2D g) {
		for (Figure f : this.figuresPane.getFigures ()) {
			g.setColor (this.rectangle.contains (f.getRect ()) 
									? this.marker.color : Color.black);
			f.draw (g);
		}
		this.marker.draw (g);
	}

	@Override public String toString () {
		return "MultiSelection";
	}
}