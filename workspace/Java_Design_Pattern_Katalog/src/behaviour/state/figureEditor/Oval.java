package behaviour.state.figureEditor;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Oval extends Figure {
	@Override public void draw (Graphics2D g) {
		Rectangle r = this.rect;
		g.drawOval (r.x, r.y, r.width, r.height);
	}
}
