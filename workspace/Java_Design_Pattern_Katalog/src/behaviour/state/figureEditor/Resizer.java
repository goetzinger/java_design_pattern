package behaviour.state.figureEditor;
import java.awt.Rectangle;
import java.util.List;
import java.util.ArrayList;

public class Resizer {
	
	private final Rectangle oldRect;
	private final List<Figure> figureList;
	private final ArrayList<Figure> oldFigureList = new ArrayList<Figure> ();
	
	public Resizer (Rectangle outer, List<Figure> figureList) {
		this.oldRect = (Rectangle) outer.clone ();
		this.figureList = figureList;
		for (Figure f : figureList)
			oldFigureList.add (f.clone ());
	}
	
	public void resize (Rectangle newRect) {
		for (int i = 0; i < oldFigureList.size (); i++) {
			Rectangle oldFigureRect = oldFigureList.get (i).getRect ();
			Rectangle newFigureRect = figureList.get (i).getRect ();

			int oldWest = oldFigureRect.x - oldRect.x;
			int oldNorth = oldFigureRect.y - oldRect.y;
			int oldEast = (oldRect.x + oldRect.width) - (oldFigureRect.x + oldFigureRect.width);
			int oldSouth = (oldRect.y + oldRect.height) - (oldFigureRect.y + oldFigureRect.height);

			double factorX = newRect.width / (double) oldRect.width;
			double factorY = newRect.height / (double) oldRect.height;

			newFigureRect.x = (int) (newRect.x + oldWest * factorX);
			newFigureRect.width = (int) (newRect.x + newRect.width - oldEast * factorX) - newFigureRect.x;
			newFigureRect.y = (int) (newRect.y + oldNorth * factorY);
			newFigureRect.height = (int) (newRect.y + newRect.height - oldSouth * factorY) - newFigureRect.y;
		}
	}
}