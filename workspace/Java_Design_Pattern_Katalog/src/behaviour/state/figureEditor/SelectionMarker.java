package behaviour.state.figureEditor;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Stroke;
import java.awt.BasicStroke;

public class SelectionMarker {
	
	public final Color color = Color.blue;

	private final Rectangle rect;
	private final Stroke stroke = new BasicStroke (1);
	private final int r = 4;
	private final int lineWidth = 2;

	public SelectionMarker (Rectangle rect) {
		this.rect = rect;
	}		
	
	private int getX (int x) {
		int mx = this.rect.x + this.rect.width / 2;
		int dx = this.rect.width / 2 + this.lineWidth / 2;
		return mx + x * dx;
	}
	
	private int getY (int y) {
		int my = this.rect.y + this.rect.height / 2;
		int dy = this.rect.height / 2 + this.lineWidth / 2;
		return my + y * dy;
	}

	public void draw (Graphics2D g) {
		if (this.rect.isEmpty ())
			return;
		Color oldColor = g.getColor ();
		BasicStroke oldStroke = (BasicStroke) g.getStroke ();
		g.setColor (this.color);
		g.setStroke (this.stroke);	
		g.drawRect (this.rect.x - this.lineWidth / 2,
								this.rect.y - this.lineWidth / 2,
								this.rect.width + this.lineWidth,
								this.rect.height + this.lineWidth);
		for (int i = -1; i <= +1; i++) 
			for (int j = -1; j <= +1; j++) 
				if (i != 0 || j != 0)
					g.fillRect (this.getX (i) - this.r, this.getY (j) - this.r, 2 * this.r, 2 * this.r);
		g.setColor (oldColor);
		g.setStroke (oldStroke);
	}

	public Direction getDirection (Point p) {
		for (int i = -1; i <= +1; i++) {
			for (int j = -1; j <= +1; j++) {
				if (i != 0 || j != 0) {
					int x = this.getX (i);
					int y = this.getY (j);
					if (p.x > x - this.r && p.x < x + this.r &&
										p.y > y - this.r && p.y < y + this.r)
						return Direction.get (i, j);
				}
			}
		}
		return null;
	}	
}