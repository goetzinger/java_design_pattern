package behaviour.state.figureEditor;
import java.awt.event.MouseEvent;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Graphics2D;
import java.awt.Color;

public class SingleSelectionState extends FiguresPaneState {

	private Figure selectedFigure;
	private SelectionMarker marker;
	private Point lastPoint;
	private Direction direction;

	public SingleSelectionState (FiguresPane figuresPane) {
		super (figuresPane);
	}

	@Override public void enter () {
		this.selectedFigure = null;
	}

	@Override public void mousePressed (MouseEvent e) { 
		Point p = e.getPoint ();
		if (this.selectedFigure == null) {
			this.selectedFigure = this.getSelectedFigure (p);
		}
		else {
			this.direction = this.marker.getDirection (p);
			if (this.direction == null) 
				this.selectedFigure = this.getSelectedFigure (p);
		}
		if (this.selectedFigure != null) {
			this.lastPoint = this.selectedFigure.translatePoint (p);
			this.marker = new SelectionMarker (this.selectedFigure.getRect ());
		}
		this.figuresPane.repaint ();
	}

	@Override public void mouseDragged (MouseEvent e) { 
		if (this.selectedFigure != null) {
			Point p = e.getPoint ();
			Rectangle r = this.selectedFigure.getRect ();
			if (this.direction == null) 
				r.setLocation (p.x - this.lastPoint.x, p.y - this.lastPoint.y);
			else 
				this.direction.resize (r, p);
			this.figuresPane.repaint ();
		}
	}

	@Override public void onRemove () {
		if (this.selectedFigure != null) {
			this.figuresPane.remove (this.selectedFigure);
			this.selectedFigure = null;
			this.figuresPane.repaint ();
		}
	}

	@Override public void paint (Graphics2D g) {
		for (Figure f: this.figuresPane.getFigures ()) {
			g.setColor (this.selectedFigure == f 
									? this.marker.color : Color.black);
			f.draw (g);
		}
		if (this.selectedFigure != null) 
			this.marker.draw (g);
	}

	@Override public String toString () {
		return "SingleSelection";
	}
	
	private Figure getSelectedFigure (Point p) {
		Figure result = null;
		for (Figure f : this.figuresPane.getFigures ()) {
			if (f.contains (p)) {
				if (result == null)
					result = f;
				else if (result.contains (f))
					result = f;
			}
		}
		return result;
	}
}