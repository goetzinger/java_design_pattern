package behaviour.state.figureEditor;
import java.awt.event.MouseEvent;
import java.awt.Graphics2D;

public interface State {
	public abstract void enter ();
	public abstract void exit ();
	public abstract void mousePressed (MouseEvent e);
	public abstract void mouseReleased (MouseEvent e);
	public abstract void mouseDragged (MouseEvent e);
	public abstract void onRemove ();
	public abstract void paint (Graphics2D g);
}
