package behaviour.state.figureEditor;
import java.awt.event.*;
import javax.swing.*;

public class ToggleBar extends JPanel {

	private final ButtonGroup buttonGroup = new ButtonGroup ();

	private boolean first = true;

  public void add (Action action) {
   	JToggleButton button = new JToggleButton ((String) action.getValue (Action.NAME));
  	this.buttonGroup.add (button);
  	button.addActionListener (action);
		this.add (button);
	  if (this.first) {
  		button.setSelected (true);
		  this.first = false;
		}
  }
}
