package behaviour.state.figureEditor;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public class Triangle extends Figure {
	@Override public void draw (Graphics2D g) {
		Rectangle r = this.rect;
		g.drawLine (r.x, r.y + r.height, r.x + r.width / 2, r.y);
		g.drawLine (r.x + r.width / 2, r.y, r.x + r.width, r.y + r.height);
		g.drawLine (r.x + r.width, r.y + r.height, r.x, r.y + r.height);
	}
}
