package behaviour.strategy.allocation;
public interface AllocStrategy {
  public abstract int initialSize ();
  public abstract int newSize (int size);
}

  