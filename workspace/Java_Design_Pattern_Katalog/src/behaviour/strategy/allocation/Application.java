package behaviour.strategy.allocation;
public class Application {
  public static void main (String [] args) {

    //ObjectArray<String> languages = new ObjectArray<String> ();

    ObjectArray<String> languages = 
          new ObjectArray<String> (new LinearAllocStrategy (2, 1));

    languages.add ("C");
    languages.add ("C++");
    languages.add ("D");
    languages.add ("Java");
    languages.add ("Eiffel");
    languages.add ("C#");
    languages.add ("Lisp");
    languages.add ("Prolog");
    languages.add ("Pascal");
    languages.add ("Modula");
    languages.add ("Oberon");
    languages.add ("Smalltalk");

    for (int i = 0; i < languages.size (); i++)
      System.out.println (languages.get (i));
  }
}