package behaviour.strategy.allocation;
public class DoubleAllocStrategy implements AllocStrategy {
  private final int initialSize;
  public DoubleAllocStrategy () {
    this (2);
  }
  public DoubleAllocStrategy (int initialSize) {
    this.initialSize = initialSize;
  }
  public int initialSize () {
    return this.initialSize;
  }
  public int newSize (int size) {
    return size * 2;
  }
}
