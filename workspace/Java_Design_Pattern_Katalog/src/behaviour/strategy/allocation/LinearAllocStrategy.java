package behaviour.strategy.allocation;
public class LinearAllocStrategy implements AllocStrategy {
  private final int initialSize;
  private final int delta;
  public LinearAllocStrategy () {
    this (2, 2);
  }
  public LinearAllocStrategy (int initialSize, int delta) {
    this.initialSize = initialSize;
    this.delta = delta;
  }
  public int initialSize () {
    return this.initialSize;
  }
  public int newSize (int size) {
    return size + this.delta;
  }
}
