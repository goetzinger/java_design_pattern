package behaviour.strategy.allocation;
public class ObjectArray<T> {
  private Object [] objects;
  private int size;
  private final AllocStrategy strategy;
  public ObjectArray (AllocStrategy strategy) {
    this.strategy = strategy;
    this.objects = new Object [strategy.initialSize ()];
  }
  public ObjectArray () {
    this (new DoubleAllocStrategy ());
  }
  public void add (T object) {
    this.ensureCapacity ();
    this.objects [this.size] = object;
    this.size++;
  }
  public int size () {
    return this.size;
  }
  public T get (int index) {
    if (index < 0 || index >= this.size)
      throw new IndexOutOfBoundsException ();
    return (T) this.objects [index];
  }
  private void ensureCapacity () {
    if (this.size < this.objects.length)
      return;
    int newSize = this.strategy.newSize (this.size);
    Object [] newObjects = new Object [newSize];
    System.arraycopy (this.objects, 0, newObjects, 0, this.objects.length);
    this.objects = newObjects;
  }
}