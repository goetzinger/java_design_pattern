package behaviour.strategy.circuit;
public class AndLogic extends Logic {
  private static AndLogic theInstance = new AndLogic ();
  public static AndLogic getInstance () {
    return theInstance;
  }
  private AndLogic () {
    super ("&&", 2, 1);
  }
  @Override public void calculate (Circuit circuit) {
    circuit.getOutput (0).setState (
        circuit.getInput (0).getState () 
        &&
        circuit.getInput (1).getState ());
  }
}