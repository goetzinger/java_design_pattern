package behaviour.strategy.circuit;
public class Application {
  public static void main (String [] args) {

    Circuit c1 = new Circuit (AndLogic.getInstance ());
    Circuit c2 = new Circuit (OrLogic.getInstance ());
    Circuit c3 = new Circuit (NotLogic.getInstance ());
    System.out.println (c1);
    System.out.println (c2);
    System.out.println (c3);
    System.out.println ();
    
    c1.getInput (0).setState (true);
    c1.getInput (1).setState (true);
    System.out.println (c1);
    System.out.println ();

    c2.getInput (0).setState (true);
    System.out.println (c2);
    System.out.println ();

    c3.getInput (0).setState (true);
    System.out.println (c3);
    System.out.println ();
  }
}