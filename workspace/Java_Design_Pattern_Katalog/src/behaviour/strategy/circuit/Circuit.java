package behaviour.strategy.circuit;
public class Circuit {
  
  private Input [] inputs;
  private Output [] outputs;
  private Logic logic;

  public Circuit (Logic logic) {
    this.setLogic (logic);
  }
  
  public void calculate () {
    this.logic.calculate (this);
  }
  
  public Logic getLogic () {
    return this.logic;
  }
  
  public void setLogic (Logic logic) {
    this.logic = logic;
    this.inputs = new Input [logic.getInputCount ()];
    for (int i = 0; i < this.inputs.length; i++)
      this.inputs [i] = new Input (this);
    this.outputs = new Output [logic.getOutputCount ()];
    for (int i = 0; i < this.outputs.length; i++)
      this.outputs [i] = new Output ();
    this.logic.calculate (this);
  }
  
  public Input getInput (int index) {
    return this.inputs [index];
  }

  public Output getOutput (int index) {
    return this.outputs [index];
  }
  
  @Override public String toString () {
    String s = this.logic.getLabel () + "\t";
    for (int i = 0; i < this.logic.getInputCount (); i++) {
      if (i > 0) s += ",";
      s += this.inputs [i].getState ();
    }
    s += " --> ";
    for (int i = 0; i < this.logic.getOutputCount (); i++) {
      if (i > 0) s += ",";
      s += this.outputs [i].getState ();
    }
    return s;
  }
}
