package behaviour.strategy.circuit;
public class Input {
  
  private boolean state;
  private final Circuit circuit;
  
  public Input (Circuit circuit) {
    this.circuit = circuit;
  }
  
  public void setState (boolean state) {
    this.state = state;
    this.circuit.calculate ();
  }
  
  public boolean getState () {
    return this.state;
  }
}