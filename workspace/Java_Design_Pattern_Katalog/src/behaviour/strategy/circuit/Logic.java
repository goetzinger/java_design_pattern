package behaviour.strategy.circuit;
public abstract class Logic {

  private final String label;
  private final int inputCount;
  private final int outputCount;

  public Logic (String label, int inputCount, int outputCount) {
    this.label = label;
    this.inputCount = inputCount;
    this.outputCount = outputCount;
  }
  
  public String getLabel () {
    return this.label;
  }
  public int getInputCount () {
    return this.inputCount;
  }
  public int getOutputCount () {
    return this.outputCount;
  }
  
  public abstract void calculate (Circuit circuit);
}
