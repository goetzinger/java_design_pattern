package behaviour.strategy.circuit;
public class NotLogic extends Logic {
  private static NotLogic theInstance = new NotLogic ();
  public static NotLogic getInstance () {
    return theInstance;
  }
  private NotLogic () {
    super ("!", 1, 1);
  }
  @Override public void calculate (Circuit circuit) {
    circuit.getOutput (0).setState (
        ! circuit.getInput (0).getState ());
  }
}