package behaviour.strategy.circuit;
public class OrLogic extends Logic {
  private static OrLogic theInstance = new OrLogic ();
  public static OrLogic getInstance () {
    return theInstance;
  }
  private OrLogic () {
    super ("||", 2, 1);
  }
  @Override public void calculate (Circuit circuit) {
    circuit.getOutput (0).setState (
        circuit.getInput (0).getState () 
        ||
        circuit.getInput (1).getState ());
  }
}