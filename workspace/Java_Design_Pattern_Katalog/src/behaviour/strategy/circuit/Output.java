package behaviour.strategy.circuit;
public class Output {
  
  private boolean state;
  
  public void setState (boolean state) {
    this.state = state;
  }
  
  public boolean getState () {
    return this.state;
  }
}