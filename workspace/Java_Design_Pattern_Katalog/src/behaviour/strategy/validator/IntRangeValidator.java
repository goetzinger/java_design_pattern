package behaviour.strategy.validator;
import javax.swing.text.JTextComponent;

public class IntRangeValidator extends Validator {
  private final int min;
  private final int max;
  public IntRangeValidator (JTextComponent component, int min, int max) {
    super (component);
    this.min = min;
    this.max = max;
  }
  public void validate () throws ValidatorException {
    try {
      int v = Integer.parseInt (this.component.getText ());
      if (v < this.min || v > this.max)
        throw new ValidatorException (
                  "[" + this.min + ", " + this.max + "]", this.component);
    }
    catch (NumberFormatException e) {
      throw new ValidatorException (e.getMessage (), this.component);
    }
  }
}
