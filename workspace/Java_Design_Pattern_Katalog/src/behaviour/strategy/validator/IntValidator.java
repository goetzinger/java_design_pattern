package behaviour.strategy.validator;
import javax.swing.text.JTextComponent;

public class IntValidator extends Validator {
  public IntValidator (JTextComponent component) {
    super (component);
  }
  public void validate () throws ValidatorException {
    String value = this.component.getText ();
    try {
      Integer.parseInt (value);
    }
    catch (NumberFormatException e) {
      throw new ValidatorException (e.getMessage (), this.component);
    }
  }
}
