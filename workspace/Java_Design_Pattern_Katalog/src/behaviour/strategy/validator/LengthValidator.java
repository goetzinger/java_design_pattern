package behaviour.strategy.validator;
import javax.swing.text.JTextComponent;

public class LengthValidator extends Validator {
  private final int minLength;
  private final int maxLength;
  public LengthValidator (JTextComponent component, int minLength, int maxLength) {
    super (component);
    this.minLength = minLength;
    this.maxLength = maxLength;
  }
  public void validate () throws ValidatorException {
    String value = this.component.getText ();
    int length = value.length ();
    if (length < this.minLength || length > this.maxLength)
        throw new ValidatorException (
          "length [" + this.minLength + "," + this.maxLength + "]", this.component);
  }
}
