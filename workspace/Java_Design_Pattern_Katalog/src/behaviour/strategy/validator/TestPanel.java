package behaviour.strategy.validator;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;

public class TestPanel extends JPanel {
  
  private final JTextField textFieldA = new JTextField (20);
  private final JTextField textFieldB = new JTextField (20);
  private final JTextField textFieldC = new JTextField (20);
  private final JButton buttonSave = new JButton ("Save");
  private final JTextField textFieldMsg = new JTextField (40);
  
  private final ValidatorList validatorList = new ValidatorList ();

  public TestPanel () {
    this.setLayout (new GridBagLayout ());
    GridBagConstraints c = new GridBagConstraints ();
    c.insets = new Insets (10, 10, 10, 10);
    c.gridy = 0; this.add (this.textFieldA, c);
    c.gridy = 1; this.add (this.textFieldB, c);
    c.gridy = 2; this.add (this.textFieldC, c);
    c.gridy = 3; this.add (this.buttonSave, c);
    c.gridy = 4; this.add (this.textFieldMsg, c);
    this.textFieldMsg.setEnabled (false);
    this.buttonSave.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        TestPanel.this.onSave ();
      }
    });
    this.validatorList.add (new IntValidator (this.textFieldA));
    this.validatorList.add (new IntRangeValidator (this.textFieldB, 1000, 2000));
    this.validatorList.add (new LengthValidator (this.textFieldC, 1, 10));
  }
  
  private void onSave () {
    try {
      this.validatorList.validateAll ();
      this.textFieldMsg.setText ("");
    }
    catch (ValidatorException e) {
      this.textFieldMsg.setText (e.toString ());
      e.getComponent ().requestFocus ();
    }
  }
}
