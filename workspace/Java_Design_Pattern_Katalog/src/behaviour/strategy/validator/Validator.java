package behaviour.strategy.validator;
import javax.swing.text.JTextComponent;

public abstract class Validator {
  protected final JTextComponent component;
  public Validator (JTextComponent component) {
    this.component = component;
  }
  public JTextComponent getComponent () {
    return this.component;
  }
  public abstract void validate () throws ValidatorException;
}
