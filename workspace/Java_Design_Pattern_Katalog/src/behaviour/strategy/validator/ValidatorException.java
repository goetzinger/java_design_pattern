package behaviour.strategy.validator;
import javax.swing.text.JTextComponent;

public class ValidatorException extends Exception {
  private final JTextComponent component;
  public ValidatorException (String msg, JTextComponent component) {
    super (msg);
    this.component = component;
  }
  public JTextComponent getComponent () {
    return this.component;
  }
  @Override public String toString () {
    return super.toString () + " value=" + this.component.getText ();
  }
}
