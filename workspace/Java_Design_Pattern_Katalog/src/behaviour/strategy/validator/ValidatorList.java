package behaviour.strategy.validator;
import java.util.ArrayList;

public class ValidatorList {
  private final ArrayList<Validator> list = new ArrayList<Validator> ();
  public void add (Validator validator) {
    this.list.add (validator);
  }
  public void validateAll () throws ValidatorException {
    for (Validator v : this.list) 
      v.validate ();
  }
}
