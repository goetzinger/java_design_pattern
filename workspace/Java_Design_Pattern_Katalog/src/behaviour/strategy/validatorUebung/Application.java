package behaviour.strategy.validatorUebung;

import java.util.ServiceLoader;

import javax.swing.JFrame;

public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Application().showFrame();

	}

	private void showFrame() {
		JFrame frame = new JFrame("Unser Validator");
		frame.setSize(300, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		ValidatorPanel validatorPanel = new ValidatorPanel();
		ValidatorStrategy validator = ServiceLoader.load(ValidatorStrategy.class).iterator().next();
		validatorPanel.setValidatorStrategy(validator);
		
		
		frame.add(validatorPanel);
		frame.setVisible(true);
	}

}
