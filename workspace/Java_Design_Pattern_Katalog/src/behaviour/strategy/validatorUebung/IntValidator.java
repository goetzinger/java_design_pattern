package behaviour.strategy.validatorUebung;

public class IntValidator implements ValidatorStrategy {

	@Override
	public String getValidationProblemsAsString(String text) {
		try {
			Integer.parseInt(text);
		} catch (NumberFormatException e) {
			return "'"+text +  "' is not a number!";
		}
		return null;
	}

}
