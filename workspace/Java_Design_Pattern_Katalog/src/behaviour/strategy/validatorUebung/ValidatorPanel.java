package behaviour.strategy.validatorUebung;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class ValidatorPanel extends JPanel {

	private JTextField inputField;
	private JButton validateButton;
	private ValidatorStrategy validator;

	public ValidatorPanel() {
		this.initializeComponents();
	}

	private void initializeComponents() {
		this.setLayout(new BorderLayout());

		// Textfeld erzeugen und hinzufügen
		inputField = new JTextField(25);
		this.add(inputField, BorderLayout.CENTER);

		// Button erzeugen und hinzufügen
		validateButton = new JButton("Validieren");
		validateButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				String text = inputField.getText();
				String validationProblemsAsString = validator
						.getValidationProblemsAsString(text);
				if (validationProblemsAsString != null)
					JOptionPane.showMessageDialog(ValidatorPanel.this,
							validationProblemsAsString);
			}
		});
		this.add(validateButton, BorderLayout.SOUTH);
	}
	
	public void setValidatorStrategy(ValidatorStrategy validator) {
		this.validator = validator;
	}

}
