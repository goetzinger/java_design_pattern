package behaviour.strategy.validatorUebung;

public interface ValidatorStrategy {
	
	String getValidationProblemsAsString(String text);

}
