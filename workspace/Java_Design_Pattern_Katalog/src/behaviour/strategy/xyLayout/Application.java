package behaviour.strategy.xyLayout;
import java.awt.event.*;
import javax.swing.*;

public class Application {
  public static void main (String [] args) throws Exception {
    UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
    JFrame frame = new JFrame ("XYLayout");
    frame.add (new TestPanel ());
    frame.addWindowListener (new WindowAdapter () {
      @Override public void windowClosing (WindowEvent e) {
        System.exit (0);
      }
    });
    frame.pack ();
    frame.setVisible (true);
  }
}
