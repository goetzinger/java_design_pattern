package behaviour.strategy.xyLayout;
import javax.swing.*;

public class TestPanel extends JPanel {
  
  private final JTextField textFieldId = new JTextField (5);
  private final JTextField textFieldName = new JTextField (20);
  private final JTextField textFieldCity = new JTextField (20);
  private final JButton buttonSave = new JButton ("Save");

  public TestPanel () {
    this.setLayout (new XYLayout (10, 20));
    this.add (new JLabel ("Id"),    new XYConstraint (0, 0));
    this.add (this.textFieldId,     new XYConstraint (1, 0));
    this.add (new JLabel ("Name"),  new XYConstraint (0, 1));
    this.add (this.textFieldName,   new XYConstraint (1, 1));
    this.add (new JLabel ("City"),  new XYConstraint (0, 2));
    this.add (this.textFieldCity,   new XYConstraint (1, 2));
    this.add (this.buttonSave,      new XYConstraint (1, 3));
  }
}
