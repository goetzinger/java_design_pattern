package behaviour.strategy.xyLayout;
public class XYConstraint {
  public final int x;
  public final int y;
  public XYConstraint (int x, int y) {
    this.x = x;
    this.y = y;
  }
}