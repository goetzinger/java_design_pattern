package behaviour.strategy.xyLayout;
import java.awt.*;
import java.util.ArrayList;

public class XYLayout implements LayoutManager2 {

  private static class Element {
    final Component comp;
    final XYConstraint constraint;
    Element (Component comp, XYConstraint constraint) {
      this.comp = comp;
      this.constraint = constraint;
    }
  }
  
  private final int hgap;
  private final int vgap;
  private final Dimension prefSize = new Dimension ();

  private ArrayList<Element> elements = new ArrayList<Element> ();
  private int columnCount;
  private int rowCount;
  private int [] prefWidth;
  private int [] prefHeight;
  
  private boolean isInit;

  public XYLayout (int hgap, int vgap) {
    this.hgap = hgap;
    this.vgap = vgap;
  }
  
  public void addLayoutComponent (Component comp, Object constraint) {
    if (! (constraint instanceof XYConstraint))
      throw new IllegalArgumentException ();
    this.elements.add (new Element (comp, (XYConstraint) constraint));  
    this.isInit = false;
  }

  public void removeLayoutComponent (Component comp) {
    for (int i = 0; i < this.elements.size (); i++) {
      if (this.elements.get (i).comp == comp) {
        this.elements.remove (i); 
        this.isInit = false;
        break;
      }
    }
  }

  public void addLayoutComponent (String name, Component comp) {
    throw new UnsupportedOperationException ();
  }

  public void layoutContainer (Container parent) {
    if (! this.isInit)
      this.init ();
    Dimension dim = parent.getSize ();
    double fx = dim.width / (double) this.prefSize.width;
    double fy = dim.height / (double) this.prefSize.height;
    for (Element e : this.elements) {
      int w = (int) (fx * prefWidth [e.constraint.x]);
      int h = (int) (fy * prefHeight [e.constraint.y]);
      int x = (int) (fx * prefX (e.constraint.x));
      int y = (int) (fy * prefY (e.constraint.y));
      int dx = (w - prefWidth [e.constraint.x]) / 2;
      int dy = (h - prefHeight [e.constraint.y]) / 2;
      e.comp.setLocation (x + dx, y + dy);
      e.comp.setSize (e.comp.getPreferredSize ());
    }
  }

  public Dimension minimumLayoutSize (Container parent) {
    if (! this.isInit)
      this.init ();
    return this.prefSize;   
  }

  public Dimension preferredLayoutSize (Container parent) {
    if (! this.isInit)
      this.init ();
    return this.prefSize;   
  }

  public Dimension maximumLayoutSize (Container target) {
    if (! this.isInit)
      this.init ();
    return this.prefSize;   
  }

  public float getLayoutAlignmentX (Container target) {
    return 0;
  }

  public float getLayoutAlignmentY (Container target) {
    return 0;
  }

  public void invalidateLayout (Container target) {
  }

  private void init () {
    this.columnCount = 0;
    this.rowCount = 0;
    for (Element e : this.elements) {
      if (e.constraint.x >= this.columnCount)
        this.columnCount = e.constraint.x + 1;
      if (e.constraint.y >= this.rowCount)
        this.rowCount = e.constraint.y + 1;
    }
    this.prefWidth = new int [this.columnCount];
    this.prefHeight = new int [this.rowCount];
    for (Element e : this.elements) {
      Dimension prefSize = e.comp.getPreferredSize ();
      int x = e.constraint.x;
      int y = e.constraint.y;
      if (this.prefWidth [x] < prefSize.width)
        this.prefWidth [x] = prefSize.width;
      if (this.prefHeight [y] < prefSize.height)
        this.prefHeight [y] = prefSize.height;
    }   
    this.prefSize.width = 0;
    for (int w : this.prefWidth) 
      this.prefSize.width += w;
    this.prefSize.height = 0;
    for (int h : this.prefHeight)
      this.prefSize.height += h;

    this.prefSize.width += (this.columnCount + 1) * this.hgap;
    this.prefSize.height += (this.rowCount + 1) * this.vgap;

    this.isInit = true;
  } 
  
  private int prefX (int column) {
    int x = this.hgap;
    for (int i = 0; i < column; i++)
      x += (this.prefWidth [i] + this.hgap);
    return x;
  }

  private int prefY (int row) {
    int y = this.vgap;
    for (int i = 0; i < row; i++)
      y += (this.prefHeight [i] + this.vgap);
    return y;
  }
}
