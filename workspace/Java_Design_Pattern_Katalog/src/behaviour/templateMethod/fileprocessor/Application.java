package behaviour.templateMethod.fileprocessor;
public class Application {

  public static void main (String [] args) {
    try {

      String filename = "Application.java";

      new PrintProcessor ().run (filename);
      
      CharCountProcessor ccp = new CharCountProcessor ();
      ccp.run (filename);
      System.out.println (ccp.getCount () + " characters");

      LineCountProcessor lcp = new LineCountProcessor ();
      lcp.run (filename);
      System.out.println (lcp.getCount () + " lines");
    }
    catch (Exception e) {
      System.out.println (e);
    }
  }
}
