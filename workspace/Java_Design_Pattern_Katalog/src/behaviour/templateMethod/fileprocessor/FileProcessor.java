package behaviour.templateMethod.fileprocessor;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public abstract class FileProcessor {

  final public void run (String filename) throws Exception {
    InputStreamReader reader = null;
    try {
      reader = new InputStreamReader (new FileInputStream (filename));
      int ch = reader.read ();
      this.initialize ();
      while(ch != -1) {
        this.process ((char) ch);
        ch = reader.read ();
      }
      this.terminate ();
    }
    finally {
      if (reader != null) {
        try {
          reader.close ();
        }
        catch (Exception e) {
          System.err.println (e);
        }
      }
    }
  }

  protected void initialize () { }

  protected abstract void process (char ch);

  protected void terminate () {}
}
