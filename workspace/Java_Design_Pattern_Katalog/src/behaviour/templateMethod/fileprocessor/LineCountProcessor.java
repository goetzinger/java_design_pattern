package behaviour.templateMethod.fileprocessor;
public class LineCountProcessor extends FileProcessor {

  private int count;

  public int getCount () {
    return this.count;
  }

  protected void initialize () {
    this.count = 0;
  }

  protected void process (char ch) {
    if (ch == '\n')
      this.count ++;
  }
}