package behaviour.templateMethod.fileprocessor;
public class PrintProcessor extends FileProcessor {

  protected void process (char ch) {
    System.out.print (ch);
  }
}