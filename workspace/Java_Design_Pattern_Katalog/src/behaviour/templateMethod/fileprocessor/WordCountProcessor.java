package behaviour.templateMethod.fileprocessor;
public class WordCountProcessor extends FileProcessor {

  private int count;
  private boolean inWord;

  public int getCount () {
    return this.count;
  }

  public void initialize () {
    this.count = 0;
    this.inWord = false;
  }
  public void process (char ch) {
    if (! this.inWord && ! Character.isWhitespace (ch)) {
      this.count++;
      this.inWord = true;
    }
    else if (this.inWord && Character.isWhitespace (ch)) {
      this.inWord = false;
    }
  }
}
