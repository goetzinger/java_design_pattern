package behaviour.templateMethod.groupChange;
public class Application {
  public static void main (String [] args) throws Exception {
    CList list = CList.sample ();
    new CGroupProcessor ().run (list.iterator ());
  }
}
