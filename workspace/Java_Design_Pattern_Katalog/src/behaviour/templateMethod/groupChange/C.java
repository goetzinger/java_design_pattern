package behaviour.templateMethod.groupChange;
public class C {

  private String key0;
  private String key1;
  private String text;
  private int value;
  
  public C (String key0, String key1, String text, int value) {
    this.key0 = key0; 
    this.key1 = key1; 
    this.text = text;
    this.value = value;
  }
  
  public String getKey0 () { 
    return this.key0; 
  }
  public String getKey1 () { 
    return this.key1; 
  }
  public String getText () { 
    return this.text; 
  }
  public int getValue () { 
    return this.value; 
  }
  @Override public String toString () {
    return this.key0 + " " + this.key1 + " " + this.text + " " + this.value;
  }
}
