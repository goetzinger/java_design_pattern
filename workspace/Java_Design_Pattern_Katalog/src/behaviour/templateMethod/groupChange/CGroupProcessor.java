package behaviour.templateMethod.groupChange;
class CGroupProcessor extends GroupProcessor<C> {

  private int sum;
  private int sum0;
  private int sum1;
  
  @Override protected int getHierarchySize () {
    return 2;
  }
  @Override protected boolean isNewGroup (int depth, C oldC, C newC) {
    switch (depth) {
      case 0: return ! (oldC.getKey0 ().equals (newC.getKey0 ()));
      case 1: return ! (oldC.getKey1 ().equals (newC.getKey1 ()));
      default: throw new RuntimeException ();
    }
  }
  @Override protected void handleBegin () {
    System.out.println (">");
    this.sum = 0;
  }
  @Override protected void handleGroupBegin (int depth, C object) {
    switch (depth) {
      case 0: 
        System.out.println ("\t> " + object.getKey0 ()); 
        this.sum0 = 0;
        break;
      case 1: 
        System.out.println ("\t\t> " + object.getKey1 ()); 
        this.sum1 = 0;
        break;
    }
  }
  @Override protected void handlePosition (int depth, C object) {
    System.out.println ("\t\t\t" + object.getText () + "\t" + object.getValue ());
    this.sum1 += object.getValue ();
  }
  @Override protected void handleGroupEnd (int depth, C object) {
    switch (depth) {
      case 1: 
        System.out.println ("\t\t< " + object.getKey1 () + "\t\t" + this.sum1); 
        this.sum0 += this.sum1;
        break;
      case 0: 
        System.out.println ("\t< " + object.getKey0 () + "\t\t\t" + this.sum0); 
        this.sum += this.sum0;
        break;
    }
  }
  @Override protected void handleEnd () {
    System.out.println ("<" + "\t\t\t\t" + this.sum);
  }
}

