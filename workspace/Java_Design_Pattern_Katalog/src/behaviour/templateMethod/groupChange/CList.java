package behaviour.templateMethod.groupChange;
import java.util.ArrayList;
import java.util.Iterator;

public class CList implements Iterable<C> {

  private final ArrayList<C> list = new ArrayList<C> ();
  
  public void add (C c) {
    this.list.add (c);
  }
  
  public Iterator<C> iterator () {
    return this.list.iterator ();
  }
  
  public static CList sample () {
    CList list = new CList ();

    list.add (new C ("A", "x", "AAAxxx", 11));
    list.add (new C ("A", "x", "AAAxxx", 21));
    list.add (new C ("A", "x", "AAAxxx", 31));

    list.add (new C ("A", "y", "AAAyyy", 12));
    list.add (new C ("A", "y", "AAAyyy", 22));
    list.add (new C ("A", "y", "AAAyyy", 32));

    list.add (new C ("A", "z", "AAAzzz", 13));
    list.add (new C ("A", "z", "AAAzzz", 23));
    list.add (new C ("A", "z", "AAAzzz", 33));

    list.add (new C ("B", "x", "BBBxxx", 14));
    list.add (new C ("B", "x", "BBBxxx", 24));
    list.add (new C ("B", "x", "BBBxxx", 34));

    list.add (new C ("B", "y", "BBByyy", 15));
    list.add (new C ("B", "y", "BBByyy", 25));
    list.add (new C ("B", "y", "BBByyy", 35));

    list.add (new C ("B", "z", "BBBzzz", 16));
    list.add (new C ("B", "z", "BBBzzz", 26));
    list.add (new C ("B", "z", "BBBzzz", 36));

    return list;
  }
}

  
