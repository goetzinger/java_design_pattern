package behaviour.templateMethod.groupChange;
import java.util.Iterator;

public abstract class GroupProcessor<T> {
  
  public final void run (Iterator<T> iterator) {
    this.handleBegin ();
    T old = null;
    T current = null;
    while (iterator.hasNext ()) {
      current = iterator.next ();
      if (old == null) {
        old = current;
        for (int i = 0; i < this.getHierarchySize (); i++)
          this.handleGroupBegin (i, current);
      }
      for (int i = 0; i < this.getHierarchySize (); i++) {
        if (this.isNewGroup (i, old, current)) {
          for (int j = this.getHierarchySize () - 1; j >= i; j--)
            this.handleGroupEnd (j, old);
          for (int j = i; j < this.getHierarchySize (); j++)
            this.handleGroupBegin (j, current);
          old = current;
        }
      }       
      this.handlePosition (this.getHierarchySize (), current);
    }
    if (current != null) {
      for (int i = this.getHierarchySize () - 1; i >= 0 ; i--)
        this.handleGroupEnd (i, current);
    }
    this.handleEnd ();
  }

  protected abstract int getHierarchySize ();
  protected abstract boolean isNewGroup (int depth, T oldObject, T newObject);
  protected abstract void handleBegin ();
  protected abstract void handleGroupBegin (int depth, T object);
  protected abstract void handlePosition (int depth, T object);
  protected abstract void handleGroupEnd (int depth, T object);
  protected abstract void handleEnd ();
}
