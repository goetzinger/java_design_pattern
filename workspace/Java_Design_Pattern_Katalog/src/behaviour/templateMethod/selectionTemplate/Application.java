package behaviour.templateMethod.selectionTemplate;
import java.sql.*;

public class Application {
  public static void main (String [] args) {
    Connection con = null;
    try {
      Class.forName ("sun.jdbc.odbc.JdbcOdbcDriver");
      con = DriverManager.getConnection ("jdbc:odbc:patterns");

      SelectionTemplate t0 = new SimplePrinter (System.out);
      t0.process (con, "select id, name from customer");
      System.out.println ();

      SelectionTemplate t1 = new SimplePrinter (System.out);
      t1.process (con, "select id, name from customer where id > ? or name = ?",
          new int [] { Types.INTEGER, Types.VARCHAR },
          new Object [] { new Integer (3000), "Nowak" });
      System.out.println ();
      
      SelectionTemplate t2 = new HtmlTablePrinter (System.out);
      t2.process (con, "select id, name from customer");
      System.out.println ();
    }
    catch (Exception e) {
      System.out.println (e);
    } 
    finally {
      try { if (con != null) con.close (); } catch (Exception e) { }
    }
  }
}

/*
1000, Nowak
3000, Rueschenpoehler

1000, Nowak

<table>
  <tr>
    <td>1000</td>
    <td>Nowak</td>
  </tr>
  <tr>
    <td>3000</td>
    <td>Rueschenpoehler</td>
  </tr>
</table>

*/
