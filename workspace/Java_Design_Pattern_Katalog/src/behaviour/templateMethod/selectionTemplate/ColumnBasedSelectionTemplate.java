package behaviour.templateMethod.selectionTemplate;

public abstract class ColumnBasedSelectionTemplate extends RowBasedSelectionTemplate {

  @Override protected final void processRow (Object [] row, int rowNum) {
    this.startRow (rowNum);
    for (int i = 0; i < row.length; i++) 
      this.processColumn (row [i], rowNum, i);
    this.endRow (rowNum);
  }

  protected void startRow (int rowNum) { }
  protected abstract void processColumn (Object obj, int rowNum, int colNum);
  protected void endRow (int rowNum) { }
}


