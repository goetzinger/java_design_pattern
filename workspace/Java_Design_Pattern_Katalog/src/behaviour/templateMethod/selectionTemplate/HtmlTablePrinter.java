package behaviour.templateMethod.selectionTemplate;
import java.io.PrintStream;

public class HtmlTablePrinter extends ColumnBasedSelectionTemplate {

  private final PrintStream out;

  public HtmlTablePrinter (PrintStream out) {
    this.out = out;
  }

  @Override protected void start () {
    this.out.println ("<table>");
  }

  @Override protected void startRow (int rowNum) {
    this.out.println ("\t<tr>");
  } 

  @Override protected void processColumn (Object obj, int rowNum, int colNum) {
    this.out.println ("\t\t<td>" + obj + "</td>");
  }

  @Override protected void endRow (int rowNum) {
    this.out.println ("\t</tr>");
  } 

  @Override protected void end () {
    this.out.println ("</table>");
  }
}

