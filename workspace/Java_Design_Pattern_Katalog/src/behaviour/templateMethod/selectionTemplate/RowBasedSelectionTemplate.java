package behaviour.templateMethod.selectionTemplate;
import java.sql.*;

public abstract class RowBasedSelectionTemplate implements SelectionTemplate {

  public final void process (Connection con, String sql) {
    this.process (con, sql, null, null);
  }

  public final void process (Connection con, String sql, int [] sqlTypes, Object [] values) {
    PreparedStatement stmt = null;
    ResultSet rs = null;
    try {
      stmt = con.prepareStatement (sql);
      if (values != null) {
        for (int i = 0; i < values.length; i++)
          stmt.setObject (i + 1, values [i], sqlTypes [i]);
      }      
      rs = stmt.executeQuery ();
      ResultSetMetaData md = rs.getMetaData ();
      int columnCount = md.getColumnCount ();
      Object [] row = new Object [columnCount];
      this.start ();
      
      for (int rowNum = 0; rs.next (); rowNum++) {
        for (int i = 0; i < columnCount; i++)
          row [i] = rs.getObject (i + 1);
        this.processRow (row, rowNum);
      }
      this.end ();
    }
    catch (SQLException e) {
      throw new RuntimeException (e);
    }
    finally {
      if (rs != null) try { rs.close (); } catch (SQLException e) { }
      if (stmt != null) try { stmt.close (); } catch (SQLException e) { }
    }
  }
  
  protected void start () { }
  protected abstract void processRow (Object [] row, int rowNum);
  protected void end () { }
}


