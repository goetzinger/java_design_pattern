package behaviour.templateMethod.selectionTemplate;
import java.sql.*;

public interface SelectionTemplate {
  public abstract void process (Connection con, String sql);
  public abstract void process (Connection con, String sql, int [] sqlTypes, Object [] values);
}
