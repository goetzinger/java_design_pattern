package behaviour.templateMethod.selectionTemplate;
import java.io.PrintStream;

public class SimplePrinter extends RowBasedSelectionTemplate {

  private final PrintStream out;

  public SimplePrinter (PrintStream out) {
    this.out = out;
  }

  @Override protected void processRow (Object [] values, int rowNum) {
    for (int i = 0; i < values.length; i++) {
      if (i > 0) 
        System.out.print (", ");
      System.out.print (values [i]);
    }
    this.out.println ();
  }
}

