package behaviour.templateMethod.simpleGroupChange;
import java.util.ArrayList;

public class Application {
  public static void main (String [] args) throws Exception {
    ArrayList<Umsatz> list = new ArrayList<Umsatz> ();
    list.add (new Umsatz (1000, 500));    
    list.add (new Umsatz (1000, 300));    
    list.add (new Umsatz (1000, 100));    
    list.add (new Umsatz (2000, 500));    
    list.add (new Umsatz (3000, 100));    
    list.add (new Umsatz (3000, 200));    
    new UmsatzProcessor ().run (list.iterator ());
  }
}
