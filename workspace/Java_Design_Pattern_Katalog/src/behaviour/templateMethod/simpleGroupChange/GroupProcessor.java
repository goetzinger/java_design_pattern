package behaviour.templateMethod.simpleGroupChange;
import java.util.Iterator;

public abstract class GroupProcessor<T> {
  
  private T read (Iterator<T> iterator) {
    return iterator.hasNext () ? iterator.next () : null;
  }

  public final void run (Iterator<T> iterator) {
    this.handleBegin ();
    T old = null;
    T current = this.read (iterator);
    while (current != null) {
      this.handleGroupBegin (current);
      old = current;
      while (current != null && ! this.isNewGroup (old, current)) {
        this.handlePosition (current);
        current = this.read (iterator);
      }
      this.handleGroupEnd (current);        
    }
    this.handleEnd ();
  }

  protected abstract boolean isNewGroup (T oldObject, T newObject);
  protected abstract void handleBegin ();
  protected abstract void handleGroupBegin (T object);
  protected abstract void handlePosition (T object);
  protected abstract void handleGroupEnd (T object);
  protected abstract void handleEnd ();
}
