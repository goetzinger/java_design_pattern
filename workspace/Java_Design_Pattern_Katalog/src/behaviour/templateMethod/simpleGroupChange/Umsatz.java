package behaviour.templateMethod.simpleGroupChange;
public class Umsatz {

  private int kdNr;
  private double umsatz;
  
  public Umsatz (int kdNr, double umsatz) {
    this.kdNr = kdNr; 
    this.umsatz = umsatz; 
  }
  
  public int getKdNr () { 
    return this.kdNr; 
  }
  public double getUmsatz () { 
    return this.umsatz; 
  }
  @Override public String toString () {
    return this.kdNr + " " + this.umsatz;
  }
}
