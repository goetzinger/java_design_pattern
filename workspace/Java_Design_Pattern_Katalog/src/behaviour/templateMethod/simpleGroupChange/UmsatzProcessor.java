package behaviour.templateMethod.simpleGroupChange;
class UmsatzProcessor extends GroupProcessor<Umsatz> {

  private double totalSum;
  private double groupSum;

  @Override protected boolean isNewGroup (Umsatz old, Umsatz current) {
    return old.getKdNr () != current.getKdNr ();
  }
  @Override protected void handleBegin () {
    System.out.println ("Umsaetze");
    this.totalSum = 0;
  }
  @Override protected void handleGroupBegin (Umsatz object) {
    System.out.println ("\t" + object.getKdNr ());
    this.groupSum = 0;
  }
  @Override protected void handlePosition (Umsatz object) {
    System.out.println ("\t\t" + object.getUmsatz ());
    this.groupSum += object.getUmsatz ();
  }
  @Override protected void handleGroupEnd (Umsatz object) {
    System.out.println ("\t\t-----");
    System.out.println ("\t\t" + groupSum);
    totalSum += groupSum;
  }
  @Override protected void handleEnd () {
    System.out.println ("\t\t=====");
    System.out.println ("\t\t" + totalSum);
  }
}

