package behaviour.visitor.components;
import java.awt.*;

public class AbstractComponentVisitor implements ComponentVisitor {
  public void visit (Button button) { }
  public void visit (TextField textField) { }
  public void visit (Label label) { }
  // ...
  
  public void traverse (Component c) {
    if (c instanceof Button) 
      this.visit ((Button) c);
    else if (c instanceof TextField) 
      this.visit ((TextField) c);
    else if (c instanceof Label) 
      this.visit ((Label) c);
    // else ...
    
    if (c instanceof Container) {
      Container container = (Container) c;
      for (int i = 0; i < container.getComponentCount (); i++) {
        this.traverse (container.getComponent (i));
      }
    }
  }
}
