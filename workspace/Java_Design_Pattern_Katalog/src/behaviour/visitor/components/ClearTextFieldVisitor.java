package behaviour.visitor.components;
import java.awt.*;

public class ClearTextFieldVisitor extends AbstractComponentVisitor {
  @Override public void visit (TextField textField) {
    textField.setText ("");
  }
}

