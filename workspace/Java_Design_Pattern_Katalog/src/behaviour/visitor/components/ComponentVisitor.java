package behaviour.visitor.components;
import java.awt.*;

public interface ComponentVisitor {
  public abstract void visit (Button button);
  public abstract void visit (TextField textField);
  public abstract void visit (Label label);
  // ...
}

