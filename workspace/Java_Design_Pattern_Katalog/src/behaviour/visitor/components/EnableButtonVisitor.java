package behaviour.visitor.components;
import java.awt.*;

public class EnableButtonVisitor extends AbstractComponentVisitor {
  private boolean enable;
  public EnableButtonVisitor (boolean enable) {
    this.enable = enable;
  }
  @Override public void visit (Button button) {
    button.setEnabled (this.enable);
  }
}


