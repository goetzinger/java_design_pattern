package behaviour.visitor.components;
import java.awt.*;
import java.awt.event.*;

public class RegisterBlueWhiteHandlerVisitor extends AbstractComponentVisitor {
  private static FocusListener blueWhiteHandler = new FocusListener () {
    public void focusGained (FocusEvent e) {
      ((TextField) e.getSource ()).selectAll ();
    }
    public void focusLost (FocusEvent e) {
      ((TextField) e.getSource ()).select (0, 0);
    }
  };
  @Override public void visit (TextField textField) {
    textField.addFocusListener (blueWhiteHandler);
  }
}

