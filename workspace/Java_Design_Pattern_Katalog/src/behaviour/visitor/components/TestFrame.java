package behaviour.visitor.components;
import java.awt.*;
import java.awt.event.*;

class TestFrame extends Frame {

  private Label labelNr = new Label ("Nr");
  private Label labelName = new Label ("Name");
  private TextField textFieldNr = new TextField (5);
  private TextField textFieldName = new TextField (5);
  private Button buttonClear = new Button ("Clear");
  private Button buttonDisable = new Button ("Disable");

  public TestFrame () {
    this.setLayout (new FlowLayout ());
    this.add (this.labelNr);      
    this.add (this.textFieldNr);
    this.add (this.labelName);
    this.add (this.textFieldName);
    this.add (this.buttonClear);
    this.add (this.buttonDisable);

    new RegisterBlueWhiteHandlerVisitor ().traverse (this);
    
    this.addWindowListener (new WindowAdapter () {
      @Override public void windowClosing (WindowEvent e) {
        System.exit (0);
      }
    });
    this.buttonClear.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        new ClearTextFieldVisitor ().traverse (TestFrame.this);
      }
    });
    this.buttonDisable.addActionListener (new ActionListener () {
      public void actionPerformed (ActionEvent e) {
        new EnableButtonVisitor (false).traverse (TestFrame.this);
      }
    });
    this.pack ();
    this.setVisible (true);
  }
}
