package behaviour.visitor.elementVisitor;
public class Application {
	public static void main (String [] args) {
		Element root = buildTree ();
		root.dump ();
	}
	
	private static Element buildTree () {
		List root = new List ("einnahmen/ausgaben");
		List ein = new List ("einnahmen");
		List aus = new List ("ausgaben");
		root.add (ein);
		root.add (aus);
		ein.add (new Atom ("seminar", "5000"));
		ein.add (new Atom ("entwicklung", "8000"));
		ein.add (new Atom ("wetten", "80000"));
		List trinken = new List ("trinken");
		trinken.add (new Atom ("osaft", "50"));
		trinken.add (new Atom ("pils", "30"));
		aus.add (trinken);
		aus.add (new Atom ("essen", "400"));		
		return root;
	}
}