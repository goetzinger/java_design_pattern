package behaviour.visitor.elementVisitor;
public class Atom extends Element {
	private final String value;
	public Atom (String name, String value) {
		super (name);
		this.value = value;
	}
	public String getValue () {
		return this.value;
	}
	@Override public String toString () {
		return this.getName () + " : " + this.getValue ();
	}
	@Override public void accept (ElementVisitor visitor, int depth) {
		visitor.visit (this, depth);
	}
}