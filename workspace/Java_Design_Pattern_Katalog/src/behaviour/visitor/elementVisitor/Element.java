package behaviour.visitor.elementVisitor;
public abstract class Element {
	
	private final String name;
	public Element (String name) {
		this.name = name;
	}
	public String getName () {
		return this.name;
	}
	public int getChildCount () {
		return 0;
	}
	public Element getChild (int index) {
		throw new RuntimeException ();
	}
	public void traverse (ElementVisitor visitor) {
		this.traverse (visitor, 0);
	}
	
	public abstract void accept (ElementVisitor visitor, int depth);
	
	private void traverse (ElementVisitor visitor, int depth) {
		this.accept (visitor, depth);
		for (int i = 0; i < this.getChildCount (); i++) 
			this.getChild (i).traverse (visitor, depth + 1);
	}
	public void dump () {
		this.traverse (new ElementVisitor () {
			public void visit (Atom atom, int depth) {
				while (depth --> 0)
					System.out.print ("\t");
				System.out.println ("ATOM: " + atom);
			}
			public void visit (List list, int depth) {
				while (depth --> 0)
					System.out.print ("\t");
				System.out.println ("LIST: " + list);
			}
		});
	}
}
