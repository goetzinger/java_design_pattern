package behaviour.visitor.elementVisitor;
public interface ElementVisitor {
	public abstract void visit (Atom atom, int depth);
	public abstract void visit (List list, int depth);
}
	