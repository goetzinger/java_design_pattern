package behaviour.visitor.elementVisitor;
import java.util.ArrayList;

public class List extends Element {
	private ArrayList<Element> children;
	public List (String name) {
		super (name);
	}
	public void add (Element element) {
		if (this.children == null)
			this.children = new ArrayList<Element> ();
		this.children.add (element);
	}
	public int getChildCount () {
		return this.children == null ? 0 : this.children.size ();
	}
	public Element getChild (int index) {
		return this.children.get (index);
	}
	@Override public String toString () {
		return this.getName ();
	}
	@Override public void accept (ElementVisitor visitor, int depth) {
		visitor.visit (this, depth);
	}
}