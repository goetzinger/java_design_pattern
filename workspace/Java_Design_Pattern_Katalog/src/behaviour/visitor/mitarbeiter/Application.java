package behaviour.visitor.mitarbeiter;
public class Application {
  public static void main (String [] args) {

    Firma firma = new Firma ();
    firma.add (new GE (1000, "Meier", 5000.00));
    firma.add (new LE (2000, "Franke", 20, 40));
    firma.add (new GE (3000, "Steins", 4000.00));
    firma.add (new LE (4000, "Wacker", 10.0, 40));
    firma.add (new LE (5000, "Michels", 20.0, 40));

    System.out.println ("Gesamtverdienst = " + firma.getGesamtVerdienst ());

    System.out.println ("\nAlle Mitarbeiter");
    firma.iterate (new PrintVisitor ());

    System.out.println ("\nAlle Lohnempfaenger");
    firma.iterate (new PrintLEVisitor ());

    LohnSummeVisitor v = new LohnSummeVisitor ();
    firma.iterate (v);
    System.out.println ("\nLohnsumme = " + v.getSumme ());
    
  }
}

