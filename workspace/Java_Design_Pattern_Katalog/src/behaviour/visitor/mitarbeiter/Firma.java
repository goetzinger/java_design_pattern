package behaviour.visitor.mitarbeiter;
import java.util.ArrayList;

public class Firma {
  private ArrayList<MA> list = new ArrayList<MA> ();
  public void add (MA m) {
    this.list.add (m);
  }
  public int getMACount () {
    return this.list.size ();
  }
  public MA getMA (int index) {
    return this.list.get (index);
  }
  public void iterate (MAVisitor v) {
    for (MA m : this.list)
      m.accept (v);
  }
  public double getGesamtVerdienst () {
    double summe = 0;
    for (MA m : this.list)
      summe += m.getVerdienst ();
    return summe;
  }
}

