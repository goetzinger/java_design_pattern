package behaviour.visitor.mitarbeiter;
public class GE extends MA {
  public double gehalt;
  public GE (int nr, String name, double gehalt) {
    super (nr, name);
    this.gehalt = gehalt;
  }
  @Override public double getVerdienst () {
    return this.gehalt;
  }
  @Override public void accept (MAVisitor v) {
    v.visit (this);
  }
}

