package behaviour.visitor.mitarbeiter;
public class LE extends MA {
  public double stdLohn;
  public double anzStd;
  public LE (int nr, String name, double stdLohn, double anzStd) {
    super (nr, name);
    this.stdLohn = stdLohn;
    this.anzStd = anzStd;
  }
  @Override public double getVerdienst () {
    return this.stdLohn * this.anzStd;
  }
  @Override public void accept (MAVisitor v) {
    v.visit (this);
  }
}
