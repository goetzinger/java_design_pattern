package behaviour.visitor.mitarbeiter;
public class LohnSummeVisitor extends AbstractMAVisitor {
  private double summe = 0;
  @Override public void visit (LE le) {
    this.summe += le.anzStd * le.stdLohn;
  }
  public double getSumme () {
    return this.summe;
  }
}

