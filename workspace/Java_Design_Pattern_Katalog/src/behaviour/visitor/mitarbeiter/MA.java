package behaviour.visitor.mitarbeiter;
abstract public class MA {
  public final int nr;
  public String name;
  protected MA (int nr, String name) {
    this.nr = nr;
    this.name = name;
  }
  public abstract double getVerdienst ();
  public abstract void accept (MAVisitor v);
}


