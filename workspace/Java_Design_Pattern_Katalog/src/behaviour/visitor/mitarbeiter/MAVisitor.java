package behaviour.visitor.mitarbeiter;
public interface MAVisitor {
  public abstract void visit (LE le);
  public abstract void visit (GE ge);
}
