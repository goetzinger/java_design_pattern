package behaviour.visitor.mitarbeiter;
public class PrintLEVisitor extends AbstractMAVisitor {
  @Override public void visit (LE le) {
    System.out.println (le.nr + " " + le.name + " "
                      + le.stdLohn + " " + le.anzStd);
  }
}
