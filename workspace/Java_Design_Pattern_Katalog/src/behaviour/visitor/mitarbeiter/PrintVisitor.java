package behaviour.visitor.mitarbeiter;
public class PrintVisitor implements MAVisitor {
  private void printBaseData (MA ma) {
    System.out.print (ma.nr + " " + ma.name + " ");
  }
  public void visit (LE le) {
    this.printBaseData (le);
    System.out.println (le.stdLohn + " " + le.anzStd);
  }
  public void visit (GE ge) {
    this.printBaseData (ge);
    System.out.println (ge.gehalt);
  }
}

