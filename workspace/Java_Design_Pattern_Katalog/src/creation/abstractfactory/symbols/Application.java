package creation.abstractfactory.symbols;

import java.io.Reader;
import java.io.StringReader;

public class Application {
  public static void main (String [] args) {
    SymbolFactory symbolFactory = new SymbolFactoryA ();
    Reader reader = new StringReader ("plus (1, minus (3, 1))");
    Scanner scanner = new Scanner (symbolFactory, "(,)", reader);
    Symbol symbol = scanner.read ();
    while (symbol != null) {
      System.out.println (symbol);
      symbol = scanner.read ();
    }
  }
}
