package creation.abstractfactory.symbols;

public interface SymbolFactory { 
  public Symbol createNumberSymbol (double value);
  public Symbol createIdentifierSymbol (String value);
  public Symbol createSpecialSymbol (char value);
}
