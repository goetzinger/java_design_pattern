package creation.abstractfactory.symbols;

public class SymbolFactoryA implements SymbolFactory { 

  private static class SymbolImpl implements Symbol { 
    private static RuntimeException illegalCallException =
            new RuntimeException ("illegal call");
    private final Object value;
    public SymbolImpl (Object value) {
      this.value = value;
    }
    public boolean isNumber () { 
      return this.value instanceof Double; 
    }
    public boolean isIdentifier () { 
      return this.value instanceof String; 
    }
    public boolean isSpecial () { 
      return this.value instanceof Character; 
    }
    public double getNumber () { 
      if (! this.isNumber ())
        throw illegalCallException; 
      return ((Double) this.value).doubleValue ();
    }
    public String getIdentifier () { 
      if (! this.isIdentifier ())
        throw illegalCallException; 
      return (String) this.value; 
    }
    public char getSpecial () { 
      if (! this.isSpecial ())
        throw illegalCallException; 
      return ((Character) this.value).charValue ();
    }
    public Object getValue () {
      return this.value;
    }
    @Override public String toString () {
      return this.getClass ().getName () + " [" + this.getValue () + "]";
    }
    @Override public boolean equals (Object other) {
      if (! (other instanceof Symbol)) return false;
      if (this == other) return true;
      return this.getValue ().equals (((Symbol) other).getValue ());
    }
    @Override public int hashCode () {
      return this.getValue ().hashCode ();
    }
  }

  public Symbol createNumberSymbol (double value) {
    return new SymbolImpl (new Double (value));
  }
  public Symbol createIdentifierSymbol (String value) {
    return new SymbolImpl (value);
  }
  public Symbol createSpecialSymbol (char value) {
    return new SymbolImpl (new Character (value));
  }
}
