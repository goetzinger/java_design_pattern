package creation.abstractfactory.symbols;

public class SymbolFactoryB implements SymbolFactory { 

  private static abstract class AbstractSymbol implements Symbol { 
    private static RuntimeException illegalCallException =
            new RuntimeException ("illegal call");
    public boolean isNumber ()      { return false; }
    public boolean isIdentifier ()  { return false; }
    public boolean isSpecial ()     { return false; }
    public double getNumber ()      { throw illegalCallException; }
    public String getIdentifier ()  { throw illegalCallException; }
    public char getSpecial ()       { throw illegalCallException; }
    @Override public String toString () {
      return this.getClass ().getName () + " [" + this.getValue () + "]";
    }
    @Override public boolean equals (Object other) {
      if (! (other instanceof Symbol)) return false;
      if (this == other) return true;
      return this.getValue ().equals (((Symbol) other).getValue ());
    }
    @Override public int hashCode () {
      return this.getValue ().hashCode ();
    }
  }

  private static class NumberSymbol extends AbstractSymbol { 
    private final double value;
    public NumberSymbol (double value)        { this.value = value; }
    @Override public boolean isNumber ()      { return true;  }
    @Override public double getNumber ()      { return this.value; }
    public Object getValue ()                 { return new Double (this.value); }
  }

  private static class IdentifierSymbol extends AbstractSymbol { 
    private final String value;
    public IdentifierSymbol (String value)    { this.value = value; }
    @Override public boolean isIdentifier ()  { return true; }
    @Override public String getIdentifier ()  { return this.value; }
    public Object getValue ()                 { return this.value; }
  }

  private static class SpecialSymbol extends AbstractSymbol { 
    private final char value;
    public SpecialSymbol (char value)         { this.value = value; }
    @Override public boolean isSpecial ()     { return true; }
    @Override public char getSpecial ()       { return this.value; }
    public Object getValue ()                 { return new Character (this.value); }
  }

  public Symbol createNumberSymbol (double value) {
    return new NumberSymbol (value);
  }
  public Symbol createIdentifierSymbol (String value) {
    return new IdentifierSymbol (value);
  }
  public Symbol createSpecialSymbol (char value) {
    return new SpecialSymbol (value);
  }
}
