package creation.builder.xmlParsing;

public class Address implements Node {
  public String name;
  public String city;
  public Address (String name, String city) {
    this.name = name;
    this.city = city;
  }
  public String getName () {
    return this.name;
  }
  public String getCity () {
    return this.city;
  }
  public int getChildCount () {
    return 0;
  }
  public Node getChild (int index) {
    throw new RuntimeException ();
  }
  @Override public String toString () {
    return this.name + " (" + this.city + ")";
  }
}