package creation.builder.xmlParsing;

import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class AddressBuilder extends DefaultHandler {

  private Node root;
  private final Stack<Group> nodes = new Stack<Group> ();

  public Node getRoot () {
    return this.root;
  }
  public void startElement (String namespaceURI, String localName,
                String rawName, Attributes attributes) throws SAXException {
    Node node;
    if (rawName.equals ("group"))
      node = new Group (attributes.getValue ("category"));
    else 
      node = new Address (
                 attributes.getValue ("name"), 
                 attributes.getValue ("city"));
    if (this.root == null)
      this.root = node;
    else
      nodes.peek ().add (node);
    if (rawName.equals ("group"))
      this.nodes.push ((Group) node);
  }
  public void endElement (String namespaceURI, String localName,
                String rawName) throws SAXException {
    if (rawName.equals ("group"))
      this.nodes.pop ();
  }
}
