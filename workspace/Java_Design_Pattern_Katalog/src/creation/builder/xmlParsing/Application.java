package creation.builder.xmlParsing;

import java.io.FileInputStream;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.helpers.DefaultHandler;

public class Application {
  static final String filename = "./adressen.xml";
  static final String systemId = "file:./address.dtd";
  
  public static void main (String [] args) {
    AddressBuilder addressBuilder = new AddressBuilder ();
    parse (addressBuilder);
    Node node = addressBuilder.getRoot ();
    print (node, 0);

    ElementBuilder elementBuilder = new ElementBuilder ();
    parse (elementBuilder);
    Element element = elementBuilder.getRoot ();
    print (element, 0);
  }
  
  private static void parse (DefaultHandler handler) {
    FileInputStream in = null;
    try {
      SAXParser parser = SAXParserFactory.newInstance ().newSAXParser ();
      in = new FileInputStream (filename);
      parser.parse (in, handler, systemId);
    }
    catch (Exception e) {
      System.out.println (e);
    }
    finally {
      if (in != null) try { in.close (); } catch (Exception e) { }
    }
  }
  
  private static void print (Node node, int depth) {
    for (int i = 0; i < depth; i++)
      System.out.print ("\t");
    System.out.println (node);
    for (int i = 0; i < node.getChildCount (); i++)
      print (node.getChild (i), depth + 1);
  }
}