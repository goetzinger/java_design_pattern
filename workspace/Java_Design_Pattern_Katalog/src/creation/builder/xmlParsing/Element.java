package creation.builder.xmlParsing;

import java.util.ArrayList;
import java.util.HashMap;

public class Element implements Node {
  private final ArrayList<Element> children = new ArrayList<Element> ();
  private final HashMap<String,String> attributes = new HashMap<String,String> ();
  private final String name;
  public Element (String name) {
    this.name = name;
  }
  public void addAttribute (String name, String value) {
    this.attributes.put (name, value);
  }
  public void addChild (Element child) {
    this.children.add (child);
  }
  public String getName () {
    return this.name;
  }
  public String getAttribute (String name) {
    return this.attributes.get (name);
  }
  public int getChildCount () {
    return this.children.size ();
  }
  public Element getChild (int index) {
    return this.children.get (index);
  }
  public String toString () {
    return this.name + " " + this.attributes;
  }
}
