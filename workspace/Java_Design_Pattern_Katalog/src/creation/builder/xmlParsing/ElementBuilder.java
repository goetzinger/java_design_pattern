package creation.builder.xmlParsing;

import java.util.Stack;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ElementBuilder extends DefaultHandler {

  private Element root;
  private final Stack<Element> elements = new Stack<Element> ();

  public Element getRoot () {
    return this.root;
  }
    
  public void startElement (String namespaceURI, String localName,
                String rawName, Attributes attributes) throws SAXException {
    Element element = new Element (rawName);
    for (int i = 0; i < attributes.getLength (); i++) {
      element.addAttribute (attributes.getQName (i), attributes.getValue (i));
    }
    if (this.root == null)
      this.root = element;
    else
      elements.peek ().addChild (element);
    this.elements.push (element);
  }
  
  public void endElement (String namespaceURI, String localName,
                String rawName) throws SAXException {
    this.elements.pop ();
  }
}
