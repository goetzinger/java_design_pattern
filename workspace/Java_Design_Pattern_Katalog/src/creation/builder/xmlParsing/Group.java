package creation.builder.xmlParsing;

import java.util.ArrayList;

public class Group implements Node {
  private final ArrayList<Node> children = new ArrayList<Node> ();
  public String category;
  public Group (String category) {
    this.category = category;
  }
  public String getCategory () {
    return this.category;
  }
  public void add (Node child) {
    this.children.add (child);
  }
  public int getChildCount () {
    return this.children.size ();
  }
  public Node getChild (int index) {
    return this.children.get (index);
  }
  @Override public String toString () {
    return this.category;
  }
}
