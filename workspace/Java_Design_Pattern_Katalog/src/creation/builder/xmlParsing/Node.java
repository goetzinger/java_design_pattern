package creation.builder.xmlParsing;

public interface Node {
  public abstract int getChildCount ();
  public abstract Node getChild (int index);
}