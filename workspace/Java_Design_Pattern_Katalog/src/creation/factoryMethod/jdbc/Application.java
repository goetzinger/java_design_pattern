package creation.factoryMethod.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class Application {
  private static final class ProductListCreator extends
			ObjectListCreator<Product> {
		@Override protected Product createObject (ResultSet rs) throws SQLException {
		    return new Product (rs.getInt ("id"), rs.getString ("descr"), rs.getInt ("price"));
		  }

		@Override
		protected String getTableName() {
			// TODO Auto-generated method stub
			return "product";
		}
	}

public static void main (String [] args) throws Exception {
    List<Customer> list = null;
    Connection con = null;
    try {
      Class.forName ("sun.jdbc.odbc.JdbcOdbcDriver");
      con = DriverManager.getConnection ("jdbc:odbc:patterns");
      createCustomers (con);
      createProducts (con);
    }
    finally {
      if (con != null) con.close ();
    }
  }
  
  private static void createCustomers (Connection con) throws SQLException {
    CustomerListCreator creator = new CustomerListCreator ();
    List<Customer> list = creator.createList (con, "customer");
    for (Customer c : list)
      System.out.println (c.id + ", " + c.name);
  }

  private static void createProducts (Connection con) throws SQLException {
    ObjectListCreator<Product> creator = new ProductListCreator();
    List<Product> list = creator.createList (con, "product");
    for (Product p : list)
      System.out.println (p.id + ", " + p.descr + ", " + p.price);
  }
}