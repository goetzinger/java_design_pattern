package creation.factoryMethod.jdbc;

public class Customer {
  public final int id;
  public final String name;
  public Customer (int id, String name) {
    this.id = id;
    this.name = name;
  }
}
