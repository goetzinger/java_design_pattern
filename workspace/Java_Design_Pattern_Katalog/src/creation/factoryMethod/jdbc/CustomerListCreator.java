package creation.factoryMethod.jdbc;

import java.sql.ResultSet;
import java.sql.SQLException;

class CustomerListCreator extends ObjectListCreator<Customer> {
  @Override protected Customer createObject (ResultSet rs) throws SQLException {
    return new Customer (rs.getInt ("id"), rs.getString ("name"));
  }

@Override
protected String getTableName() {
	return "customer";
}
}
