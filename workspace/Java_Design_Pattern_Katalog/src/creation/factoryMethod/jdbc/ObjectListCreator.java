package creation.factoryMethod.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public abstract class ObjectListCreator<T> {
  public List<T> createList (Connection con, String tablename) throws SQLException {
    Statement stmt = null;
    ResultSet rs = null;
    try {
      ArrayList<T> list = new ArrayList<T> ();
      stmt = con.createStatement ();
      rs = stmt.executeQuery ("select * from " + getTableName());
      while (rs.next ()) {
        T object = this.createObject (rs);
        list.add (object);
      }
      return list;
    }
    finally {
      try { if (rs != null) rs.close (); }
      catch (Exception e) { System.err.println (e); }
      try { if (stmt != null) stmt.close (); }
      catch (Exception e) { System.err.println (e); }
    }
  }
  protected abstract T createObject (ResultSet rs) throws SQLException;
  
  protected abstract String getTableName();
}
