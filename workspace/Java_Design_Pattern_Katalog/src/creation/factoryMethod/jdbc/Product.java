package creation.factoryMethod.jdbc;

public class Product {
  public final int id;
  public final String descr;
  public final int price;
  public Product (int id, String descr, int price) {
    this.id = id;
    this.descr = descr;
    this.price = price;
  }
}
