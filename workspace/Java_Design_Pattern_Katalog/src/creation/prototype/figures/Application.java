package creation.prototype.figures;

public class Application {
  public static void main (String [] args) {
    FigurePrototypes prototypes = FigurePrototypes.getInstance ();
    
    Figure f1 = prototypes.get (0).clone ();
    Figure f2 = prototypes.get (0).clone ();
    
    System.out.println (f1.getWidth ());
    System.out.println (f2.getWidth ());
    
    f2.setSize (200, 200);
    
    System.out.println (f1.getWidth ());
    System.out.println (f2.getWidth ());
  }
}