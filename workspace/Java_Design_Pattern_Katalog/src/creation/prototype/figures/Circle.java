package creation.prototype.figures;

import java.awt.Graphics;

public class Circle extends Figure {
  public void draw (Graphics g) {
    g.drawOval (getX (), getY (), getWidth (), getHeight ());
  }
}
