package creation.prototype.figures;

import java.awt.Graphics;
import java.awt.Rectangle;

public abstract class Figure implements Cloneable {
  private Rectangle enclosingRect = new Rectangle ();
  public abstract void draw (Graphics g);
  @Override public Figure clone () {
    try {
      Figure f = (Figure) super.clone ();
      f.enclosingRect = (Rectangle) this.enclosingRect.clone ();
      return f;
    }
    catch (CloneNotSupportedException e) {
      throw new RuntimeException (e);
    }
  }
  public void setSize (int width, int height) {
    this.enclosingRect.setSize (width, height);
  }
  public void setLocation (int x, int y) {
    this.enclosingRect.setLocation (x, y);
  }
  public int getX ()      { return this.enclosingRect.x; }
  public int getY ()      { return this.enclosingRect.y; }
  public int getWidth ()  { return this.enclosingRect.width; }
  public int getHeight () { return this.enclosingRect.height; }
}