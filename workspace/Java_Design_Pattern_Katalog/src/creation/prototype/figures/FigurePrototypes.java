package creation.prototype.figures;

import java.util.ArrayList;

public class FigurePrototypes {
  private final ArrayList<Figure> prototypes = new ArrayList<Figure> ();

  private FigurePrototypes () {
    this.add (new Rect ());
    this.add (new Circle ());
    // koennten auch dynamisch erzeugt werden!!
  }
  private void add (Figure f) {
    f.setSize (100, 100);
    this.prototypes.add (f);
  } 
  public int getCount () {
    return this.prototypes.size ();
  }
  public Figure get (int index) {
    return this.prototypes.get (index);
  }

  private static FigurePrototypes theInstance = new FigurePrototypes ();
  public static FigurePrototypes getInstance () {
    return theInstance;
  }
}
