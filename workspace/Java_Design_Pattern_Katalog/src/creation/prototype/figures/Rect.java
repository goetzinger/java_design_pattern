package creation.prototype.figures;

import java.awt.Graphics;

public class Rect extends Figure {
  public void draw (Graphics g) {
    g.drawRect (getX (), getY (), getWidth (), getHeight ());
  }
}
