package creation.singleton.blueWhiteHandler;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Application {
  public static void main (String [] args) throws Exception {
    UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
    JFrame frame = new JFrame ("BlueWhiteHandler");
    frame.setLayout (new FlowLayout ());
    JTextField textFieldX = new JTextField (10);
    JTextField textFieldY = new JTextField (10);
    textFieldX.addFocusListener (BlueWhiteHandler.getInstance ());
    textFieldY.addFocusListener (BlueWhiteHandler.getInstance ());
    frame.add (textFieldX);
    frame.add (textFieldY);
    frame.addWindowListener (new WindowAdapter () {
      @Override public void windowClosing (WindowEvent e) {
        System.exit (0);
      }
    });
    frame.pack ();
    frame.setVisible (true);
  }
}