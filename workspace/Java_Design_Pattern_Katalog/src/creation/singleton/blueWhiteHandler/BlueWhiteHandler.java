package creation.singleton.blueWhiteHandler;

import javax.swing.*;
import java.awt.event.*;

public class BlueWhiteHandler implements FocusListener {
  
  private static BlueWhiteHandler theInstance;
  
  private BlueWhiteHandler () { }

  public static BlueWhiteHandler getInstance () {
    if (theInstance == null)
      theInstance = new BlueWhiteHandler ();
    return theInstance;
  }
  
  public void focusGained (FocusEvent e) {
    Object source = e.getSource ();
    if (source instanceof JTextField) 
      ((JTextField) source).selectAll ();
  }
  public void focusLost (FocusEvent e) {
    Object source = e.getSource ();
    if (source instanceof JTextField) 
      ((JTextField) source).select (0, 0);
  }
}