package creation.singleton.chess;

public class Application {
  public static void main (String [] args) {
    Position p1 = Position.get (0, 0);
    Position p2 = Position.get ('A', 1);
    System.out.println (p1 == p2);
    Position p3 = Position.get (7, 7);
    Position p4 = Position.get ('H', 8);
    System.out.println (p3 == p4);
    for (int i = 0; i < Position.count; i++)
      System.out.println (Position.get (i));
  }
}
