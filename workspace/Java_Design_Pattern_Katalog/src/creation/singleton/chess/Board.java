package creation.singleton.chess;

public class Board {

  private final Figure [][] figures = new Figure [8][];

  public Board () {
    for (int i = 0; i < 8; i++)
      figures [i] = new Figure [8];

    for (char column = 'A'; column < 'H'; column++) {
      this.set (Position.get (column, 2), Knight.white);
      this.set (Position.get (column, 7), Knight.black);
    }
    // dito for Castles, Queens etc.
  }
  
  public Figure get (Position p) {
    return this.figures [p.x][p.y];
  }
  
  public void set (Position p, Figure f) {
    this.figures [p.x][p.y] = f;
  }
}
