package creation.singleton.chess;

public abstract class Figure {

  public static enum Color { WHITE, BLACK };

  public final Color color;
  public Figure (Color color) {
    this.color = color;
  }
  public abstract boolean canMove (Position from, Position to);
}

class Knight extends Figure {
  private Knight (Color color) { 
    super (color);
  }
  public static final Knight white = new Knight (Color.WHITE);
  public static final Knight black = new Knight (Color.BLACK);
  public boolean canMove (Position from, Position to) {
    return true; // too difficult to implement here...
  }
}

class Bishop extends Figure {
  private Bishop (Color color) { 
    super (color);
  }
  public static final Bishop white = new Bishop (Color.WHITE);
  public static final Bishop black = new Bishop (Color.BLACK);
  public boolean canMove (Position from, Position to) {
    return from.x - to.x == from.y - to.y;
  }
}

class Castle extends Figure {
  private Castle (Color color) { 
    super (color);
  }
  public static final Castle white = new Castle (Color.WHITE);
  public static final Castle black = new Castle (Color.BLACK);
  public boolean canMove (Position from, Position to) {
    return from.x == to.x || from.y == to.y;
  }
}
// etc.
