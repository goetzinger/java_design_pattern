package creation.singleton.chess;

public class Position {
  
  public final int x;
  public final int y;
  
  private Position (int x, int y) {
    this.x = x;
    this.y = y;
  }
  
  public static final int count = 64;
  private static final Position [] positions = new Position [count];

  static {
    for (int x = 0; x < 8; x++)
      for (int y = 0; y < 8; y++)
        positions [x * 8 + y] = new Position (x, y);
  }

  public static Position get (int x, int y) {
    if (x < 0 || x >= 8 || y < 0 || y >= 8)
      throw new IllegalArgumentException ();
    return positions [x * 8 + y];
  }
  
  public static Position get (int index) {
    return positions [index];
  }
  
  public static Position get (char column, int row) {
    Character.toUpperCase (column);
    if (column < 'A' || column > 'H' || row < 1 || row > 8)
      throw new IllegalArgumentException ();
    int x = column - 'A';
    int y = row - 1;
    return get (x, y);
  }
  @Override public String toString () {
    return "" + (char) ('A' + this.x) + (1 + this.y);
  }
}