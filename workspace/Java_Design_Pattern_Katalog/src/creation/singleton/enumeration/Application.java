package creation.singleton.enumeration;

public class Application {
  public static void main (String [] args) {
    Season spring = Season.SPRING;
    print (spring);
    print (Season.SUMMER);
    System.out.println ();
    Season [] seasons = Season.values ();
    for (Season s : seasons)
      System.out.println (s);
  }

  private static void print (Season season) {
    System.out.println (season);
  }
}
