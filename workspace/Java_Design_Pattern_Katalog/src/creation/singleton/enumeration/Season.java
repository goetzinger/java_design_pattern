package creation.singleton.enumeration;

public class Season {
  
  private final String name;
      
  private Season (String name) {
    this.name = name;
  }
  
  @Override public String toString () {
    return this.name;
  }
  public final static Season SPRING = new Season ("SPRING");
  public final static Season SUMMER = new Season ("SUMMER");
  public final static Season AUTUMN = new Season ("AUTUMN");
  public final static Season WINTER = new Season ("WINTER");
  
  public static Season [] values () {
    return new Season [] { SPRING, SUMMER, AUTUMN, WINTER };
  }
}
