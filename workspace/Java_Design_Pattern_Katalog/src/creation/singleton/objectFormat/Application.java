package creation.singleton.objectFormat;

public class Application {
  public static void main (String [] args) throws Exception {
    Class.forName ("PointFormat");
    Class.forName ("DimensionFormat");
    Class.forName ("ColorFormat");
    read (Integer.class);
    read (Double.class);
    read (Boolean.class);
    read (Character.class);
    read (String.class);
    read (java.awt.Dimension.class);
    read (java.awt.Color.class);
    read (java.awt.Point.class);
    Console.readLine ("type <CR> to exit... ");
  }
  
  public static void read (Class cls) {
    try {
      ObjectFormat format = ObjectFormatManager.getInstance ().getFormat (cls);
      boolean done = false;
      while (! done) {
        String line = Console.readLine (cls.getName () + " ");
        try {
          Object obj = format.toObject (line);
          System.out.println ("\t" + obj);
          System.out.println ("\t" + format.toString (obj));
          done = true;
        }
        catch (ObjectFormatException e) {
          System.out.println (e);
        }
      }   
    }
    catch (Exception e) {
      System.out.println (e);
    }
  }
}



