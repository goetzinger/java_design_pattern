package creation.singleton.objectFormat;

public class BooleanFormat extends ObjectFormat {
  static {
    new BooleanFormat ();
  }
  private BooleanFormat () { 
    super (Boolean.class);
  }
  public Object toObject (String s) throws ObjectFormatException {
    if (! s.equals ("true") && ! s.equals ("false"))
      throw new ObjectFormatException (this, s);
    return new Boolean (s);
  }
}