package creation.singleton.objectFormat;

public class CharacterFormat extends ObjectFormat {
	static {
		new CharacterFormat ();
	}
	private CharacterFormat () { 
		super (Character.class);
	}
	public Object toObject (String s) throws ObjectFormatException {
		if (s.length () != 1)	
			throw new ObjectFormatException (this, s);
		return new Character (s.charAt (0));
	}
}