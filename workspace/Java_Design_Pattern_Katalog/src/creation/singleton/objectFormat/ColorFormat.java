package creation.singleton.objectFormat;

import java.awt.*;

public class ColorFormat extends ObjectFormat {
  static {
    new ColorFormat ();
  }
  private ColorFormat () { 
    super (Color.class);
  }
  public String toString (Object obj) {
    Color color = (Color) obj;
    return color.getRed () + " " + color.getGreen () + " " + color.getBlue ();
  }
  public Object toObject (String s) throws ObjectFormatException {
    int [] v = this.parseIntList (this, s, 3);
    try {
      return new Color (v [0], v [1], v [2]);
    }
    catch (IllegalArgumentException e) {
      throw new ObjectFormatException (this, s);
    }
  }
}