package creation.singleton.objectFormat;

import java.awt.*;

public class DimensionFormat extends ObjectFormat {
	static {
		new DimensionFormat ();
	}
	private DimensionFormat () { 
		super (Dimension.class);
	}
	public String toString (Object obj) {
		Dimension dim = (Dimension) obj;
		return dim.width + " " + dim.height;
	}
	public Object toObject (String s) throws ObjectFormatException {
		int [] v = this.parseIntList (this, s, 2);
		return new Dimension (v [0], v [1]);
	}
}