package creation.singleton.objectFormat;

public class DoubleFormat extends ObjectFormat {
	static {
		new DoubleFormat ();
	}
	private DoubleFormat () { 
		super (Double.class);
	}
	public Object toObject (String s) throws ObjectFormatException {
		try {
			return new Double (s);
		}
		catch (NumberFormatException e) {
			throw new ObjectFormatException (this, s);
		}
	}
}