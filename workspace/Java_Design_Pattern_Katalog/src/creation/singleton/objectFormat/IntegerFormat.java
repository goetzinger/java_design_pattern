package creation.singleton.objectFormat;

public class IntegerFormat extends ObjectFormat {
  static {
    new IntegerFormat ();
  }
  private IntegerFormat () { 
    super (Integer.class);
  }
  public Object toObject (String s) throws ObjectFormatException {
    try {
      return new Integer (s);
    }
    catch (NumberFormatException e) {
      throw new ObjectFormatException (this, s);
    }
  }
}