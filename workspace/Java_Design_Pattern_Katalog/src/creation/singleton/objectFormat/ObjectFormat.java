package creation.singleton.objectFormat;

public abstract class ObjectFormat {

  private final Class cls;

  public ObjectFormat (Class cls) {
    this.cls = cls;
    ObjectFormatManager.getInstance ().register (this);
  }

  public Class getObjectClass () {
    return this.cls;
  }
  public String toString (Object object) {
    return object.toString ();
  }
  public abstract Object toObject (String s) throws ObjectFormatException;
  
  protected int [] parseIntList (ObjectFormat format, String s, int count) throws ObjectFormatException {
    try {
      String [] tokens = s.split (" ");
      if (tokens.length != count)
        throw new ObjectFormatException (format, s);
      int [] values = new int [tokens.length];
      for (int i = 0; i < tokens.length; i++)
        values [i] = Integer.parseInt (tokens [i]);
      return values;
    }
    catch (NumberFormatException e) {
      throw new ObjectFormatException (format, s);
    }
  }
}

