package creation.singleton.objectFormat;

public class ObjectFormatException extends Exception {
	public ObjectFormatException (ObjectFormat objectFormat, String string) {
		super (objectFormat.getClass ().getName () + ": illegal string = " + string);
	}
}