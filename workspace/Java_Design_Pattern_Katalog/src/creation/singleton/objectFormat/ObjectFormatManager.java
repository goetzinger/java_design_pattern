package creation.singleton.objectFormat;

import java.util.ArrayList;

public class ObjectFormatManager {

  private static ObjectFormatManager instance;
  
  public static ObjectFormatManager getInstance () {
    if (instance == null) {
      instance = new ObjectFormatManager ();
      try {
        Class.forName ("IntegerFormat");
        Class.forName ("BooleanFormat");
        Class.forName ("DoubleFormat");
        Class.forName ("CharacterFormat");
        Class.forName ("StringFormat");
      }
      catch (ClassNotFoundException e) {
        System.out.println (e);
      }
    }   
    return instance;
  }
  
  private ArrayList<ObjectFormat> list = new ArrayList<ObjectFormat> ();
  
  private ObjectFormatManager () {  }

  public ObjectFormat getFormat (Class cls) throws Exception {
    for (ObjectFormat format : this.list)
      if (format.getObjectClass () == cls)
        return format;
    throw new Exception ("format for " + cls.getName () + " not registered");
  }

  public void register (ObjectFormat format) {
    this.list.add (0, format);
  }
}