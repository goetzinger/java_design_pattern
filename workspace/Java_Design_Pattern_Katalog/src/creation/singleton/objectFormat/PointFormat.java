package creation.singleton.objectFormat;

import java.awt.*;

public class PointFormat extends ObjectFormat {
	static {
		new PointFormat ();
	}
	private PointFormat () { 
		super (Point.class);
	}
	public String toString (Object obj) {
		Point p = (Point) obj;
		return p.x + " " + p.y;
	}
	public Object toObject (String s) throws ObjectFormatException {
		int [] v = this.parseIntList (this, s, 2);
		return new Point (v [0], v [1]);
	}
}