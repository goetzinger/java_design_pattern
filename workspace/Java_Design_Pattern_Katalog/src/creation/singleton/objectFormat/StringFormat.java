package creation.singleton.objectFormat;

public class StringFormat extends ObjectFormat {
  static {
    new StringFormat ();
  }
  private StringFormat () { 
    super (String.class);
  }
  public String toObject (String s) throws ObjectFormatException {
    return s;
  }
}