package creation.singleton.runtimeAndToolkit;

import java.io.*;
import java.awt.*;

public class Application {
  public static void main (String [] args) {
    Runtime runtime = Runtime.getRuntime ();
    try {
      Process process = runtime.exec ("C:/WINNT/Notepad.exe");
      System.out.println ("Notepad started");
      process.waitFor ();
      System.out.println ("Notepad terminated");
      
      Toolkit toolkit = Toolkit.getDefaultToolkit ();
      toolkit.beep ();
      Dimension dim = toolkit.getScreenSize ();
      System.out.println ("Size = " + dim.width + ", " + dim.height);
    }
    catch (Exception e) {
      System.err.println (e);
    }
  }
}
