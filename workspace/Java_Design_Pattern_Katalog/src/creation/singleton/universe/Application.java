package creation.singleton.universe;

public class Application {
  public static void main (String [] args) throws Exception {
  	Universe u1 = Universe.getTheUniverse ();
  	Universe u2 = Universe.getTheUniverse ();
  	System.out.println (u1 != null);
  	System.out.println (u1 == u2);
  }
}