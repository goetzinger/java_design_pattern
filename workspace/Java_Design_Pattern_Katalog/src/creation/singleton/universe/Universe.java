package creation.singleton.universe;

public class Universe {
  
  private static Universe theUniverse;
  
  private Universe () { }

  synchronized public static Universe getTheUniverse () {
    if (theUniverse == null)
      theUniverse = new Universe ();
    return theUniverse;
  }
}