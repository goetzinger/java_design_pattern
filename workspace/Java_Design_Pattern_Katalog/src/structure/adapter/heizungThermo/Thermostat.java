package structure.adapter.heizungThermo;
import java.util.ArrayList;

public class Thermostat {
  private final ArrayList<ThermostatListener> listeners = 
                new ArrayList<ThermostatListener> ();
  public void addThermostatListener (ThermostatListener listener) {
    this.listeners.add (listener);
  }
  public void removeThermostatListener (ThermostatListener listener) {
    this.listeners.remove (listener);
  }
  public void run () {
    ThermostatEvent event;
    // es ist zu kalt geworden
    event = new ThermostatEvent (this);
    for (ThermostatListener listener : this.listeners)
      listener.minAlarm (event);

    // es ist zu warm geworden
    event = new ThermostatEvent (this);
    for (ThermostatListener listener : this.listeners)
      listener.maxAlarm (event);
  }
}
