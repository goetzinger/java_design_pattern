package structure.adapter.heizungThermo;
public class ThermostatHeizungAdapter implements ThermostatListener {
  private final Heizung heizung;
  public ThermostatHeizungAdapter (Heizung heizung) {
    this.heizung = heizung;
  }
  public void minAlarm (ThermostatEvent e) {
    this.heizung.brennerEin ();
  }
  public void maxAlarm (ThermostatEvent e) {
    this.heizung.brennerAus ();
  }
}