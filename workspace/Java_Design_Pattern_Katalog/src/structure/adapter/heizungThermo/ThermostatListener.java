package structure.adapter.heizungThermo;
import java.util.EventListener;

public interface ThermostatListener extends EventListener {
  public abstract void minAlarm (ThermostatEvent e);
  public abstract void maxAlarm (ThermostatEvent e);
}
  