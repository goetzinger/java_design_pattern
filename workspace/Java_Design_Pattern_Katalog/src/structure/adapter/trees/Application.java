package structure.adapter.trees;
import javax.swing.*;

public class Application {
  public static void main (String [] args) throws Exception {
    Composite root = new Composite ("einnahmen/ausgaben");
    Composite einnahmen = new Composite ("einnahmen");
    Composite ausgaben = new Composite ("ausgaben");
    root.add (einnahmen);
    root.add (ausgaben);
    einnahmen.add (new Leaf ("seminar", 5000));
    einnahmen.add (new Leaf ("entwicklung", 8000));
    einnahmen.add (new Leaf ("wetten", 80000));
    Composite trinken = new Composite ("trinken");
    trinken.add (new Leaf ("osaft", -50));
    trinken.add (new Leaf ("pils", -30));
    ausgaben.add (trinken);
    ausgaben.add (new Leaf ("essen", -400));   

    UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
    new ElementFrame (root);
  }
}