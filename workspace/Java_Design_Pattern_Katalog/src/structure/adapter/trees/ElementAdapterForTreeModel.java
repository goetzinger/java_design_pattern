package structure.adapter.trees;
import javax.swing.event.*;
import javax.swing.tree.*;

public class ElementAdapterForTreeModel implements TreeModel {
  private final Element root;
  public ElementAdapterForTreeModel (Element root) {
    this.root = root;
  }
  public Object getRoot () {
    return this.root;
  }
  public int getChildCount (Object parent) {
    return ((Element) parent).getChildCount ();
  }
  public Object getChild (Object parent, int index) {
    return ((Element) parent).getChild (index);
  }
  public boolean isLeaf (Object node) {
    return node instanceof Leaf;
  }
  public int getIndexOfChild (Object parent, Object child) {
    Element node = (Element) parent;
    for (int i = 0; i < node.getChildCount (); i++) 
      if (node.getChild (i) == child)
        return i;
    return -1;
  }
  public void valueForPathChanged (TreePath path, Object value) {
  }
  public void addTreeModelListener (TreeModelListener l) {
  }
  public void removeTreeModelListener (TreeModelListener l) {
  }
}
