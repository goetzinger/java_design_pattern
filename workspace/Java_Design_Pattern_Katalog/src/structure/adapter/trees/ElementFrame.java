package structure.adapter.trees;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ElementFrame extends JFrame {
  private JTree tree = new JTree ();
  private JScrollPane scrollPane = new JScrollPane (this.tree);
  public ElementFrame (Element root) {
    this.setLayout (new BorderLayout ());
    this.add (this.scrollPane, BorderLayout.CENTER);
    this.tree.setModel (new ElementAdapterForTreeModel (root));
    for (int i = 0; i < this.tree.getRowCount (); i++)
      this.tree.expandRow (i);
    this.scrollPane.setPreferredSize (new Dimension (300, 500));
    this.addWindowListener (new WindowAdapter () {
      @Override public void windowClosing (WindowEvent e) {
        System.exit (0);
      }
    });
    this.pack ();
    this.setVisible (true);
  }
}
