package structure.adapter.trees;
public class Leaf extends Element {
  private final double value;
  public Leaf (String name, double value) {
    super (name);
    this.value = value;
  }
  @Override public double getValue () {
    return this.value;
  }
  @Override public String toString () {
    return this.getName () + " : " + this.getValue ();
  }
}