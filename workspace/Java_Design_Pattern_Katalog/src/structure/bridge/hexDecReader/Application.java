package structure.bridge.hexDecReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.IOException;

public class Application {
  public static void main (String [] args) throws IOException {
    testDecimal (new StringReader (" 100   200   1   12345 "));
    testHex (new StringReader ("0x0  0xff 0xf 0x10"));
    testDecimal (new InputStreamReader (new FileInputStream ("decimal.txt")));
    testHex (new InputStreamReader (new FileInputStream ("hex.txt")));  }

  private static void testDecimal (Reader reader) throws IOException {
    IntReader intReader = new DecimalIntReader (reader);
    Integer value = intReader.read ();
    while (value != null) {
      System.out.println (value);
      value = intReader.read ();
    }
  }
  private static void testHex (Reader reader) throws IOException {
    IntReader intReader = new HexIntReader (reader);
    Integer value = intReader.read ();
    while (value != null) {
      System.out.println (value);
      value = intReader.read ();
    }
  }
}