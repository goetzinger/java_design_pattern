package structure.bridge.hexDecReader;
import java.io.Reader;
import java.io.IOException;

public class DecimalIntReader extends IntReader {

  public DecimalIntReader (Reader reader) throws IOException {
    super (reader);
  }
  
  @Override public Integer read () throws java.io.IOException {
    while (Character.isWhitespace (this.current ()))
      this.next ();
    if (this.current () == -1)
      return null;
    if (! Character.isDigit (this.current ()))
      throw new NumberFormatException (String.valueOf ((char) this.current ()));
    int value = 0;
    while (Character.isDigit (this.current ())) {
      value = value * 10 + this.current () - '0';
      this.next ();
    }
    return Integer.valueOf (value);
  }
}