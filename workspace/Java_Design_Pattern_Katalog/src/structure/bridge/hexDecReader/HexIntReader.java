package structure.bridge.hexDecReader;
import java.io.Reader;
import java.io.IOException;

public class HexIntReader extends IntReader {

  public HexIntReader (Reader reader) throws IOException {
    super (reader);
  }
  
  @Override public Integer read () throws java.io.IOException {
    while (Character.isWhitespace (this.current ()))
      this.next ();
    if (this.current () == -1)
      return null;
    if (this.current () != '0')
      throw new NumberFormatException (String.valueOf ((char) this.current ()));
    this.next ();
    if (this.current () != 'x')
      throw new NumberFormatException (String.valueOf ((char) this.current ()));
    this.next ();
    if (! isHex (this.current ()))
      throw new NumberFormatException (String.valueOf ((char) this.current ()));
    int value = 0;
    while (isHex (this.current ())) {
      value = value * 16 + getValue (this.current ());
      this.next ();
    }
    return Integer.valueOf (value);
  }
  
  private static boolean isHex (int ch) {
    return Character.isDigit (ch) || (ch >= 'a' && ch <= 'z');
  }
  private static int getValue (int ch) {
    assert isHex (ch);
    return Character.isDigit (ch) ? ch - '0' : 10 + ch - 'a';
  }
}