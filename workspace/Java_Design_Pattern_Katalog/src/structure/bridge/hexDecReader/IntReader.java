package structure.bridge.hexDecReader;
import java.io.Reader;
import java.io.IOException;

public abstract class IntReader {
  
  private final Reader impl;
  private int current;
  
  public IntReader (Reader impl) throws IOException {
    this.impl = impl;
    this.next ();
  }
  
  public abstract Integer read () throws IOException;
  
  protected void next () throws IOException {
    this.current = impl.read ();
  }
  
  protected int current () {
    return this.current;
  }
}