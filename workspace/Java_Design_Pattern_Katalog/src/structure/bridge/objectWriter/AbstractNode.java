package structure.bridge.objectWriter;
import java.util.Iterator;

public abstract class AbstractNode implements Node {

	private final int id;
	private final Key key;
	private final Object object;

	public AbstractNode (int id, Key key, Object object) {
		this.id = id; 
		this.key = key; 
		this.object = object;
	}

	public int getId ()									{ return this.id; }
	public Key getKey ()								{ return this.key; }
	public Object getObject ()					{ return this.object; }

	@Override public String toString () {
		return this.getClass ().getName () 
			+ " [" + this.id + " " + this.key + " " + this.object	+ "]";
	}
}
