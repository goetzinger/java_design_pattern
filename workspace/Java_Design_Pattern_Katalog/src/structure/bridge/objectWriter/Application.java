package structure.bridge.objectWriter;
public class Application {
  public static void main (String [] args) {

		Teacher teacher = new Teacher (4711, "Nowak", new City ("33106", "Paderborn"));
		teacher.getSkills ().put ("Java", 1);
		teacher.getSkills ().put ("C++", 2);
		teacher.getSkills ().put ("C#", 3);
		teacher.getSkills ().put ("Eiffel", 1);
		teacher.getCompanies ().add ("Unilog");
		teacher.getCompanies ().add ("Siemens");
		teacher.getCompanies ().add ("Bertelsmann");
																					
    Book book = new Book ("1111", "Programming in Oberon", 20,
                          new Publisher ("Addison Wesley", "New York"), 
                          new Author ("Niklaus", "Wirth"),
                          new Author ("Martin", "Raiser")
                      );
 
 		TreeBuilder builder = new TreeBuilder ();
 		Node node = builder.buildTree (teacher);
 		//Node node = builder.buildTree (book);

 		//ObjectWriter writer = new StdObjectWriter (System.out);
 		ObjectWriter writer = new XmlObjectWriter (System.out);
 		writer.writeTree (node);
  }
}