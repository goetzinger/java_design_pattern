package structure.bridge.objectWriter;
public class Author {
  
  private String firstname;
  private String lastname;

  public Author (String firstname, String lastname) {
    this.firstname = firstname;
    this.lastname = lastname;
  }
  
  public String getFirstname ()        	{ return this.firstname; }
  public void setFirstname (String v)  	{ this.firstname = v; }
  
  public String getLastname ()          { return this.lastname; }
  public void setLastname (String v)    { this.lastname = v; }
}