package structure.bridge.objectWriter;
import java.util.*;

public class Book {
  
  private String isbn;
  private String title;
  private double price;
  private Publisher publisher;
  private final ArrayList<Author> authors = new ArrayList<Author> ();

  public Book (String isbn, String title, double price, 
                        Publisher publisher, Author... authors) {
    this.isbn = isbn;
    this.title = title;
    this.price = price;
    this.publisher = publisher;
    for (Author a : authors)
      this.authors.add (a);
    publisher.add (this);     
  }
  
  public String getIsbn ()                { return this.isbn; }
  public void setIsbn (String v)          { this.isbn = v; }

  public String getTitle ()               { return this.title; }
  public void setTitle (String v)         { this.title = v; }
  
  public double getPrice ()               { return this.price; }
  public void setPrice (double v)         { this.price = v; }

  public Publisher getPublisher ()        { return this.publisher; }
  public void setPublisher (Publisher v)  { this.publisher = v; }

  public List<Author> getAuthors ()       { return (List<Author>) this.authors.clone (); }
}