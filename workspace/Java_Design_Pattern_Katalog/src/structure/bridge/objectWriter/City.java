package structure.bridge.objectWriter;
public class City {
  
  private String zip;
  private String name;

	public City () { }
  public City (String zip, String name) {
    this.zip = zip;
    this.name = name;
  }
  
  public String getZip ()                		{ return this.zip; }

  public String getName ()               		{ return this.name; }
  public void setName (String name)       	{ this.name = name; }
}
