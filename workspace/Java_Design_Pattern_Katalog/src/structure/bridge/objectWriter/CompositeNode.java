package structure.bridge.objectWriter;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;

public abstract class CompositeNode extends AbstractNode {
	public final ArrayList<Node> children;
	public CompositeNode (int id, Key key, Object object, List<Node> children) {
		super (id, key, object);
		this.children = new ArrayList (children);
	}
	public Iterator<Node> iterator () 	{ return this.children.iterator (); }
}
