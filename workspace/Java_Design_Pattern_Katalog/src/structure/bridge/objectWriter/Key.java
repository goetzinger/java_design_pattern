package structure.bridge.objectWriter;
public abstract class Key {
	private final Object value;
	public Key (Object value)						{ this.value = value; }
	public Object getValue ()						{ return this.value; }
	public String getPropertyKey ()			{ throw new RuntimeException (); }
	public int getIndexKey ()						{ throw new RuntimeException (); }
	public Object getMapKey ()					{ throw new RuntimeException (); }
	public String toString ()						{ return this.value.toString (); }
}
