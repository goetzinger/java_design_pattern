package structure.bridge.objectWriter;
import java.util.List;

public class ListNode extends CompositeNode {
	public ListNode (int id, Key key, Object object, List<Node> children) {
		super (id, key, object, children);
	}
}
