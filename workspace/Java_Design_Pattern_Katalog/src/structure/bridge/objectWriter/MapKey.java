package structure.bridge.objectWriter;
public class MapKey extends Key {
	public MapKey (Object value) 				{ super (value); }
	public Object getMapKey ()					{ return this.getValue (); }
}
