package structure.bridge.objectWriter;
import java.util.List;

public class MapNode extends CompositeNode {
	public MapNode (int id, Key key, Object object, List<Node> children) {
		super (id, key, object, children);
	}
}