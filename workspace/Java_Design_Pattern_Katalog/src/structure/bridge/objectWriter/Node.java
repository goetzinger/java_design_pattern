package structure.bridge.objectWriter;
import java.util.Iterator;

public interface Node extends Iterable<Node> {
	
	public abstract int getId ();
	public abstract Key getKey ();
	public abstract Object getObject ();
	
	public abstract Iterator<Node> iterator ();
}