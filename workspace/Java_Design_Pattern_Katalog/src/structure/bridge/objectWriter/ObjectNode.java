package structure.bridge.objectWriter;
import java.util.List;

public class ObjectNode extends CompositeNode {
	public ObjectNode (int id, Key key, Object object, List<Node> children) {
		super (id, key, object, children);
	}
}
