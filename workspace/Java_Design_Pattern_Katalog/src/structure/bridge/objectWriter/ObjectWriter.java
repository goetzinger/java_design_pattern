package structure.bridge.objectWriter;
import java.util.*;
import java.io.*;

public abstract class ObjectWriter extends OutputStreamWriter {
  
  private int depth;
	
  public ObjectWriter (OutputStream stream) {
  	super (stream);
  }

  public final void writeTree (Node node) {
  	try {
		  this.depth = 0;
		  this.writeNode (node);
	   	this.flush ();
    }
    catch (Exception e) {
    	throw new RuntimeException (e);
    }
  }

  private void writeNode (Node node) throws Exception {
  	if (node instanceof PrimitiveNode) {
    	this.write ((PrimitiveNode) node);
    }
  	else if (node instanceof CircularNode) {
    	this.write ((CircularNode) node);
    }
    else if (node instanceof ObjectNode) {
    	this.start ((ObjectNode) node);
    	this.writeChildren (node);
      this.end ((ObjectNode) node);
    }
    else if (node instanceof ListNode) {
    	this.start ((ListNode) node);
    	this.writeChildren (node);
      this.end ((ListNode) node);
    }
    else if (node instanceof MapNode) {
    	this.start ((MapNode) node);
    	this.writeChildren (node);
      this.end ((MapNode) node);
    }
    else
    	throw new RuntimeException ();
  }

	private void writeChildren (Node node) throws Exception {
  	this.depth++;
    for (Node child : node) 
      this.writeNode (child);
    this.depth--;
	}
		
  protected void writeln (Object value) {
  	try {
	    for (int i = 0; i < this.depth; i++)
	      this.write ("\t");
	    this.write (value.toString ());
	    this.write ('\n');
	  }
	  catch (IOException e) {
	  	throw new RuntimeException (e);
	  }
  }

	protected abstract void start (ObjectNode node);
	protected abstract void end (ObjectNode node);
	protected abstract void start (ListNode node);
	protected abstract void end (ListNode node);
	protected abstract void start (MapNode node);
	protected abstract void end (MapNode node);
	protected abstract void write (PrimitiveNode node);
	protected abstract void write (CircularNode node);
}
