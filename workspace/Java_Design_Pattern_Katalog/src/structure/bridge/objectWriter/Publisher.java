package structure.bridge.objectWriter;
import java.util.*;

public class Publisher {
  
  private String name;
  private String city;
  private final ArrayList<Book> books = new ArrayList<Book> ();

  public Publisher (String name, String city) {
    this.name = name;
    this.city = city;
  }
  
  void add (Book book) {
    this.books.add (book);
  }
  
  public String getName ()        { return this.name; }
  public void setName (String v)  { this.name = v; }
  
  public String getCity ()        { return this.city; }
  public void setCity (String v)  { this.city = v; }

  public Publisher getMe ()       { return this; }
  
  public List<Book> getBooks ()   { return (List<Book>) this.books.clone (); }
}