package structure.bridge.objectWriter;
import java.util.Iterator;

public abstract class SimpleNode extends AbstractNode {

	private static class EmptyIterator implements Iterator<Node> {
		public boolean hasNext () 				{ return false; }
		public Node next ()								{ throw new RuntimeException (); }
		public void remove () 						{ throw new RuntimeException (); }
	}
	private static EmptyIterator emptyIterator = new EmptyIterator ();

	public SimpleNode (int id, Key key, Object object) {
		super (id, key, object);
	}
	public Iterator<Node> iterator () 	{ return emptyIterator; }
}
