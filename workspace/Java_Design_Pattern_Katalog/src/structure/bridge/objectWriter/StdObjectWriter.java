package structure.bridge.objectWriter;
public class StdObjectWriter extends ObjectWriter {

  public StdObjectWriter (java.io.OutputStream stream) {
  	super (stream);
  }

	protected void start (ObjectNode node) {
    writeln (
    		"#" + node.getId () + ":" 
    		+ (node.getKey () == null ? "" : (node.getKey () + " = "))
    		+ node.getObject ().getClass ().getName () 
    		+ " {");
  }
	protected void end (ObjectNode node) {
    writeln ("}");
	}
	protected void start (ListNode node) {
    writeln ("#" + node.getId () + ":" + node.getKey () + " = List {");
	}
	protected void end (ListNode node) {
		writeln ("}");
	}
	protected void start (MapNode node) {
    writeln ("#" + node.getId () + ":" + node.getKey () + " = Map {");
	}
	protected void end (MapNode node) {
		writeln ("}");
	}
	protected void write (CircularNode node) {
    writeln (node.getKey () + " = ^" + node.getId ());
	}
	protected void write (PrimitiveNode node) {
    writeln (node.getKey () + " = " + node.getObject ());
	}
}