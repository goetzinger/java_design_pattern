package structure.bridge.objectWriter;
import java.util.*;

public class Teacher {
  
  private int id;
  private String name;
  private City city;
  private ArrayList<String> companies = new ArrayList<String> ();
	private HashMap<String,Integer> skills = new HashMap<String,Integer> ();
	
  public Teacher (int id, String name, City city) {
    this.id = id;
    this.name = name;
    this.city = city;
  }
  
  public int getId ()                			{ return this.id; }

	public Map<String,Integer> getSkills ()	{ return this.skills; }
	
	public List<String> getCompanies ()			{ return this.companies; }
	
  public String getName ()               	{ return this.name; }
  public void setName (String name)       { this.name = name; }
  
  public City getCity ()               		{ return this.city; }
  public void setCity (City city)         { this.city = city; }
}
