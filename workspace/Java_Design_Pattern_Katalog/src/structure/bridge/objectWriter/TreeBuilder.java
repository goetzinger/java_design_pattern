package structure.bridge.objectWriter;
import java.util.*;
import java.beans.*;

public class TreeBuilder {
  
  private static HashMap primitives = new HashMap ();
  private static void addPrimitive (Class cls) {
    primitives.put (cls, cls);
  }
  private static boolean isPrimitive (Class cls) {
    return primitives.get (cls) != null;
  }
  static {
    addPrimitive (Character.class);
    addPrimitive (Byte.class);
    addPrimitive (Short.class);
    addPrimitive (Integer.class);
    addPrimitive (Long.class);
    addPrimitive (Float.class);
    addPrimitive (Double.class);
    addPrimitive (Boolean.class);
    addPrimitive (String.class);
  }

	private int id;
  private ArrayList<Object> list;
	
  public final Node buildTree (Object obj) {
    try {
		  this.list = new ArrayList<Object> ();
		  this.id = -1;
		  return this.buildTree (null, obj);
    }
    catch (Exception e) {
    	throw new RuntimeException (e);
    }
  }

  private Node buildTree (Key key, Object obj) throws Exception {
    if (obj == null || isPrimitive (obj.getClass ())) {
    	return new PrimitiveNode (-1, key, obj);
    }
    
   	int objIndex = this.list.indexOf (obj);
    if (objIndex >= 0) {
    	return new CircularNode (objIndex, key, obj);
    }

    this.id++;
    this.list.add (obj);

    if (obj instanceof List) {
    	ArrayList<Node> children = new ArrayList<Node> ();
			int i = 0;	    	
      for (Object elem : (List) obj) 
        children.add (this.buildTree (new ListKey (i++), elem));
    	return new ListNode (this.id, key, obj, children);
    }

    if (obj instanceof Map) {
    	ArrayList<Node> children = new ArrayList<Node> ();
    	Set<Map.Entry> set = ((Map) obj).entrySet ();
      for (Map.Entry elem : set) 
        children.add (this.buildTree (new MapKey (elem.getKey ()), elem.getValue ()));
    	return new MapNode (this.id, key, obj, children);
    }

   	ArrayList<Node> children = new ArrayList<Node> ();
		PropertyDescriptor [] pds = Introspector.getBeanInfo (obj.getClass ()).getPropertyDescriptors (); 		
		for (PropertyDescriptor pd : pds) {
			String propertyName = pd.getName ();
			if (propertyName.equals ("class"))
				continue;
      Object propertyValue = pd.getReadMethod ().invoke (obj);
      children.add (this.buildTree (new ObjectKey (propertyName), propertyValue));
    }
    return new ObjectNode (id, key, obj, children);
  }
}
