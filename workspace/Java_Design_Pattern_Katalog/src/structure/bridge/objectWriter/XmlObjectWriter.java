package structure.bridge.objectWriter;
public class XmlObjectWriter extends ObjectWriter {

  public XmlObjectWriter (java.io.OutputStream stream) {
  	super (stream);
  }

	private void writeStart (String tag, String... attributes) {
		StringBuffer buf = new StringBuffer ();
		buf.append ("<").append (tag);
		for (String attr : attributes) 
			if (attr.length () > 0) 
				buf.append (" ").append (attr);
		buf.append (">");
		writeln (buf.toString ());
	}
	private void writeEnd (String s) {
		writeln (new StringBuffer ().append ("</").append (s).append (">").toString ());
	}
	
	private String attr (String name, String value) {
		return new StringBuffer ().append (name).append ("='").append (value).append ("'").toString ();
	}
	private String attr (String name, int value) {
		return attr (name, String.valueOf (value));
	}

	private String key (Node node) {
		Key key = node.getKey ();
		if (key == null)
			return "";
		if (key instanceof ObjectKey)
			return attr ("property", key.getPropertyKey ());
		if (key instanceof ListKey)
			return attr ("index", key.getIndexKey ());
		if (key instanceof MapKey)
			return attr ("key", key.getMapKey ().toString ());
		throw new RuntimeException ();
	}
	private String cls (Node node) {
		Object obj = node.getObject ();
		return obj == null ? "" : attr ("class", obj.getClass ().getName ());
	}
	private String id (Node node) {
		return attr ("id", node.getId ());
	}
	private String refId (Node node) {
		return attr ("refId", node.getId ());
	}

	private String tag (Node node, String value) {
		return node.getKey () == null ? "java" : value;
	}

	protected void start (ObjectNode node) {
		String tag = tag (node, "object");
	 	writeStart (tag, key (node), id (node), cls (node));
  }
	protected void end (ObjectNode node) {
		String tag = tag (node, "object");
   	writeEnd (tag);
	}
	protected void start (ListNode node) {
		String tag = tag (node, "list");
	 	writeStart (tag, key (node), id (node), cls (node));
	}
	protected void end (ListNode node) {
		String tag = tag (node, "list");
   	writeEnd (tag);
	}
	protected void start (MapNode node) {
		String tag = tag (node, "map");
	 	writeStart (tag, key (node), id (node), cls (node));
	}
	protected void end (MapNode node) {
		String tag = tag (node, "map");
   	writeEnd (tag);
	}
	protected void write (CircularNode node) {
		String tag = tag (node, "ref");
	 	writeStart (tag, key (node), refId (node));
	}
	protected void write (PrimitiveNode node) {
		String tag = tag (node, "simple");
	 	writeStart (tag, key (node), cls (node));
	 	writeln ("\t" + node.getObject ());
	 	writeEnd (tag);
  }
}