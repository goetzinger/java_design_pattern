package structure.composite.awt;
import java.awt.*;
import java.awt.event.*;

public class Application {
  public static void main (String [] args) {

    Frame f = new Frame ("Frame");
    Panel p1 = new Panel ();
    Panel p2 = new Panel ();
    Panel p21 = new Panel ();
    Panel p22 = new Panel ();
    
    f.setLayout (new GridLayout (2, 1));
    p1.setLayout (new FlowLayout ());
    p2.setLayout (new FlowLayout ());
    p21.setLayout (new FlowLayout ());
    p22.setLayout (new FlowLayout ());

    f.add (p1);
    f.add (p2);
    p1.add (new TextField (10));
    p1.add (new TextField (10));
    p2.add (p21);
    p2.add (p22);
    p21.add (new Button ("aaa"));
    p21.add (new Button ("bbb"));
    p22.add (new TextField (20));
    
    p1.setBackground (Color.blue);
    p2.setBackground (Color.yellow);
    p21.setBackground (Color.red);
    p22.setBackground (Color.green);
    
    f.pack ();
    f.setVisible (true);
    f.addWindowListener (new WindowAdapter () {
      @Override public void windowClosing (WindowEvent e) {
        System.exit (0);
      }
    });
  }
}