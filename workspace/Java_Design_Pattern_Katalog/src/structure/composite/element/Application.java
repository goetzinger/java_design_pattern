package structure.composite.element;
public class Application {
  public static void main (String [] args) {
    Composite root = new Composite ("einnahmen/ausgaben");
    Composite einnahmen = new Composite ("einnahmen");
    Composite ausgaben = new Composite ("ausgaben");
    root.add (einnahmen);
    root.add (ausgaben);
    einnahmen.add (new Leaf ("seminar", 5000));
    einnahmen.add (new Leaf ("entwicklung", 8000));
    einnahmen.add (new Leaf ("wetten", 80000));
    Composite trinken = new Composite ("trinken");
    trinken.add (new Leaf ("osaft", -50));
    trinken.add (new Leaf ("pils", -30));
    ausgaben.add (trinken);
    ausgaben.add (new Leaf ("essen", -400));   
    
    print (root, 0);
    System.out.println (root.getValue ());
  }
  
  static void print (Element element, int depth) {
    for (int i = 0; i < depth; i++)
      System.out.print ("\t");
    System.out.println (element);
    for (int i = 0; i < element.getChildCount (); i++) {
      Element child = element.getChild (i);
      print (child, depth + 1);
    }   
  }
}