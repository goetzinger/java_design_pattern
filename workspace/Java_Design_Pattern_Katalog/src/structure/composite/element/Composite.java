package structure.composite.element;
import java.util.ArrayList;

public class Composite extends Element {
  private ArrayList<Element> children;
  public Composite (String name) {
    super (name);
  }
  public void add (Element element) {
    if (this.children == null)
      this.children = new ArrayList<Element> ();
    this.children.add (element);
  }
  public int getChildCount () {
    return this.children == null ? 0 : this.children.size ();
  }
  public Element getChild (int index) {
    return this.children.get (index);
  }
  @Override public String toString () {
    return this.getName ();
  }
  @Override public double getValue () {
    double v = 0;
    for (int i = 0; i < this.getChildCount (); i++) {
      v += this.getChild (i).getValue ();
    }
    return v;
  }
}