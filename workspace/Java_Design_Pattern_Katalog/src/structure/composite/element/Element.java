package structure.composite.element;
public abstract class Element {
  
  private final String name;
  public Element (String name) {
    this.name = name;
  }
  public String getName () {
    return this.name;
  }
  public int getChildCount () {
    return 0;
  }
  public Element getChild (int index) {
    throw new RuntimeException ();
  }
  public abstract double getValue ();
}
