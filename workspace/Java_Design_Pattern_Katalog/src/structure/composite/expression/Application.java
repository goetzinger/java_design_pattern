package structure.composite.expression;
public class Application {
  public static void main (String [] args) {
    // plus (minus (5, 2), sqr (2))
    Expression sqr = new SqrCall (new Constant (2));
    Expression minus = new MinusCall (new Constant (5), new Constant (2));
    Expression plus = new PlusCall (minus, sqr);
    System.out.println (plus.evaluate ());
  }
}