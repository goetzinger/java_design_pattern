package structure.composite.expression;
public abstract class Call extends Expression {
  private final String name;
  private final Expression [] arguments;
  public Call (String name, Expression [] arguments) {
    this.name = name;
    this.arguments = arguments;
  }
  @Override public int getArgumentCount () {
    return this.arguments.length;
  }
  @Override public Expression getArgument (int index) {
    return this.arguments [index];
  }
  @Override public String toString () {
    return this.name;
  }
}