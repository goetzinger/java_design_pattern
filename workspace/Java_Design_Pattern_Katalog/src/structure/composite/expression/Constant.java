package structure.composite.expression;
public class Constant extends Expression {
  private final double value;
  public Constant (double value) {
    this.value = value;
  }
  @Override public int getArgumentCount () {
    return 0;
  }
  @Override public Expression getArgument (int index) {
    throw new RuntimeException ();
  }
  @Override public double evaluate () {
    return this.value;
  }
  @Override public String toString () {
    return String.valueOf (this.value);
  }
}