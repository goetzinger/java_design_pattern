package structure.composite.expression;
public abstract class Expression {
  public abstract int getArgumentCount ();
  public abstract Expression getArgument (int index);
  public abstract double evaluate ();
}