package structure.composite.expression;
public class MinusCall extends Call {
	public MinusCall (Expression ... arguments) {
		super ("minus", arguments);
		if (arguments.length != 2)
			throw new RuntimeException ("bad arg count");
	}
	@Override public double evaluate () {
		return this.getArgument (0).evaluate () - this.getArgument (1).evaluate ();
	}
}