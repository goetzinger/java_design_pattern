package structure.composite.expression;
public class PlusCall extends Call {
  public PlusCall (Expression ... arguments) {
    super ("plus", arguments);
    if (arguments.length != 2)
      throw new RuntimeException ("bad arg count");
  }
  @Override public double evaluate () {
    return this.getArgument (0).evaluate () + this.getArgument (1).evaluate ();
  }
}

