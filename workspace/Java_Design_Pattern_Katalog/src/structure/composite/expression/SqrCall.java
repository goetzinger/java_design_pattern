package structure.composite.expression;
public class SqrCall extends Call {
	public SqrCall (Expression ... arguments) {
		super ("plus", arguments);
		if (arguments.length != 1)
			throw new RuntimeException ("bad arg count");
	}
	@Override public double evaluate () {
		return this.getArgument (0).evaluate () * this.getArgument (0).evaluate ();
	}
}