package structure.decorator.calculator;
public class Application {
  public static void main (String [] args) {
    test0 ();
    test1 ();
    test2 ();
    test3 ();
  }
  private static void test0 () {
    Calculator calc = new CalculatorImpl ();
    System.out.println (calc.plus (1, 1));
    System.out.println (calc.minus (10, 1));
    System.out.println ();
  }   
  private static void test1 () {
    TimesCalculator timesCalc = new TimesCalculator (new CalculatorImpl ());
    System.out.println (timesCalc.plus (1, 1));
    System.out.println (timesCalc.minus (10, 1));
    System.out.println (timesCalc.times (4, 3));
    System.out.println ();
  }   
  private static void test2 () {
    DivModCalculator divModCalc = new DivModCalculator (new CalculatorImpl ());
    System.out.println (divModCalc.plus (1, 1));
    System.out.println (divModCalc.minus (10, 1));
    System.out.println (divModCalc.div (20, 3));
    System.out.println (divModCalc.mod (20, 3));
    System.out.println ();
  }   
  private static void test3 () {
    TimesCalculator timesCalc = new TimesCalculator (new CalculatorImpl ());
    DivModCalculator divModCalc = new DivModCalculator (timesCalc);
    System.out.println (divModCalc.plus (1, 1));
    System.out.println (divModCalc.minus (10, 1));
    System.out.println (divModCalc.div (20, 3));
    System.out.println (divModCalc.mod (20, 3));
    System.out.println (timesCalc.times (4, 3));
    System.out.println ();
  }
}