package structure.decorator.calculator;
public interface Calculator {
  public abstract int plus (int x, int y);
  public abstract int minus (int x, int y);
}
