package structure.decorator.calculator;
public abstract class CalculatorDecorator implements Calculator {
  private final Calculator calculator;
  public CalculatorDecorator (Calculator calculator) {
    this.calculator = calculator;
  }
  public int plus (int x, int y) {
    return this.calculator.plus (x, y);
  }
  public int minus (int x, int y) {
    return this.calculator.minus (x, y);
  }
}