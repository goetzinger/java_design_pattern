package structure.decorator.calculator;
public class DivModCalculator extends CalculatorDecorator {
  public DivModCalculator (Calculator calculator) {
    super (calculator);
  }
  public int div (int x, int y) {
    int result = 0;
    while (x > y) {
      x = this.minus (x, y);
      result = this.plus (result, 1);
    }
    return result;
  }
  public int mod (int x, int y) {
    while (x > y) 
      x = this.minus (x, y);
    return x;
  }
}
