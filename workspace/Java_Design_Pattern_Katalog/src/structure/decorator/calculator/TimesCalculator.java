package structure.decorator.calculator;
public class TimesCalculator extends CalculatorDecorator {
  public TimesCalculator (Calculator calculator) {
    super (calculator);
  }
  public int times (int x, int y) {
    int result = 0;
    for (int i = 0; i < y; i++)
      result = this.plus (result, x);
    return result;
  }
}
