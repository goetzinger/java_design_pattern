package structure.decorator.writerAndReader;
import java.io.*;

public class Application {
  
  public static void main (String [] args) {
    writeFile ();
    readFile ();
  }
  
  private static void writeFile () {
    OutputStream outputStream = null;
    OutputStreamWriter outputStreamWriter = null;
    BufferedWriter bufferedWriter = null;
    PrintWriter printWriter = null;
    try {
      outputStream = new FileOutputStream ("abc.txt");
      outputStreamWriter = new OutputStreamWriter (outputStream);
      bufferedWriter = new BufferedWriter (outputStreamWriter);
      printWriter = new PrintWriter (bufferedWriter);
      
      outputStreamWriter.write ('X');
      outputStreamWriter.write (new char [] { 'Y', 'Z' });
      outputStreamWriter.write ('\n');
      
      bufferedWriter.write ('X');
      bufferedWriter.write (new char [] { 'Y', 'Z' });
      bufferedWriter.write ('\n');
      bufferedWriter.write ("Hello", 0, "Hello".length ());
      bufferedWriter.newLine ();
      
      printWriter.write ('X');
      printWriter.write (new char [] { 'Y', 'Z' });
      printWriter.write ('\n');
      printWriter.println ("Hello");
      printWriter.println (123);
      printWriter.println (3.14);
    }
    catch (IOException e) {
      System.err.println (e);
    }
    finally {
      printWriter.close ();
    }
  }

  private static void readFile () {
    InputStream inputStream = null;
    InputStreamReader inputStreamReader = null;
    BufferedReader bufferedReader = null;
    try {
      inputStream = new FileInputStream ("abc.txt");
      inputStreamReader = new InputStreamReader (inputStream);
      bufferedReader = new BufferedReader (inputStreamReader);
      
      char ch = (char) inputStreamReader.read ();
      System.out.print (ch);
      char [] buf = new char [2];
      inputStreamReader.read (buf, 0, buf.length);
      System.out.print (buf);
      ch = (char) inputStreamReader.read ();
      System.out.print (ch);
      
      ch = (char) bufferedReader.read ();
      System.out.print (ch);
      bufferedReader.read (buf, 0, buf.length);
      System.out.print (buf);
      ch = (char) bufferedReader.read ();
      System.out.print (ch);
      String s = bufferedReader.readLine ();
      System.out.println (s);
    }
    catch (IOException e) {
      System.err.println (e);
    }
    finally {
      try {
        bufferedReader.close ();
      }
      catch (IOException e) { }
    }
  }
}

/*

abstract public class Writer { // all methods throw IOExceptions 
  abstract public void flush ();
  abstract public void close ();
  abstract public void write (char [] buf, int offset, int length);
  public void write (char [] buf) {
    this.write (buf, 0, buf.length);
  }
  public void write (char ch) {
    this.write (new char [] { ch });
  }
  public void write (String s) {
    this.write (s.getBytes ());
  }
  // ...
}

public class BufferedWriter extends Writer {
  private Writer writer;
  public BufferedWriter (Writer writer) {
    this.writer = writer;
  }
  @Override public void close () { 
    this.writer.close ();
  }
  @Override public void flush () { 
    this.writer.flush ();
  }
  public void newLine () { ... }
  @Override public void write (char [] buf, int offset, int length) { ... }
  @Override public void write (char ch) { ... }
  public void write (String s, int offset, int length) { ... }
  
}

public class OutputStreamWriter extends Writer {
  private OutputStream stream;
  public OutputStreamWriter (OutputStream stream) {
    this.stream = stream;
  }
  @Override public void flush () { 
    this.stream.flush ();
  }
  @Override public void close () { 
    this.stream.close ();
  }
  @Override public void write (char [] buf, int offset, int length) {
    stream.write (buf, offset, length);
  }
}


abstract public class OutputStream {
  public void flush () { }
  public void close () { }
  public void write (byte [] buf) {
    this.write (buf, 0, buf.length);
  }
  public void write (byte [] buf, int offset, int length) {
    for (int i = 0; i < length; i++)
      this.write (buf [i + offset];
  }
  abstract public void write (int byte);  
  // ...
}

class FileOutputStream extends OutputStream {
  public FileOutputStream (String name) { ... }
  @Override public void flush () { ... }
  @Override public void close () { ... }
  @Override public void write (int byte) { ... }
  // ...
}

*/
