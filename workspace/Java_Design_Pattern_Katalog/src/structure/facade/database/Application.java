package structure.facade.database;
public class Application {
  public static void main (String [] args) {
    Database db = null;
    Table table = null;
    try {
      db = new Database ("sun.jdbc.odbc.JdbcOdbcDriver", "jdbc:odbc:patterns");
      for (String tableName : db.getTableNames ())
        System.out.println (tableName);

      System.out.println ();  

      table = db.select ("select * from customer");
      for (int i = 0; i < table.getColumnCount (); i++)
        System.out.print (table.getColumnName (i) + "\t");

      System.out.println ("\n");

      Object [] row;
      while ((row = table.next ()) != null) {
        for (Object value : row)
          System.out.print (value + "\t");
        System.out.println ();
      }
    }
    finally {
      if (table != null) table.close ();
      if (db != null) db.close ();
    }
  }

}