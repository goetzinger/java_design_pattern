package structure.facade.database;
import java.sql.*;
import java.util.ArrayList;

public class Database {
  private final Connection con;
  public Database (String driverClassName, String url) {
    try {
      Class.forName (driverClassName);
      this.con = DriverManager.getConnection (url);
    }
    catch (Exception e) {
      throw new RuntimeException (e);
    }
  }
  public Table select (String query) {
    return new Table (this.con, query);
  }

  public String [] getTableNames () {
    ResultSet rs = null;
    try {
      ArrayList<String> tableNames = new ArrayList<String> ();
      DatabaseMetaData md = this.con.getMetaData ();
      rs = md.getColumns (null, null, "%", "%");
      String oldName = "";
      while (rs.next ()) {
        String name = rs.getString ("TABLE_NAME");
        if (! name.equals (oldName)) {
          oldName = name;
          tableNames.add (name);
        }
      }
      return tableNames.toArray (new String [tableNames.size ()]);
    }
    catch (SQLException e) {
      throw new RuntimeException (e);
    }
    finally {
      if (rs != null) try { rs.close (); } catch (Exception e) { }
    }
  }

  public void close () {
    try {
      this.con.close ();
    }
    catch (SQLException e) {
    } 
  }
}

