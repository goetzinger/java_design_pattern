package structure.facade.database;
import java.sql.*;

public class Table {
  
  private Statement stmt;
  private ResultSet rs;
  private ResultSetMetaData md;
  
  Table (Connection con, String query) {
    try {
      this.stmt = con.createStatement ();
      this.rs = this.stmt.executeQuery (query); 
      this.md = this.rs.getMetaData ();
    }
    catch (SQLException e) {
      throw new RuntimeException (e);
    }
  }
  public Object [] next () {
    try {
      if (! this.rs.next ())
        return null;
      int n = this.md.getColumnCount ();    
      Object [] row = new Object [n];
      for (int i = 0; i < n; i++) 
        row [i] = rs.getObject (i + 1);
      return row;
    }
    catch (SQLException e) {
      throw new RuntimeException (e);
    }
  }
  public int getColumnCount () {
    try {
      return this.md.getColumnCount ();
    }
    catch (SQLException e) {
      throw new RuntimeException (e);
    }
  }
  public String getColumnName (int index) {
    try {
      return this.md.getColumnName (index + 1);
    }
    catch (SQLException e) {
      throw new RuntimeException (e);
    }
  }
  public void close () {
    try {
      this.rs.close ();
      this.stmt.close ();
    }
    catch (SQLException e) {
    }
  }
}