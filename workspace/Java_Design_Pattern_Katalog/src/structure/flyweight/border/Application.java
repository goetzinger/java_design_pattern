package structure.flyweight.border;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Application {
  public static void main (String [] args) throws Exception {
    UIManager.setLookAndFeel (UIManager.getSystemLookAndFeelClassName ());
    JFrame frame = new JFrame ("Borders");
    JLabel labelA = new JLabel ("Line Border");
    JLabel labelB = new JLabel ("Bevel Border (RAISED)");
    JLabel labelC = new JLabel ("Bevel Border (LOWERED)");
    labelA.setBorder (BorderFactory.createLineBorder (Color.red, 2));
    labelB.setBorder (BorderFactory.createRaisedBevelBorder ());
    labelC.setBorder (BorderFactory.createLoweredBevelBorder ());
    frame.setLayout (new FlowLayout ());
    frame.add (labelA);
    frame.add (labelB);
    frame.add (labelC);
    frame.addWindowListener (new WindowAdapter () {
      @Override public void windowClosing (WindowEvent e) {
        System.exit (0);
      }
    });
    frame.pack ();
    frame.setVisible (true);
  }
} 
  