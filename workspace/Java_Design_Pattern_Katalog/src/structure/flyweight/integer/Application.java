package structure.flyweight.integer;
public class Application {
  public static void main (String [] args) {
    Integer i1 = new Integer (0);
    Integer i2 = new Integer (0);
    Integer i3 = Integer.valueOf (0);
    Integer i4 = Integer.valueOf (0);
    
    System.out.println (i1 == i2);
    System.out.println (i3 == i4);
    System.out.println ();
        
    for (int i = 0; i < 130; i++)
      System.out.println (i + "\t:" + 
                  (Integer.valueOf (i) == Integer.valueOf (i)));
                 
  }
}