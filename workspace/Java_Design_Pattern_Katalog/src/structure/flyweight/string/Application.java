package structure.flyweight.string;
public class Application {
  public static void main (String [] args) {
    String s1 = Console.readLine ();
    String s2 = Console.readLine ();
    System.out.println (s1 == s2);
    
    String s3 = Console.readLine ().intern ();
    String s4 = Console.readLine ().intern ();
    System.out.println (s3 == s4);

    String s5 = StringCache.intern (Console.readLine ());
    String s6 = StringCache.intern (Console.readLine ());
    System.out.println (s5 == s6);

    Console.readLine ("type CR ");
  }
}