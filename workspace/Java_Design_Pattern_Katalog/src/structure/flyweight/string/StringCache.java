package structure.flyweight.string;
import java.util.ArrayList;

public class StringCache {
  private static ArrayList<String> cache = new ArrayList<String> ();
  public static String intern (String str) {
    if (str == null)
      return str;
    for (String s : cache)
      if (s.equals (str))
        return s;
    cache.add (str);
    return str;
  }
}