package structure.flyweight.string;

public class StringTest2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String hallo = "Hallo";
		String hallo2 = "Hallo";
		System.out.println(hallo == hallo2);
		
		String hallo3 = new String("Hallo");
		System.out.println(hallo == hallo3);
		System.out.println(hallo.equals(hallo3));
		
		System.out.println(hallo.intern() == hallo3.intern());
		

	}

}
