package structure.flyweight.symbol;
import java.io.StringReader;

public class Application {
  public static void main (String [] args) {
    SymbolFactory symbolFactory = new SymbolFactoryA ();
    StringReader reader = new StringReader ("plus (10, plus (10, 10))");
    Scanner scanner = new Scanner (symbolFactory, "(,)", reader);
    Symbol s0 = scanner.read ();    // plus
    Symbol s1 = scanner.read ();    // (
    Symbol s2 = scanner.read ();    // 10
    Symbol s3 = scanner.read ();    // ,
    Symbol s4 = scanner.read ();    // plus
    System.out.println (s0 == s4);
    Symbol s5 = scanner.read ();    // (
    System.out.println (s1 == s5);
    Symbol s6 = scanner.read ();    // 10
    System.out.println (s6 == s2);
    Symbol s7 = scanner.read ();    // ,
    System.out.println (s7 == s3);
    Symbol s8 = scanner.read ();    // 10
    System.out.println (s8 == s6);
    Symbol s9 = scanner.read ();    // )
    Symbol s10 = scanner.read ();   // )
    System.out.println (s10 == s9);
  }
}
