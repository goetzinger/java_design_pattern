package structure.flyweight.symbol;
import java.io.Reader;

class Scanner {

  private final SymbolFactory symbolFactory;
  private final Reader reader;
  private final String specialCharacters;
  private int currentChar;
  private final StringBuffer buf = new StringBuffer ();

  public Scanner (SymbolFactory symbolFactory, String specialCharacters, Reader reader) {
    this.symbolFactory = symbolFactory;
    this.specialCharacters = specialCharacters;
    this.reader = reader;
    this.readChar ();
  }

  public Symbol read () {
    while (Character.isWhitespace(this.currentChar))
      this.readChar ();
    if (this.currentChar == -1) 
      return null;
    if (Character.isDigit (this.currentChar)) 
      return this.readNumber ();
    if (Character.isLetter(this.currentChar)) 
      return this.readIdentifier ();
    if (this.specialCharacters.indexOf (this.currentChar) >= 0) {
      Symbol symbol = this.symbolFactory.createSpecialSymbol ((char) this.currentChar);
      this.readChar ();
      return symbol;
    }
    throw new RuntimeException ("bad char: '" + (char) this.currentChar + "'");
  }

  private Symbol readNumber () {
    this.buf.setLength (0);
    while (Character.isDigit(this.currentChar)) {
      this.buf.append ((char)this.currentChar);
      this.readChar ();
    }
    if (this.currentChar == '.') {
      this.buf.append ((char)this.currentChar);
      this.readChar ();
      while (Character.isDigit(this.currentChar)) {
        this.buf.append ((char)this.currentChar);
        this.readChar ();
      }
    }
    if (Character.isLetter(this.currentChar))
      throw new RuntimeException ("bad number: " + this.buf + (char) this.currentChar);
    return this.symbolFactory.createNumberSymbol (
                          Double.parseDouble (this.buf.toString()));
  }

  private Symbol readIdentifier () {
    this.buf.setLength (0);
    this.buf.append ((char) this.currentChar);
    this.readChar ();
    while (Character.isLetterOrDigit(this.currentChar)) {
      this.buf.append ((char) this.currentChar);
      this.readChar ();
    }
    return this.symbolFactory.createIdentifierSymbol (this.buf.toString ());
  }

  private void readChar () {
    try {
      this.currentChar = this.reader.read ();
    }
    catch (Exception e) {
      throw new RuntimeException (e);
    }
  }
}
