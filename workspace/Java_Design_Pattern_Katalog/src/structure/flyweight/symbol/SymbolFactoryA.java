package structure.flyweight.symbol;
import java.util.HashMap;

public class SymbolFactoryA implements SymbolFactory { 

  private static class SymbolImpl implements Symbol { 

    private static RuntimeException illegalCallException =
            new RuntimeException ("illegal call");

    private final Object value;

    public SymbolImpl (Object value) {
      this.value = value;
    }

    public boolean isNumber () { 
      return this.value instanceof Double; 
    }
    public boolean isIdentifier () { 
      return this.value instanceof String; 
    }
    public boolean isSpecial () { 
      return this.value instanceof Character; 
    }

    public double getNumber () { 
      if (! this.isNumber ())
        throw illegalCallException; 
      return ((Double) this.value).doubleValue ();
    }
    public String getIdentifier () { 
      if (! this.isIdentifier ())
        throw illegalCallException; 
      return (String) this.value; 
    }
    public char getSpecial () { 
      if (! this.isSpecial ())
        throw illegalCallException; 
      return ((Character) this.value).charValue ();
    }

    public Object getValue () {
      return this.value;
    }

    @Override public String toString () {
      return this.getClass ().getName () + " [" + this.getValue () + "]";
    }
  }

  private final HashMap<Object,Symbol> symbols = new HashMap<Object,Symbol> ();
  private Symbol getSymbol (Object value) {
    Symbol symbol = this.symbols.get (value);
    if (symbol == null) {
      symbol = new SymbolImpl (value);
      this.symbols.put (value, symbol);
    }
    return symbol;
  }

  public Symbol createNumberSymbol (double value) {
    return this.getSymbol (new Double (value));
  }
  public Symbol createIdentifierSymbol (String value) {
    return this.getSymbol (value);
  }
  public Symbol createSpecialSymbol (char value) {
    return this.getSymbol (new Character (value));
  }
}
