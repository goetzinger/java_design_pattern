package structure.flyweight.symbol;
import java.util.HashMap;

public class SymbolFactoryB implements SymbolFactory { 

  private static abstract class AbstractSymbol implements Symbol { 
    private static RuntimeException illegalCallException =
            new RuntimeException ("illegal call");

    public boolean isNumber ()      { return false; }
    public boolean isIdentifier ()  { return false; }
    public boolean isSpecial ()     { return false; }

    public double getNumber ()      { throw illegalCallException; }
    public String getIdentifier ()  { throw illegalCallException; }
    public char getSpecial ()       { throw illegalCallException; }

    @Override public String toString () {
      return this.getClass ().getName () + " [" + this.getValue () + "]";
    }
  }

  private static class NumberSymbol extends AbstractSymbol { 
    private final double value;
    public NumberSymbol (double value)        { this.value = value; }
    @Override public boolean isNumber ()      { return true;  }
    @Override public double getNumber ()      { return this.value; }
    public Object getValue ()                 { return new Double (this.value); }
  }

  private static class IdentifierSymbol extends AbstractSymbol { 
    private final String value;
    public IdentifierSymbol (String value)    { this.value = value; }
    @Override public boolean isIdentifier ()  { return true; }
    @Override public String getIdentifier ()  { return this.value; }
    public Object getValue ()                 { return this.value; }
  }

  private static class SpecialSymbol extends AbstractSymbol { 
    private final char value;
    public SpecialSymbol (char value)         { this.value = value; }
    @Override public boolean isSpecial ()     { return true; }
    @Override public char getSpecial ()       { return this.value; }
    public Object getValue ()                 { return new Character (this.value); }
  }

  private final HashMap<Object,Symbol> symbols = new HashMap<Object,Symbol> ();

  public Symbol createNumberSymbol (double value) {
    Symbol symbol = this.symbols.get (value);
    if (symbol == null) 
      this.symbols.put (value, symbol = new NumberSymbol (value));
    return symbol;
  }
  public Symbol createIdentifierSymbol (String value) {
    Symbol symbol = this.symbols.get (value);
    if (symbol == null) 
      this.symbols.put (value, symbol = new IdentifierSymbol (value));
    return symbol;
  }
  public Symbol createSpecialSymbol (char value) {
    Symbol symbol = this.symbols.get (value);
    if (symbol == null) 
      this.symbols.put (value, symbol = new SpecialSymbol (value));
    return symbol;
  }
}
