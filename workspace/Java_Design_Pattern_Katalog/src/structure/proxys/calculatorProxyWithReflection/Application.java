package structure.proxys.calculatorProxyWithReflection;
public class Application {

  public static void main (String [] args) {
    Calculator calculator = new CalculatorProxy (new TraceHandler (new CalculatorImpl ()));
    
    calculator.add (20);
    calculator.subtract (5);
    System.out.println (calculator.getValue ());
  }
}
