package structure.proxys.calculatorProxyWithReflection;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationHandler;

public class CalculatorProxy implements Calculator {

  static private Method m0;
  static private Method m1;
  static private Method m2;
  
  static {
    try {
      m0 = Calculator.class.getMethod ("add", new Class [] { int.class });
      m1 = Calculator.class.getMethod ("subtract", new Class [] { int.class });
      m2 = Calculator.class.getMethod ("getValue", new Class [] { });
    }
    catch (Exception ignored) { }
  }
  
  private final InvocationHandler handler;
  
  public CalculatorProxy (InvocationHandler handler) {
    this.handler = handler;
  }
  
  public void add (int value) {
    try {
      this.handler.invoke (this, m0, new Object [] { new Integer (value) });
    }
    catch (Throwable e) { throw new RuntimeException (e); }
  }

  public void subtract (int value) {
    try {
      this.handler.invoke (this, m1, new Object [] { new Integer (value) });
    }
    catch (Throwable e) { throw new RuntimeException (e); }
  }

  public int getValue () {
    try {
      return ((Integer) this.handler.invoke (this, m2, new Object [] { })).intValue ();
    }
    catch (Throwable e) { throw new RuntimeException (e); }
  }
}
