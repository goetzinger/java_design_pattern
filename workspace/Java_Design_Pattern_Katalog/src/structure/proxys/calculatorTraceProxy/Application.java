package structure.proxys.calculatorTraceProxy;
import java.util.*;

class CalculatorEvent extends EventObject {
  private final String methodName;
  private final Object [] args;
  public CalculatorEvent (Calculator source, String methodName, Object [] args) {
    super (source);
    this.methodName = methodName;
    this.args = args;
  }
  public String getMethodName () {
    return this.methodName;
  }
  public int getArgCount () {
    return this.args == null ? 0 : this.args.length;
  }
  public Object getArg (int index) {
    return this.args [index];
  }
}
  
interface CalculatorListener extends EventListener {
  public abstract void methodCalled (CalculatorEvent e);
}

class CalculatorNotificationProxy implements Calculator {

  private final Calculator calculator;
  private ArrayList<CalculatorListener> listeners 
    = new ArrayList<CalculatorListener> ();
  
  public void addCalculatorListener (CalculatorListener l) {
    this.listeners.add (l);
  }
  public void removeCalculatorListener (CalculatorListener l) {
    this.listeners.remove (l);
  }
  private void notify (String methodName, Object [] args) {
    CalculatorEvent e = new CalculatorEvent (this, methodName, args);
    for (CalculatorListener l : this.listeners)
      l.methodCalled (e);
  }
  public CalculatorNotificationProxy (Calculator calculator) {
    this.calculator = calculator;
  }
  
  public void add (int value) {
    this.notify ("add", new Object [] { value });
    this.calculator.add (value);
  }

  public void subtract (int value) {
    this.notify ("subtract", new Object [] { value });
    this.calculator.subtract (value);
  }

  public int getValue () {
    this.notify ("getValue", null);
    return this.calculator.getValue (); 
  }
}

public class Application {

  public static void main (String [] args) {
    Calculator calculator = new CalculatorTraceProxy (new CalculatorImpl ());
    //Calculator calculator = new CalculatorImpl ();
    
    calculator.add (20);
    calculator.subtract (5);
    System.out.println (calculator.getValue ());
  }
}
