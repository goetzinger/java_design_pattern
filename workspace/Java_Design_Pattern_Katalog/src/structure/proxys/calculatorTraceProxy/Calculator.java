package structure.proxys.calculatorTraceProxy;
public interface Calculator {
  public abstract void add (int value);
  public abstract void subtract (int value);
  public abstract int getValue (); 
}