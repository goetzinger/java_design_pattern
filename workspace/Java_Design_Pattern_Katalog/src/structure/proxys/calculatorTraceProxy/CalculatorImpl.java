package structure.proxys.calculatorTraceProxy;
public class CalculatorImpl implements Calculator {

  private int value;

  public void add (int value) {
    this.value += value;
  }

  public void subtract (int value) {
    this.value -= value;
  }

  public int getValue () {
    return this.value;
  }
}