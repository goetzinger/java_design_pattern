package structure.proxys.calculatorTraceProxy;
public class CalculatorTraceProxy implements Calculator {

  private final Calculator calculator;

  public CalculatorTraceProxy (Calculator calculator) {
    this.calculator = calculator;
  }
  
  public void add (int value) {
    System.out.println ("--> add (" + value + ")");
    this.calculator.add (value);
    System.out.println ("<--");
  }

  public void subtract (int value) {
    System.out.println ("--> subtract (" + value + ")");
    this.calculator.subtract (value);
    System.out.println ("<--");
  }

  public int getValue () {
    System.out.println ("--> getValue ()");
    int value = this.calculator.getValue (); 
    System.out.println ("<-- " + value);
    return value;
  }
}