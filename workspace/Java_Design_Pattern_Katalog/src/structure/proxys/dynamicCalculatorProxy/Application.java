package structure.proxys.dynamicCalculatorProxy;
import java.lang.reflect.Proxy;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationHandler;

public class Application {

  public static void main (String [] args) {
    try {
      Class counterProxyClass = Proxy.getProxyClass (
        Calculator.class.getClassLoader (), new Class[] { Calculator.class });

			System.out.println (counterProxyClass.getName ());
      Constructor constructor = counterProxyClass.getConstructor (
        new Class [] { InvocationHandler.class }
      );

      Calculator c = (Calculator) constructor.newInstance (
        new Object [] { new TraceHandler (new CalculatorImpl ()) }
      );
      
      /*
      Calculator c = (Calculator) Proxy.newProxyInstance (Calculator.class.getClassLoader(),
                                        new Class[] { Calculator.class },
                                        new TraceHandler (new CalculatorImpl ()));
      */
      
      c.add (20);
      c.subtract (5);
      System.out.println (c.getValue ());
    }
    catch (Exception e) {
      e.printStackTrace ();
    }
  }
}
