package structure.proxys.dynamicCalculatorProxy;
public interface Calculator {
  public abstract void add (int value);
  public abstract void subtract (int value);
  public abstract int getValue (); 
}