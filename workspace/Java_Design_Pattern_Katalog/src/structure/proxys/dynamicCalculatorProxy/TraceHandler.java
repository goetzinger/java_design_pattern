package structure.proxys.dynamicCalculatorProxy;
import java.lang.reflect.Method;
import java.lang.reflect.InvocationHandler;

public class TraceHandler implements InvocationHandler {
  
  private final Object object;
  
  public TraceHandler (Object object) {
    this.object = object;
  }

  public Object invoke (Object proxy, Method method, Object[] args) throws Throwable {

    // pre-invoke!!!
    String arguments = "";
    if (args != null) {
      for (int i = 0; i < args.length; i++) {
        if (i > 0)
          arguments += ", ";
        arguments += args [i];
      }
    }
    System.out.println ("--> " + method.getDeclaringClass ().getName () + "#" + method.getName () + " (" + arguments + ")");

    // invoke!!!
    Object result = method.invoke (this.object, args);

    // post-invoke!!!
    if (method.getReturnType () != void.class) 
      System.out.println ("<-- " + result);
    else    
      System.out.println ("<--");

    return result;
  }
}
