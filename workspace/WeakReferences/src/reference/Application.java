package reference;

import java.lang.ref.SoftReference;

public class Application {
	
	private Byte[] grossesByteArray;
	private SoftReference<Byte[]> refToByteArray;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		new Application().init();
	}

	private void init() {
		grossesByteArray = new Byte[100000];
		refToByteArray = new SoftReference<Byte[]>(grossesByteArray);
		grossesByteArray = null;
		System.gc();
		Byte[] bytes = refToByteArray.get();
		if(bytes != null){
			System.out.println("!= null");
		}
		else 
			System.out.println("null");
	}

}
